# S.T.F. - privacy policy (En)

This application saves Data about your behaviour. Only Dates that you did manually enter. 

These data will only be accessible to you. 

If necessary (you use several devices and need to synchonize them, or you need to process Datas externally) you can synchronize them with a server on your local network (install Apache CouchDB for example), by entering the necessary configuration via the System Preferences. This function is non-active for the moment.


# S.T.F. - Politique de confidentialité (Fr)

Cette application n'enregistre de données que selon votre comportement. Seulement des Dates que vous avez choisies.

Ces données ne sont accessibles que par vou-même.

Si nécessaire (vous utilisez plusieurs appareils que vous voulez synchroniser, ou voulez les traîter sur un poste de travail), vous avez la possibilité de vous synchroniser avec un serveur de votre réseau local (vous pouvez installer Apache CouchDB par exemple), si vous entrez la configuration nécessaire en passant par les préférences système. Cette fonction est inactive pour l'instant.
