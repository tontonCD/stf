# STF - Première Version (Fr)

Dans la première version, le temps d'attente (conseillé) pour un événement dépend des deux précédents.

## exemple

Disons que la <u>dernière</u> cigarette (la plus récente) a été prise **à Midi**. Combien attendre pour la suivante ?

Selon que celle avant a été prise 

- à 11h00 (ou plus tôt) -> vous devrez attendre 12h45 ;
- à 11h15 -> vous devrez attendre 13h00 ;

<img src="DOCS/ganttSTF.png" />

Au niveau des réglages, celui-ci correspond à un délai après la cigarette C2 de 

- <u>1h</u> par défaut, avec possibilité de le réduire à <u>45mn</u>, **si** la précédente (C1) est suffisamment ancienne.

Ce système est assez réaliste sur des cas comme le matin (on est moins disposé à attendre), ou une abstinence plus longue (sortie au cinéma, ...)

Ce réglage de (1h00, -15mn) doit correspondre au temps à partir duquel vous allez généralement commencer à vous <i>poser la question</i> d'allumer une nouvelle cigarette. S'il est bien choisi vous vous rendrez compte souvent qu'en voulant attendre 5mn (en faisant autre chose) il s'écoulera 15mn ou plus sans que vous y pensiez. 

Le bilan dans un premier temps est une réduction de 10 à 20%. Il est assez difficile de l'améliorer, mais au moins il est stable.

## Et après ?

Les cigarettes sont plus rapprochées le matin, jusqu'à la troisième, ce dont cette version ne tient pas compte. D'où la V2.
