# S.T.F. - VERSION LOG




## Issues


- change text when the Devise is vertical ("portrait") : 01, 02, 03...
  
  - this will avoid resizing with too much tabbing because of too much overlapping; 
  
  - this is an useful information too,
  
  - needs to track this information in Entries (beware of Insert mode); needs also to fix the time for Day Start (after midnight)

- when a forgetten event is added, the color for the Current Date is not updated (it is, but later))


## Change Log

Here is the list of the newest versions in Descending Order, beginning from the latest version.


### pending

-- don't suppress older Entries considering CurrentDate distance but last Entry distance
-- remove Disclaimer (moved to a web page, link known from AppStore)
-- issue: after a timeField is edited, its border color (as for the next ones) is not updated
fastlane: add image examples to the project for visionning situations
remove disclaimer (displace to web), replace with usage description
fastlane: ? chain 1 page for tuto, then data exemple , then settings (e.g.),
TODO: check to clone the repository (GitLab), if all compile (pay attention to cocoapod, ignore fastlane)

TODO: the scrollView is smaller than its container(wanted?) 
fastlane: enabled xcode log,
fastlane: force Landscape orientation

TODO : grossir "Add"
TODO: faire 2 version "Add" et "Modify", to avoid equivocity, 
CLEAN?
    gvDisplayedEntries_y
    
    settingChanged
    UITapGestureRecognizer
    etfDefault_y
    func transition
    replicatorDidError
    refacto (refactored, refactoring, ...)
    adtfTabbed
    is24Hour
    what about WMGraphicView (i.e. graph update)
    // displaced
    gvDisplayedDates_y
    "Entry", "Date"

trying dvcDigicodeView.dcMainViewControler?.digicodeAppearing = ..., VOIR shouldAutorotate
-> on devrait désactiver ensureDateRangeForCurrentDate() quand digicode (ou pas?)


###  #0.49.1
 - tagged as "**v2.9.1**"

###  #0.49
- **MAJOR FIX: for Edit, Field identification** when double-tap, 
- FIX: controlling screen rotation when dicode appears,
- **MAJOR FIX: correction on a Full Date when in the other side of 00:00** (e.g. 01:00 while 23:00 or 23:00 while 01:00)

###  #0.48
- localized Strings Fr/En for MWCurrentDateTextField ("Double-Tap...")
- localized Strings Fr/En for WMDigicodeViewController ("Add", "Modify", ...)
- localized Strings Fr/En for WMSettingsViewController ("Delays", ...)
- issue with iPod Touch, the presentation text doesn't appear in totality -> Scroll view
- re-enabled the Analysis (xcode) feature

###  #0.47
- fastlane: changed target devices,
- added a **view presenting the app**, managed as displayed just once (as the disclaimer),
- localized this view,
- **fix with DarkMode: the Color Gradient was not correct when switching**




###  #0.46
- refactoring
- added "description" functions to improve debugging
- **added fastlane support**
- found an anomaly in cocoapod, because initial project was "STF" (and the scheme "STF"), and the workwspace was renamed as "STF_New", so "pod update" looked for a scheme named "STF_New", FIXED (note: with cocoapod use '''arch -x86_64 pod update''' on M1)
- changed Deployment Target to 9.0

###  #0.46
- fix Dates TF to not always present at the right place
- fixed the XCTest functions to compile
- fixed one XCTest function

###  #0.45
- FIXED: saving the disclaimer (don't present again!: caused an infinite loop)
- FIXED: the graphicView height at launch ; because of a top bar appearing beetween Rotation notification (calling zoomUpdate), and Draw notification.
- FIXED: at launch (while no Entry), currentDate TextFields Y position was wrong after rotations
- FIXED: at launch (while no Entry), any new Date TextField Y position was totaly wrong 
- adding more Date (missing ones) at launch
- tagged as "**v2.9**" (version 2.8.9 in project)



###  #0.44
- did add all Icons necessary to Apple submission,
- did start some Localization
- added an EasterEgg for testing the Synchro,


###  #0.43
- ISSUE: removing an Entry didn't really remove if from Database, only from display. This affected also Modify (so appeared twice at relaunch). FIXED
- updated Pod
- more on Replicator integration (viewController, activityIndicator, ...), still disabled; 
- displays the disclaimer, once time using UserDefaults
- tagged as "v2.8"

###  #0.42
- refactoring
- FIX on graphic updates when Setting changed
- FIX on unassigned userID default at the first launch (expecting "Guest")
- fixed some UI settings (buttons Styles, iOS prior to 15.0)
- **updated the "Add" view to not cover the graphic view** (was right in Simulator, not Device)
- Settings: the Steppers (Optimal/Allowed) are now continuous,


###  #0.41
- fixed some settings (build targets)
- fixed some settings (buttons attributes, iOS prior to 15.0)
- fixed some settings (views/tables attributes, iOS prior to 11.0)
- replaced the default user (unasigned, identified with ""), with "Guest" (for easier future implementation of Users managing)
- **save/load the Settings values within the User entity (Setting is no more a distinct Entity)**
- feature: fully implemented the Settings View (load/save); (todo: needeed to update graphics -> OK)

###  #0.40
- playing with the SettingsViewController updates the Settings
- the WMLevelUtils parameters are updated


###  #0.39
- connected the Settings View, 
- get the Settings, can modify, save (in System UserDefaults), (todo: apply)
- fix: Times as computed to be "green" often appeared in "orange", due to rounding values as minutes,
- strong issue: system icons for the Settings (and so on) don't appear on iPhone -> due to iOS version -> designed all icons, FIXED
- strong issue: date conversions (Date<->String) fail if User Settings want the Time to be displayed as 12h (e.g. "14:44:30" VS "2:44:30 PM"), could crash, FIXED



###  #0.38
- refactored Dates functions from DataTextFields to DatesTextFields,
- simplified Green/Blue TextFields access,
- simplified Dates TextFields builds,
- fixed a wrong TextFields size at launch
- fix a wrong Day Date when a new Entry and the Current Time are in opposite part of midnight (not check)

                
### #0.37
- removed an useless redundancy in Entries with the corresponding database object
- Digicode: Remove implemented;
- Digicode: buttonActionSettingsS
- Fix: at start the Current date is wrong placed
- Fix: when the last untabbed Entry disapears, checking overlapping did crash
- shows the userID chooser from DigicodeViewController
- swow the User Settings from DigicodeViewController
- placed the Sync button on DigicodeViewController
- removed the DigicodeView embed in the main view (just working now with the DigicodeViewController)

### #0.36

- fix about forcing the landscape display at launch,
- moved the Digicode View to its own ViewController
- the Digicode ViewController display clearly: "Add Entry" vs "Modify Entry"
- prepared the "Remove Entry" feature

### #0.35

- included **Replicator**, for NoSQL synchronization,
- checked replication with Apache CouchDB (https://couchdb.apache.org), in local OK, on device OK,
- fix System Prefs, so can use address/credential/password from there,
- migration: can work with database if not user defined;
- the **multi-users feature** is **disabled** (needs too much updates / not essential now)
- added the Entity Type to the docID; updated save; updated load using also the old docID; 
- can work with userID, as with no userID;

todo: migration: when an userID is set (via the Setting View), purpose convert/remove/ignore

### #0.34

- UI: can **choose an User** at start; 
- Entries not load at start, but when an User is set,
- the last used User is purposed at start, (saved when chosen),
- the userID setting is unactivated, to allow working with no user (needs update)
- changed: the leftest Data not mandatory to be an Entry, can now be a Date (todo: improve this)
- implemented a **Setting View** (todo: improve, load/save the settings)
- implemented parameters as **System Prefs** (server address/password)(todo: missed userName)
- **force the display to landscape when at launch time**

### #0.33

- added userID in Entry id; Save OK, Load OK
- Load (from range) can remove old-style stored Entry (without userID)
- Modify Entry changes the ID back to old-style: fixed
- FIX: adding/modifying an Entry before the last didn't correctly update levels for the following Entries
- FIX: adding/modifying an Entry before the last didn't correctly update the green/blue Dates
- FIX: colors for Entry TextFields were not applied at launch
- preparing Parameters Setting via System Prefs

### #0.32

- fix: adding Dates din't make them to appear if no Entry,
- reconsidered the calculation for zoom parameters,
- more compatibility for the future Swipe function

### #0.31

- refactored TextFields management,
- fixed the #0.30 issue,
- fixed an Entry position issue: not tabbed while too much at left to be visible

### #0.30

- added TextFields for orangeLevel and greenLevel (for time to reach)
- added Lines drawing for these
- fixed the currentDate flashing
- fixing an issue: can crash if all Entries are tabbed; waiting refactoring to be efficient

### #0.29

- refactoring for tabNum (left/right)
- added EasterEgg: "87:03", for debugging
- added EasterEgg: "87:00", for debugging (remove all Entries)
- made the line down from an Entry to take the color from this Entry level
- added the CurrentDate TF to flash its ':' character

### #0.28

- fixing an issue: changed the EasterEgg Code and behavior ("87:01" adds Entries for the current Day, "87:02" for the previous)
- ISSUE: editing an old Entry (more than 24h before) changes its date (its Day changes to the current Day) => really, editing a tabbed Entry caused to make it untabbed (while successors remain tabbed) -> FIXED
- the CurrentDate makes the zoom to update if its field come across the view right border
- FIX: index mismatch when tabbing an Entry

### #0.27

- FIX: some Date fields miss to be removed when in the Tab Zone (because no Entry field recovering was detected)

### #0.26

- HOT Fix: replaced all Time references, (from: 60\*hh+mm) to a more precise description, because comparing two could fail with an unexpected behavior, if the difference was more than 24h.

### #0.25

- removed adding static Dates at launch, just add at Left and Right from the currentDate
- fix: the y-position for the very first Entry field was wrong
- displays a Black vertical line when Tabbed active, to delimit the "Tab Zone"
- fixed the x-position for the first untabbed Entry, i.e. just after the the last tabbed one (recompute graphic limits and zoom)
- doesn't draw graphic in the Tab Zone
- did choose in the Tab mode, that the leftest Untabbed TextField will be a Date TextField => infinite loop when Tabbing a Date => Fixed (the following TextField is tabbed if it is an Entry)

### #0.24

- added a TextField for the Current Time (placed Vertically at middle View)
- set the color of this textField to the Score color (corresponding to current Time)
- added a Timer for this  textField; the Timer updates its value, and its Color; actually based on a 20s repeating;

### #0.23

- made WMDisplayedEntries to deal with MWAbstractDateTextField (so, Dates and Entries) instead of just MWEntryTextField

- gvDateTextFields: changed from [MWDateTextField] to WMDisplayedEntries (as gvDisplayedEntries was)

- issue: when a Date was removed at Left, adding an Entry makes each time this Date to be added again, and removed again.

- Fix: needed to remove more Dates in the Tabbed Mode (no Date in the Tab Zone) 

- Fix: all Tabbed Entries are now well placed.
  
  <img src="DOCS/screenshot_STF_VLOG_0.23_.png" width="311" height="171" />

### #0.22,

- activated the Tabbed mode, for Left;

### #0.21,

- refactored gvEntriesTextFields to a class (was an Array), with iterator protocol
- fix in Level Calculation
- can remove Dates textFold at left if found overlapping within DisplayEntries

### #0.20,

- refactoring: removed gvEntriesTextFields (using now the existing gvDisplayedEntries)

### #0.19,

- it's possible now to Edit Entries

### #0.18,

- improved Digicode reaction,
- made possible to add Entries, for which Date is anterior to the last entered

### #0.17,

- conversion delay->reduction: Fixed, now can apply to other halfLife
- halfLife ready to become a parameter,
- conversion level_to_reach->delay

### #0.16,

- more dealing with full Dates instead of "hh:mm", which can cause errors
  placing or comparing elements

### #0.15, ce1f1fa

- added frame border for the digicodeView
- add a class from a custom ScrollView
- added a background gradient to the custom ScrollView
- commit before Recommended Settings

<img src="DOCS/screenshot_STF_VLOG_0.15_.png" width="311" height="171" />

- ? Dates not displayed at start if no Entry

### #0.14, 857772e

- removed the timeView
- added the scrollView
- made the Digicode buttons bigger
- improved the Digicode reaction
- added an Easter Egg, to fill the current Day with known Entries
- re-factorized the Level computation
- **added color changes according to the level** (alert/red, warning/orange, green)
- changed Level Computation to not automatically increase Level as done if new Entry
- Fixed Unit Tests, with this

TODO: gradient on Levels

### #0.13, cecd04c

- compute level for each Entry, dealing with the remaining level from the previous Entry
- added XCTests
  TODO: make that if the current time is before "06:00" (can be set?) the Dates range will be for the previous Day
  TODO: spray level addition when new Entry, e.g. (40) into (10*4) in 5 mn ?

### #0.12 , d190507

- draw the level from the last Entry to the last Date

<img src="DOCS/screenshot_STF_VLOG_0.12_.png" width="311" height="171" />

### #0.11, fc9c42d

- fine usage of DAO for Entries
- implemented WMDateUtils
- lot of refactoring
- tracking: displayed TextFields<->database relation for Entries
- adding an Entry doesn't reload the whole data for the current Day

### #0.10, bea3757

- add vertical lines from Entries (blue color), shorten at bottom
- did change lines color to grey for Date, and shorten at top
- fix on Device rotation
- refactored UITextField type entries and dates to  MWDateTextField and MWEntryTextField
- introducing data management as DAO,

<img src="DOCS/screenshot_STF_VLOG_0.10_.png" width="311" height="171" />

### #0.9, 5039a3f

- added a Hints.md (to improve)
- shorten vertical lines, managing with existin or not entries
- fixes both on String->Ints and Ints->Strings
- intercepts device rotation (with viewWillTransitionToSize)
- FIX on new Entries positions

### #0.8, cc89e48

- rearanged all Entries, rearanged all Dates, applying a zoom function
- FIX, mismatching Ints obtained from "HH:mm" (Entry) vs "HH" (Date)
- adds a Date at left if a new Entry is minor than it, at right if a new Entry is major than it

### #0.7, 46f5d29

- made Entries textfield as not editable
- made Absisses textfield as not editable
- updated code to show Absisses at bottom
- refactorized textFields building (both for Entries and Absisses)
- FIX: 2-digits padding with new Entries
- FIX comparing: Dates (as "HH" and Entries (as "HH:mm")
- added elements to Dates each time an Entry is post to the last Date (needs  a FIX)

### #0.6 , 9daf189

- CDTDatastore : added create, save, findData(forDay: String)
- Digicode: FIX on filter

### #0.5, 080f481

- added CDTDatastore (CloudAnt) support via cocoapods
- added textFields for hours, with lines as absisses

### #0.4

- changed the view hierarchy because of alpha conflicts
- improved the Time filter; moved it to the Digicode View

### #0.3

- changed Deployment Target to 11.4
- fix for iOS prior to 13
- filter Date Value when manually keyed (numbers can't be over 59)

### #0.2

- replaced the add Button by a gesture
- fixed the UI placements

### #0.1

- made the essential UI, 
- can add a time and display it

### #0.0.1

- added function to set the value to the digicode to appear
- added repartition within textFields, (needs FIX)
