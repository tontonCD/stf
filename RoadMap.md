# S.T.F. - Roadmap


## Features Resume

- The display shows 3 kinds of TextFields:
  
  - "**Entries**" TextFields, <u>defined by Users</u>, are shown on Top of the GraphView, their horizontal repartition reflects their Date value, 
  - "**Date**" TextFields, build sequentially by the app, are shown on Bottom of the GraphView, their are shown as abscissae marks; showed as two-digits values;
  - a "**Time**" TextFields, vertically centered, shows the <u>Current Time</u>, moving to to right as the Current Time increase; in addition two other field show  the recommended and expected Times; showed as two-digits+two-digits values (hh:mm); forced to the 24h display style;
  - all of these are <u>dynamically built</u> at runtime;

- when too many **Entries** make that some TextField do overleap, some are "Tabbed" at Left : they are vertically aligned (same X position), not considering their Date property,
  (todo: swipe feature => do tab at Right / do untab at Left)

- **Entries** are made when double-clicking any where, and setting a time is the popup-view; if double-clicking <u>in an existing</u> **Entry** TextField, it makes it to be Edited (a new value will replace the previous and the TextField is moved accordingly to this value); Entry can also be removed
  
- as soon as an **Entry** exists, a **Graphic** shows Levels evolment in 3 colors, tied to your real nicotine Level and eveluated using two thresholds: Red if the level is highter than the "maximum" level, Green if lower than the "accepted" level, Orange between. You may set these two level by yourself.
  
  This is so showing an evaluation for the opportunity to create a ***new*** **Entry**; you are supposed to wait the **current Time** to stand over an <u>orange</u> zone. The goal is to wait a <u>green</u> zone and change as it comes easier. The <u>orange</u> zone is a tolerance.

- **Dates** are created as necessary, ensuring to cover the all **Entries** TextField, as the **current Time** TextField, and the time where the Graphic turns to green

- if creating an **Entry** Fields an overleap occurs, a zoom factor is applied; if all Entries for a day are not contained in the main View, some **Date** Fields are removed, and some Entry Fields are "**tabbed**", meaning that a "Tab Zone" is activated, these Entry Fields will be vertically placed in, and all objects in the remaining zone (whole view zone minus "Tab Zone") are placed differently,

- "**Entries**" can be added in any order, and sorted (it's rarelly usefull do do that)

- "**Entries**" are persistent

- "**Entries**" dates are saved in the UTC format, and displayed in the locale format; 24h display (vs AM/PM) is forced, to avoid spoiling place of the display.




## Status:

**INCOMPLETE**


## RoadMap

Some of them can be implemented after publication.


- [x] add Entries (using double-tap)

- [x] can change the display range (automatic, checking for Entries overlapping)

- [x] colors for Entry Fields (and the line to bottom) according to the level at creation Time; needs to store levelBefore/levelNow

- [x] present a View for Level Settings: "maximum" level and "accepted" level,
  
  - [x] design the view
  - [ ] add the choice for the Starting Day Time (means that a "Day" starts from this Time to the same Time 24h later) (**ok** but not used yet)

- [ ] use "User" identity (ask at launch?)
  
  - [x] use userID to save and Load (local database)
  - [x] can work with/without userID 
  - [ ] when switching from without to with identity, ask for converting non-user Entries
  - [ ] display this view
  - [ ] if use userID, display option (checkbox) to ask for an UserID at launch, or change explicitly from this view
  the "set user" feature is disabled for now; data are expected to remain usable within the next version.

- [ ] synchronize database ("replicator"); add user ID in the DocID first;
  
  - [x] use System Prefs for the Server Settings (address/credential/password)
  - [x] can sync
  - [ ] present the feature to the User (where?)
    Note: the "synchronize" feature is functionnal but disabled for now; for exemple we need to precise (detail) the strategy

- [x] make entries as editable (ok, using double-tap inside an Entry) 
  
  - [x] update Levels when the Entry is not the last one

- [x] Remove action
  
  - [x] remove the first Entry (ok, using double-tap inside an Entry)
  - [x] remove the last Entry
  - [x] remove an Entry when it's alone 
  - [x] update Levels when the Entry is not the last one (toto: check)

- [x] add title to Digicode, for Add VS Moddify/Remove
  
  - [x] use a different View
  

## RoadMap (future version)

- swipe to change the view per day?
  
  - better do this: draw the whole current Day (curve) in a scrollView, synchronize scroll with tab/untab Entries/Dates

- display Tips to help waiting some durations (e.g.  sweep one room = 3mn, put away the dishes, ..). can sort these with durations. can ask for user how much such a duration was for him.

- currently, color for Entries TextFields is only for border -> try to apply to background (with alpha?)?

  - check applying a shadow on TextField?

- do group the entries (e.g. by 5) in the tabzone!
  
  - needs to map (time->xPosition) the last tabbed Entry
  
  - wait for the scroll view
