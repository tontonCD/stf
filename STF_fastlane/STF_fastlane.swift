//
//  STF_fastlane.swift
//  STF_fastlane
//
//  Created by Christophe Delannoy on 11/03/2022.
//  Copyright © 2022 Wan More. All rights reserved.
//

import XCTest

class STF_fastlane: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        
        // TEST, check dark_mode (disabled)
        // https://stackoverflow.com/questions/59447982/how-to-set-dark-mode-in-xcuiapplication-in-swift-uitests
        app.launchArguments.append("ForceDarkMode_Disabled")
        // ForceDarkMode_Enabled / ForceDarkMode_Disabled; @available(iOS 13.0, *)
        
        // TODO:
        app.launchArguments.append("ForceOrientation_Landscape")
        
        /*
         @seealso:
            if ProcessInfo().arguments.contains("SKIP_ANIMATIONS") {
                UIView.setAnimationsEnabled(false)
            }
         */
        
        // TODO:
        app.launchArguments.append("Show_SampleImages")
        
        
        setupSnapshot(app)
        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
                
        
//        let app = XCUIApplication()
        
        /*
        let graphicviewElement = app/*@START_MENU_TOKEN@*/.otherElements["GraphicView"]/*[[".scrollViews.otherElements[\"GraphicView\"]",".otherElements[\"GraphicView\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        graphicviewElement.tap()
        graphicviewElement.tap()
        app.buttons["chevron back circle outline"].tap()
        */
                
//        let graphicviewElement = app/*@START_MENU_TOKEN@*/.otherElements["GraphicView"]/*[[".scrollViews.otherElements[\"GraphicView\"]",".otherElements[\"GraphicView\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        graphicviewElement.tap()
//        graphicviewElement.tap()
//        app.windows.children(matching: .other).element.children(matching: .other).element.tap()
                                        

        print("Calling snapshot")
        snapshot("01LoginScreen")

        // Use recording to get started writing UI tests.
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
