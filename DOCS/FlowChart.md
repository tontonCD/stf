# STF FlowChart

## Object

The display must update (adding Dates, fixing zoom factor) according to 

- increasing the Current Time (moving to right)

- adding new Entries

## Abstract:

- adding an Entry:
  - makes it to display (as a new TextField)
  - computes the time for available next Entry (Green/Blue times)
  - check recovering, if so the zoom is increased
- updating the Current Time position:
  - can makes Dates (abscises) to be added
- if found recovering,
  - remove or Tab some Entries/Dates
- removing or Tabbing Entries/Dates
  - makes the Zoom factor to be updated

```mermaid
flowchart LR
      
      NewE(New Entry) --> CheckEntry{Still exists?}
      CheckEntry -->|Yes| E1(End)
      CheckEntry -->|No| NTF2[Build TextField]

      style NewE fill:#f9f,stroke:#333,stroke-width:4px
      style E1 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5

      NTF2[Build TextField] --> GB[computes new Gren/Blue Times]
      GB --> COvL
      DScore --> E2(end)
      CT(CurrentTime Update) --> UTF[Update CurrentTime Text Field]
      style CT fill:#f9f,stroke:#333,stroke-width:4px
      UTF --> TAddD{need to add Dates?}
      TAddD -->|No| E3(end)
      TAddD -->|Yes| AddD[Add Dates]
      AddD --> COvL{Check Overleap?}
      COvL --> |No| DScore[DrawScores]
      COvL --> |YES| TTF[Tab Some TF]
      TTF --> cRE{Remove Old Entries?}
      cRE --> |no| UpZ
      cRE --> |yes| RemE[Remove Entries]
      RemE --> cRD{Remove Dates?}
      cRD --> |yes| RemD[Remove Dates]
      RemE --> UpZ[Update Zoom]
      RemD --> UpZ
      UpZ --> DScore
```
