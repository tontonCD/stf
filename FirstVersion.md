# STF - First Version (En)

In the first version, the time you are advice to wait is compute from the twice previous events.

## exemple

Say that you took your <u>last</u> cigarette (the most recent) at **midday**. How much do you need to wait before the next?

According that the one before was

- at 11:00 AM (or earlier) -> you should wait 12:45 AM;
- at 11:00 PM -> you should wait 13:00 PM;

<img src="DOCS/ganttSTF.png" />

Talking about settings, this one suits for a delay after the C2 cigarette, as 

- <u>1h</u> by default, with the possibility it's reduced to <u>45mn</u>, **if** the previous (C1) is old enough.

This concept is realistic with case as morning time (we are less able to wait), or a more long abstinence (movie outing, ...)

This setting, here (1h00, -15mn), must match for you with usually, how much time after one you <i>ask yourself</i> for a next. If well fixed, you may frequently observe that if you expect to wait 5mn (doing something else) this will extend to 15mn without thinking about.  

The balance is, firstly a 10%-20% reduction. It's difficult to get more, at least it is stable.

## After?

Cigarettes are more closely spaced in the morning, until the third; this version still doesn't consider this. So why the V2.
