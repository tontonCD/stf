//
//  STF_NewTests.swift
//  STF_NewTests
//
//  Created by Developer White on 05/03/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import XCTest
@testable import STF_New


class STF_NewTests: XCTestCase {

    func _compare2(_ value1: Double, _ value2: Double) -> Bool    {
        let result = ( round(value1 * 100.0) / 100.0 )==value2
        if result == false {
            print("compare failed, \(round(value1 * 100.0) / 100.0 ) vs \(value2)")
        }
        return result
    }
    
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testRepair()   {
        var okTime =                    "09:54"
        WMUtils.repair(&okTime)
        XCTAssert( okTime.elementsEqual("09:54") )
        var wrongTime =                     "010:010"
        WMUtils.repair(&wrongTime)
        XCTAssert( wrongTime.elementsEqual( "10:10") )
    }

    func testLevel()    {
        // as Date
        let entries_Str: [String] =
        ["1899-12-31 04:50:00",
         "1899-12-31 06:08:00", "1899-12-31 08:23:00", "1899-12-31 09:54:00", "1899-12-31 11:20:00", "1899-12-31 13:45:00", "1899-12-31 15:20:00", "1899-12-31 17:55:00", "1899-12-31 19:34:00", "1899-12-31 21:45:00", "1899-12-31 23:52:00"]
        let expected: [Double] =
        [ 40,
          50.35, 44.85, 49.27, 51.10, 44.14, 48.51, 43.31, 47.79, 44.94, 44.98 ]

        // convert to Date, for the test
        var entries_Date: [Date] = [ ]
        for es in entries_Str   {
            entries_Date.append( WMDateUtils.date(fromFullDate: es)! )
        }
        
        WMLevelUtils.refDate = entries_Date[0]
        WMLevelUtils.refLevel = 40.0
        
        for hourIndex in 1..<entries_Date.count  {
            let newDate = entries_Date[hourIndex]
            let newLevel = WMLevelUtils.levelForAddition + WMLevelUtils.remainingLevelForDate(date: newDate)
            print( "\(entries_Str[hourIndex].dropFirst(11)) => \(newLevel)")
            
//            XCTAssert( ( round(newLevel * 100.0) / 100.0 )==expected[hourIndex] )
            XCTAssert( _compare2(newLevel, expected[hourIndex]) )
            
            WMLevelUtils.refDate = newDate
            WMLevelUtils.refLevel = newLevel
        }
        
        // as hours
        let entries_Hour: [Int] =
        [04*60+50, 06*60+08, 08*60+23, 09*60+54, 11*60+20, 13*60+45, 15*60+20, 17*60+55, 19*60+34, 21*60+45, 23*60+52]
        
        var entries_Minutes: [Int] = [ ]
        for es in entries_Str   {
            let hhmmss = String( es.dropFirst(11) )
            //var hh, mm: Int
            let (hh, mm): (Int, Int) = WMUtils.hhmmIntsFrom(hhmmss)
            entries_Minutes.append( hh*60 + mm )
        }

        
        WMLevelUtils.refMinutes = entries_Minutes[0]
        WMLevelUtils.refLevel = 40.0

        for hourIndex in 1..<entries_Date.count  {
            let newHour = entries_Minutes[hourIndex]
            let newLevel = WMLevelUtils.levelForAddition + WMLevelUtils.remainingLevelForHour(minutes: newHour)
            print( "\(entries_Hour[hourIndex]) => \(newLevel)")
            
//            XCTAssert( ( round(newLevel * 100.0) / 100.0 )==expected[hourIndex] )
            XCTAssert(  _compare2(newLevel, expected[hourIndex]) )
                
            WMLevelUtils.refMinutes = newHour
            WMLevelUtils.refLevel = newLevel
        }
    }

    func testLevelUtil() {
        
        
//        let test50 =  WMLevelUtils.reductionAfter(difference:  50)
//        XCTAssert( ( round(test50 *  100.0) / 100.0 )==0.61 )
//        let test70 =  WMLevelUtils.reductionAfter(difference:  70)
//        XCTAssert( ( round(test70 *  100.0) / 100.0 )==0.50 )
//        let test100 = WMLevelUtils.reductionAfter(difference: 100)
//        XCTAssert( ( round(test100 * 100.0) / 100.0 )==0.37)
        
        let test00 = WMLevelUtils.reductionAfter(difference:  0)
        XCTAssert( _compare2(test00, 1.00) )
        let test50 = WMLevelUtils.reductionAfter(difference:  WMLevelUtils.timeForHalflife)
        XCTAssert( _compare2(test50, 0.50) )
        
        let timeGreen =  WMLevelUtils.delayUntilGreen(fromLevel:  40)
        XCTAssert(
            // fix: +1 may have been added because, due to roundage, the drawn level may habeen wrong
            // !!!: possibly because we use two remainingLevelxxx() functions, one is wrong?
            _compare2(timeGreen,   WMLevelUtils.luGreenDelay) ||
            _compare2(timeGreen-1, WMLevelUtils.luGreenDelay)   )
        let timeBlue =  WMLevelUtils.delayUntilOrange(fromLevel:  40)
        XCTAssert(
            _compare2(timeBlue, WMLevelUtils.luBlueDelay) ||
            _compare2(timeBlue-1, WMLevelUtils.luBlueDelay  )
        )
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testTime()  {
        // PB: Date conversions (Date<->String) fail if the User Settings are on 12h (+AM/PM) VS 24h
        
        let nowDate: Date = Date()
        // e.g.             2021-12-11 14:44:30 +0000
        // or (the pb!):    2021-12-11 2:44:30 PM +0000
        
        let en_US_Posix_Locale: Locale = Locale.init(identifier: "en_US_POSIX")
        // see: Locale.availableIdentifiers
        
        let formatter: DateFormatter = DateFormatter.init()
        formatter.locale = en_US_Posix_Locale
        
        // formatter.dateFormat="yy-mm-ddTHH:mm:ss:.SSSZ"
        formatter.dateFormat="yy'-'mm'-'dd'T'HH':'mm':'ss':'.SSS'Z'"
        
//        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        
        // the Test, 1: check posix Locale
        let dateString: String = formatter.string(from: nowDate) // -> "21-46-11T14:46:42:.548Z"    "21-07-11T15:07:47:.157Z"
        print(dateString)
        // if works also with a simplier format:
        formatter.dateFormat = "yy-mm-dd HH:mm:ss"
        
        // the Test, 2:
        let aDateStr = "21-07-11 15:07:47"
        var aDate = formatter.date(from: aDateStr)
        XCTAssert( aDate != nil )
        let aDateStr2 = formatter.string(from: aDate!)
        XCTAssert( aDateStr2.elementsEqual(aDateStr) )
        
        // the  Test, 2; because this was reported to WMDateUtils
        
        aDate = WMDateUtils.date(fromFullDate: aDateStr)
        XCTAssert( aDate != nil )
        let hhmm = WMDateUtils.hoursString(date: aDate!)
        XCTAssert( hhmm.count == 8 )
        let yymmdd = WMDateUtils.dayDateString(date: aDate!)
        XCTAssert( yymmdd.count == 10 )
    }

}
