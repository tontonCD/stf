#  Hints

(Conseils)

Se réserver une pièce. Eviter en priorité le salon (devant la télé ?). Jamais dans la chambre. Dans tous les cas l'odeur persiste et vous incitera à fumer. Tenir compte des revètements de murs (tapisserie, ...), en général la cuisine (carrelages) convient.

Essayer de noter l'heure du lever (voir les options du digicode)(en cours), et de tenir 30mn. Dans un premier temps essayer 20mn ou moins et s'y tenir. Simultanément tenir 15mn après le déjeuner. Commencer plus bas (7mn) si nécessaire.

De même éviter de boire du café en même temps, au moins jusqu'à midi. 

Ne pas faire de jeux en même temps. Ni regarder la télé. S'efforcer de se concentrer sur ce que l'on fait, l'idéal est de ne pas rechercher le plaisir. 
En fait il revient, si on vient de passer une période de très faible consommation : profiter du moment puis oublier de le rechercher.




- Ranger le matériel (étuits/paquets, briquet, cendrier) dès que possible. Idéallement dès qu'il a servi.
D'abord on sera moins stimulé pour la consommation ; 
Il peut arriver qu'en le cherchant on pense sans s'en rendre compte à autre chose, ce qui en retardera l'usage.
Si on se dit en le trouvant "ah il est là ce p... de tabac", c'est une bonne chose.
De manière générale associer une corvée à la consommation aura pour conséquence de lui associer un moment désagréable, et d'en diminuer le besoin.
Et puis, on dit bien "loin des yeux, loin du coeur" !


En attendant :
Si l'attente vous paraît longue, boire de l'eau. Des anciens fumeurs m'on d'ailleurs raconté avoir complètement arrêté ainsi, seulement cela peut représenter plusieurs litres par jour.

Faire des pompes de skats, ...

Passer le balai,

ranger la vaisselle qui sèche sur l'évier,

arroser des plantes





