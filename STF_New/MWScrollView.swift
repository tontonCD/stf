//
//  MWScrollView.swift
//  STF_New
//
//  Created by Christophe Delannoy on 23/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

/* known issue:
 *  IBInspectable makes IB to crash;
 *  would requiere to update target, but will not compila because of not compatible with the current CDTDatastore version from cocoapod
 *  => commented "@IBDesignable"
 */

//@IBDesignable
class  MWScrollView: UIScrollView  {
    
//    var firstColorTest: UIColor = UIColor.systemGreen
    // po firstColorTest.cgColor
    //  white mode => sRGB IEC61966-2.1; extended range)] ( 0.203922 0.780392 0.34902 1 )
    //  Darkmode =>   sRGB IEC61966-2.1; extended range)] ( 0.188235 0.819608 0.345098 1 )
    
    
    /// baground color #1, for gradient
    @IBInspectable var firstColor: UIColor // = UIColor.red
    = UIColor(red: 0.79514, green: 0.998651, blue: 0.984532, alpha: 1)
    { didSet { updateColors() }            }
    // 0.79514 0.998651 0.984532 1
    
    /// baground color #2, for gradient
    @IBInspectable var secondColor: UIColor //= UIColor.green
    = UIColor.white
    { didSet { updateColors() }            } // systemBackgroundColor only iOS 13
    /* can see: https://developer.apple.com/design/human-interface-guidelines/ios/visual-design/color/#color-management) */
    
    
    @IBInspectable var vertical: Bool = true                { didSet { updateGradientDirection() } }

    /// color Gradient for the main View Background
    /* lazy */ var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.startPoint = CGPoint.zero // CGPoint(x: 1.0, y: 1.0)
        // layer.endPoint  -> defined in updateGradientDirection
        return layer
    }()

    
    //MARK: -

    required init?(coder aDecoder: NSCoder) {   // called from ApplicationMain
        super.init(coder: aDecoder)

        // background:
        applyGradientForApp()
    }

    override init(frame: CGRect) {              // called from IB (?)
        super.init(frame: frame)

//        applyGradientForIB()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
//        applyGradientForIB()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
        
        // fix for Dark Mode; (secondColor is (1, 1) in Normal and (0, 1) in Dark)
        updateColors()
    }

    //MARK: -
    
    func applyGradientForIB() {
        updateGradientDirection()

        layer.addSublayer(gradientLayer) // will mask the graphicView if in the App (but good for IB)
        //layer.insertSublayer(gradientLayer, at: 0)
    }
    func applyGradientForApp() {
        updateGradientDirection()
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }
    
    func updateColors() {
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
    }
    
    func updateGradientDirection() {
        gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        // note: layer.startPoint always CGPoint.zero
    }
    
}
