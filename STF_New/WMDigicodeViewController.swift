//
//  WMDigicodeViewController.swift
//  STF_New
//
//  Created by Christophe Delannoy on 16/11/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class WMDigicodeViewController: UIViewController {

    @IBOutlet weak var dvcDigicodeView: WMDigicodeView!
    
    
    // these depend on caller
    @IBOutlet weak var mvTitle:         UILabel!     // New Entry   Modify Entry
    @IBOutlet weak var mvOKButton:      UIButton!    // Add         (hidden)
    @IBOutlet weak var mvCancelButton:  UIButton!    // (symbol)    (symbol)
    
    @IBOutlet weak var mvSettingButton: UIButton!
    @IBOutlet weak var mvUserButton:    UIButton!
    @IBOutlet weak var mvSynchroButton: UIButton!
    
    // optional (only for "Modify"):
    @IBOutlet weak var mvRemoveButton:  UIButton!    // (hidden)    Remove

    
    var dvcParentViewController: WMAddViewDelegate? // set in prepareSegue (in previous viewController)
    var dvcInitialTime: String = "00:00"
    var dvcModifying = false
    
    // (EasterEgg)
    var dvcCanShowReplicator = false


    private func _localizedString(_ key: String) -> String  {
        let result = NSLocalizedString(key, tableName: "WMDigicodeViewController", bundle: Bundle.main, value: "", comment: "")
        return result
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        dvcDigicodeView.dcMainViewControler = self.dvcParentViewController
        dvcDigicodeView.setValue(hhmmValue: dvcInitialTime)

        

        if dvcModifying == false    {
            // new Entry
            mvTitle.text = _localizedString("New Entry")
            mvOKButton.setTitle(    _localizedString("Add"),        for: UIControl.State.normal)
            
            mvRemoveButton.isHidden = true
        } else                      {
            // modify Entry
            mvTitle.text = _localizedString("Modify Entry")
            mvOKButton.setTitle(    _localizedString("OK"),          for: UIControl.State.normal)
            
            mvRemoveButton.isHidden = false
            mvRemoveButton.setTitle(_localizedString("Remove"),       for: UIControl.State.normal)
        }
        
        // a default Title ("Button") is displayed on some, remove it:
        mvCancelButton.setTitle( "",        for: UIControl.State.normal)
        mvSettingButton.setTitle("",        for: UIControl.State.normal)
        mvUserButton.setTitle(   "",        for: UIControl.State.normal) // WMUser.uCurrentUser.uName?
        mvSynchroButton.setTitle("",        for: UIControl.State.normal)
        
        // TEMPS (needs code update, tag #V2)
        mvUserButton.isEnabled = false
        mvSynchroButton.isEnabled = dvcCanShowReplicator
  
        
        // test (issue: the window is not modal)
        // https://help.apple.com/xcode/mac/8.0/#/dev564169bb1
        mvSettingButton.contentMode = .scaleAspectFill
        // not the good way: it's an iPhone particularity -> ...?? see with the Segue
        //.scaleAspectFit
        
        dvcDigicodeView.dcMainViewControler?.digicodeAppearing = true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func buttonActionCancel (sender: UIButton, event: UIEvent)    {
        dvcDigicodeView.dcMainViewControler?.digicodeAppearing = false
        self.dismiss(animated: true)
    }
    
    @IBAction func buttonActionOK (sender: UIButton, event: UIEvent)        {
    
        if self.dvcParentViewController != nil {
            
            let textField = dvcDigicodeView.dcTextField
            
            if (textField!.text != nil)  {
                

                // EasterEgg; 4 digit, with 3 mandatory, and one precise the option.
                if textField!.text!.dropLast().elementsEqual("87:0") {

                    let egCode: Int = Int( textField!.text!.dropFirst(4) )!
                    /*
                     "87:00"    // removes all Entries
                     "87:01"    // inserts Dates from entries_Str, at today,
                     "87:02"    // inserts Dates from entries_Str, at yesterday
                     "87:03"    // inserts two Dates, for testing this: when one Entry tabbed and one Entry non-tabbed, check removing the first
                     "87:04"    // switches Synchronisation Enabled option
                     */

                    let title = "Special Feature (tests)"
                    let message = "Do you want to continue? (danger, don't if you don't know the effect)"
                    
                    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

                    alertController.addAction(UIAlertAction(title: "Yes", style: .destructive, handler:
                                                                {_ in
                        
                        if egCode==4    {
                            // display replicator button and return to digicode
                            self.dvcCanShowReplicator = !self.dvcCanShowReplicator
                            self.mvSynchroButton.isEnabled = self.dvcCanShowReplicator
                        } else {
                            // send code to previous controller
                            self.dvcParentViewController?.didEG(egCode)
                            self.dvcDigicodeView.dcMainViewControler?.digicodeAppearing = false
                            self.dismiss(animated: true)
                        }
                    })
                    )
                    alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil)) // Style.cancel??


                        self.present(alertController, animated: true) {
                        return
                    }
                }
                
                if dvcDigicodeView.filterForTime()==false  {
                    
                    dvcParentViewController?.didEnterCode(dateHM: dvcDigicodeView.dcTextField.text!)
                    dvcDigicodeView.dcMainViewControler?.digicodeAppearing = false
                    self.dismiss(animated: true)
                }
                
            } else {
                dvcParentViewController?.didCancel()
                dvcDigicodeView.dcMainViewControler?.digicodeAppearing = false
                self.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func buttonActionRemove  (sender: UIButton, event: UIEvent)    {
        dvcParentViewController?.didRemove()
        dvcDigicodeView.dcMainViewControler?.digicodeAppearing = false
        self.dismiss(animated: true)
    }
    @IBAction func buttonActionSettings(sender: UIButton, event: UIEvent)    {
        // useless (?)
    }
    @IBAction func buttonActionSynchro(sender: UIButton, event: UIEvent)    {
        // TODO: implement or use Segue
    }
}
