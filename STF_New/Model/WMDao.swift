//
//  WMDao.swift
//  STF_New
//
//  Created by Christophe Delannoy on 18/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import Foundation
import CDTDatastore

class WMDao //: NSObject
{
    /// userID, for persistance; could use a parameter with function, but we allo only one user per session
    static var userID: String {
        get {
            return WMSharedDatastore.dsUserID
        }
        set(userid) {
            WMSharedDatastore.dsUserID = userid
        }
    }
    

    
    // MARK:- WMEntity common functions
    
    static func saveEntity(entity: WMEntity) -> String {
        let rev : CDTDocumentRevision?
        if let previous = WMSharedDatastore.getDocument(withDocID: entity.eDocID!) {
            previous.body = NSMutableDictionary(dictionary:entity.encode() as [AnyHashable : Any])
            rev = WMSharedDatastore.updateDocument(fromRev: previous)
        } else {
            let newRev = CDTDocumentRevision(docId: entity.eDocID!)
            newRev.body = NSMutableDictionary(dictionary:entity.encode() as [AnyHashable : Any])
            rev = WMSharedDatastore.createDocument(fromRev: newRev)
        }
        
        return rev?.revId ?? "";
    }
    
    static func removeEntity(entity: WMEntity) -> String {
        var rev : CDTDocumentRevision? = nil
        if let _ /* previous */ = WMSharedDatastore.getDocument(withDocID: entity.eDocID!) {
            let result = WMSharedDatastore.removeDocument(withDocID: entity.eDocID!)
            if result.count > 0 {
                rev = result.first
            }
        }
        
        return rev?.docId ?? ""
    }

}
