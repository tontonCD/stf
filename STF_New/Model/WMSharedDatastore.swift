//
//  WMSharedDatastore.swift
//  STF_New
//
//  Created by Christophe Delannoy on 18/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import CDTDatastore

class WMSharedDatastore: NSObject {

    
    /// userID, for persistance; could use a parameter with function, but we allo only one User per session
    static var dsUserID: String = WMUser.uCurrentUser.uName
    
    /// Cahed data store.
    private static var cachedDatastore: CDTDatastore? = nil
    
    /// Datasotre manager
    private static var cachedDSManager: CDTDatastoreManager? = nil
    
    /// Create a CDTDatastore and associate it with to cachedDatastore
    private static var getSharedDatastore: CDTDatastore?  = {
        
        do {
            let fileManager = FileManager.default
            
            let documentsDir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).last!
            
            let storeURL = documentsDir.appendingPathComponent("cloudant-sync-datastore")
            let path = storeURL.path
            
            cachedDSManager = try CDTDatastoreManager(directory: path)
            // LOG:
            print("Database opened at \(path)")
            
            cachedDatastore = try cachedDSManager!.datastoreNamed("stf_datastore")
            
            return cachedDatastore
        }
        catch {
            print("Encountered an error: \(error)")
            return nil
        }
    } ()
    
    
    public static let sharedDatastore: CDTDatastore? = {
        if (cachedDatastore != nil)  {
            return cachedDatastore
        } else  {
            return getSharedDatastore
        }
    }()
    
    
    public static let sharedDatastoreManager: CDTDatastoreManager? = {
        if (cachedDSManager != nil)  {
            
            return cachedDSManager
        } else  {
            
            _ = WMSharedDatastore.sharedDatastore
            return cachedDSManager
        }
    }()
    
    
    override private init() {
        // L'initialisation est privé pour être sur qu'une seule instance sera créé
    }
    
    
    class func createDocument(fromRev: CDTDocumentRevision) -> CDTDocumentRevision?    {
        
        guard let datastore = WMSharedDatastore.sharedDatastore else {
            return nil
        }
        
        var document: CDTDocumentRevision?
        do
        {
            document = try datastore.createDocument(from: fromRev)
        } catch
        {
            document = nil
            NSLog("Error Creating document: %@", error.localizedDescription)
        }
        return document
    }
    
    
    class func updateDocument(fromRev: CDTDocumentRevision) -> CDTDocumentRevision?    {
        
        guard let datastore = WMSharedDatastore.sharedDatastore else {
            return nil
        }
        
        var document: CDTDocumentRevision?
        do
        {
            document = try datastore.updateDocument(from: fromRev)
        } catch
        {
            document = nil
        }
        return document
    }
    
    
    class func getDocument(withDocID: String) -> CDTDocumentRevision?    {
        
        guard let datastore = WMSharedDatastore.sharedDatastore else {
            return nil
        }
        
        do
        {
            let document = try datastore.getDocumentWithId(withDocID)
            return document
        } catch
        {
            return nil
        }
    }
    class func removeDocument(withDocID: String) -> Array<CDTDocumentRevision>   {
        
        guard let datastore = WMSharedDatastore.sharedDatastore else {
            return []
        }
        
        do
        {
            let documents: Array<Any> = try datastore.deleteDocument(withId: withDocID)

            if documents.count > 0  {
                let result: CDTDocumentRevision = documents.first as! CDTDocumentRevision
                return [result] // !!!: should loop, but no care...
            } else  {
                return []
            }
        } catch
        {
            return []
        }
    }

    
    class func getEntries(forDay: String) -> [CDTDocumentRevision]     {
        
        guard let datastore = WMSharedDatastore.sharedDatastore else {
            return []
        }
            
        var docIDs: [String] = []
        var docRevs: [CDTDocumentRevision] = []
        
        let allIDs = datastore.getAllDocumentIds()
        for anID in allIDs  {
            
            // check if this one belong to result;
            if anID.starts(with: forDay)    {
                
                docIDs.append(anID)
                
                do
                {
                    let documentRevision = try datastore.getDocumentWithId(anID)
                    docRevs.append(documentRevision)
                    // po documentRevision.docId    => Optional<String> "2021-07-21 04:50:00"

                } catch
                {
                    continue
                }
                
                
            }
        }
        
        return docRevs
    }
    class func getEntries(sinceDate: Date) -> [CDTDocumentRevision]     {
        
        guard let datastore = WMSharedDatastore.sharedDatastore else {
            return []
        }
            
        var docIDs: [String] = []
        var docRevs: [CDTDocumentRevision] = []
        
        let allIDs = datastore.getAllDocumentIds()
        print("did found \(allIDs.count) documents")
        for anID in allIDs  {
            
            var anID_Date: Date?
            
            var anID_DateStr: String = anID // default value using old-style docID ("date", not "type_userID_date_")
            var anID_userID:  String = ""   // default value using old-style docID (date_, not date_userID_)
            var anID_type: String
            
            // split anID to (anID_type, anID_userID, anID_DateStr)
            (type: anID_type, userID: anID_userID, dateStr: anID_DateStr) = WMEntry.splitDate(docID: anID)
                        
            anID_Date = WMDateUtils.date(fromFullDate: anID_DateStr)
            if anID_Date ==  nil    {
                // 1) wrong Date, remove? interesting to do, but did fall in troubles when checking the 12h-display
//                let test = WMSharedDatastore.removeDocument(withDocID: anID)
                // 2) @since 0.41 some IDs are for WMUser, don't remove!
                
                if anID_type.count == 0 {
                    // probably Type is not Entry (according to splitDate()),
                    // e.g. for anID=="user_Guest", splitDate() returns nil,
                    // not an issue here (expecting Entries)
                    continue
                }
                WMUtils.printWarning(from: "getEntries(): found wrong Date")
                continue
            }
            if !anID_type.elementsEqual(WMEntry.ENTITY_TYPE)   {
                if anID_type.count > 0  { // !!!: for now accept empty string
                    WMUtils.printWarning(from: "getEntries(): found empty string for Type")
                    continue
                }
            }
            
            // ignore other users then the Current one:
            if !anID_userID.elementsEqual(dsUserID)  {
                
                // note: it means that data using "" will be ignored **when** an userID is Set; this because a next defined User would choose to work with or not;
                
                if anID_userID.count > 0  { // another userID (not empty)
                    continue
                }
                // TODO: here, anID_userID is "", convert or remove (next version)
            }
            
            if anID_Date! >= sinceDate    {
                
                docIDs.append(anID)
                
                do
                {
                    let documentRevision = try datastore.getDocumentWithId(anID)
                    docRevs.append(documentRevision)
                    // po documentRevision.docId    => Optional<String> "2021-07-21 04:50:00"

                } catch
                {
                    continue
                }
                
                
            }
        }
        
        //return docIDs
        return docRevs
    }
}
