//
//  WMEntity.swift
//  STF_New
//
//  Created by Christophe Delannoy on 18/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

import Foundation
import CDTDatastore

protocol EntityCodable {
    
    init(fromDict: [String: Any?])
    
    func decode(from dict:[String: Any?])
    
    func encode() -> [String: Any?]
}


extension WMEntity {
    
    /// Save the document
    func save() -> String {
        return WMDao.saveEntity(entity: self)
    }
    /// Remove (delete) the document
    func remove() -> String {
        return WMDao.removeEntity(entity: self)
    }

}


//
class WMEntity : NSObject, EntityCodable {
    
    static let EDOCID_PROPERTY = "eDocID"
//    static let CREATED_PROPERTY = "created"
//    static let MODIFIED_PROPERTY = "modified"
    

    var eDocID : String? // ???: use enum
//    var created : TimeInterval?
//    var modified : TimeInterval?
    

    /// - parameter userID: optional, but if so must be used as an empty String
    public init(type: String) {
        
        let docID = type                // set to: "entry", "settings", ...

        self.eDocID = docID;
    }

    required init(fromDict dict:[String: Any?]) {
        super.init()
        decode(from: dict)
    }
    
    /// Codable cutomization
    func encode() -> [String: Any?]{
        var dict = [String: Any?]()
        dict[WMEntity.EDOCID_PROPERTY] = eDocID
//        dict[WMEntity.CREATED_PROPERTY] = created
//        dict[WMEntity.MODIFIED_PROPERTY] = modified
        return dict
    }
    
    func decode(from dict: [String: Any?]) {
        eDocID = dict[WMEntity.EDOCID_PROPERTY] as? String
//        created = dict[WMEntity.CREATED_PROPERTY] as? Double
//        modified = dict[WMEntity.MODIFIED_PROPERTY] as? Double
    }
    
}


