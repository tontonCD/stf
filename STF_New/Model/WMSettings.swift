//
//  WMSettings.swift
//  STF_New
//
//  Created by Christophe Delannoy on 09/12/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

import CDTDatastore


protocol WMSettingsProtocol: NSObject   {
    func settingChanged()    // if using WMGraphView instance
}


//class WMSettings: WMEntity {
class WMSettings: NSObject { // !!!: CHANGED, saved now within WMUser

    static let ENTITY_TYPE = "settings"
    
    static let SETTINGS_DelayOptimal_Property = "optimal"
    static let SETTINGS_DelayAllowed_Property = "allowed"
    static let SETTINGS_DayStartHour_Property = "dayStartHour"
    
    var _sDelayOptimal: Int = 70    // default value; @see WMLevelUtils:luGreenDelay
    var _sDelayAllowed: Int = 50    // default value; @see WMLevelUtils:luBlueDelay
    var sDelayOptimal: Int  {   // getter only; use setDelays()
        get {
            return _sDelayOptimal
        }
    }
    var sDelayAllowed: Int  {   // read only; use setDelays()
        get {
            return _sDelayAllowed
        }
    }
    
    var sDayStartHour: String = "00:00" // not used yet
    
    

    var sDelegate: WMSettingsProtocol? = nil
        
    
    // - MARK: utils
    
    func setDelays(optimal: Int, allowed: Int, dayStartHour: String)  {
        _sDelayOptimal = optimal
        _sDelayAllowed = allowed
        sDayStartHour = dayStartHour
        


        

        sDelegate?.settingChanged() // sDelegate is MainViewController
    }
    
    
    // - MARK: WMSetting Operation

    
    func encode() -> [String: Any?] {
        var dict: [String: Any?] = [:]
        
        dict[WMSettings.SETTINGS_DelayOptimal_Property] = _sDelayOptimal
        dict[WMSettings.SETTINGS_DelayAllowed_Property] = _sDelayAllowed
        dict[WMSettings.SETTINGS_DayStartHour_Property] = sDayStartHour
        
        return dict
    }
    
    func decode(from dict:[String: Any?]) {
        
        if let value = dict[Self.SETTINGS_DelayOptimal_Property] as? Int      {
            _sDelayOptimal = value
        }
        if let value = dict[Self.SETTINGS_DelayAllowed_Property] as? Int      {
            _sDelayAllowed = value
        }
        if let value = dict[Self.SETTINGS_DayStartHour_Property] as? String   {
            sDayStartHour = value
        }
    }
    
    
    override var description: String    {
        let result =
"""
{
    \(Self.SETTINGS_DayStartHour_Property): \(sDayStartHour)
    \(Self.SETTINGS_DelayOptimal_Property): \(_sDelayAllowed)
    \(Self.SETTINGS_DelayAllowed_Property): \(_sDelayOptimal)
}
"""
//        let result = String.init(format: descFormat, sDayStartHour, _sDelayAllowed, _sDelayOptimal)
        return result
    }
}

