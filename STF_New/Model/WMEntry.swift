//
//  WMEntry.swift
//  STF_New
//
//  Created by Christophe Delannoy on 19/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

//import UIKit
import Foundation

import CDTDatastore


enum WMEventType: String {
    case
        wakup =  "wakeup",  // wake up; the user may choose to apply a delay from this (unimplemented)
        normal = "normal",  // e.g. tobacco
        fake =   "fake"     // e.g. vape
}


class WMEntry: WMEntity, Comparable
{
    static func < (lhs: WMEntry, rhs: WMEntry) -> Bool {
        return ( lhs.eDocID!.compare(rhs.eDocID!)==ComparisonResult.orderedAscending )
    }

    static let ENTITY_TYPE = "entry"
    
    static let ENTRY_what_PROPERTY =  "what"
    static let ENTRY_who_PROPERTY =   "who"
    static let ENTRY_levelBefore_PROPERTY = "levelBefore"
    static let ENTRY_levelAfter_PROPERTY = "level"

    var what: WMEventType? = WMEventType.normal
    var who:  String? = "" // "simulator" or "device", set later
    
    var levelAfter:  Double = -1.0 // "-1" means: needs to be compute from the previous Entry
    var levelBefore: Double = -1.0

    init(userID: String, date: String)  {
        
        // docID will be as "entry_Rob_2021-11-09 20:30:00"
        
        super.init(type: Self.ENTITY_TYPE)
        // -> docType set to:                       "entry"
        
        eDocID = "\(eDocID!)_\(userID)"     // adds:     "_Rob"
        eDocID =  "\(eDocID!)_\(date)"      // adds:         "_2021-11-09 20:30:00"
        // note: '!' is requiered,
        //  as the result may have been something like this: "Optional(\"entry_\")_2021-12-10 23:59:00"
        
        // changed: if no userID, the result was "entry__2021-12-10 23:52:00"
        //          actualy, using "Guest" (=> "entry_Guest_2021-12-10 23:52:00")
        
        if TARGET_OS_SIMULATOR != 0 {
            who = "simulator"
        } else                      {
            who = "device"
        }
    }
    
    required init(fromDict dict: [String : Any?]) {
        super.init(fromDict: dict)
        // decode(from: dict) // useless, was called from Entity.init(...)
    }
    
    override func decode(from dict:[String: Any?]) {
        super.decode(from: dict)
        what =  WMEventType(rawValue: dict[WMEntry.ENTRY_what_PROPERTY] as? String ?? "normal")
        who =   dict[WMEntry.ENTRY_who_PROPERTY] as? String
        levelBefore = dict[WMEntry.ENTRY_levelBefore_PROPERTY] as? Double ?? -1.0
        levelAfter =  dict[WMEntry.ENTRY_levelAfter_PROPERTY]  as? Double ?? -1.0
    }
    
    override func encode() -> [String: Any?] {
        var dict = super.encode();
        dict[WMEntry.ENTRY_what_PROPERTY] =  what?.rawValue
        dict[WMEntry.ENTRY_who_PROPERTY] =   who
        dict[WMEntry.ENTRY_levelBefore_PROPERTY] = levelBefore
        dict[WMEntry.ENTRY_levelAfter_PROPERTY]  = levelAfter

        return dict
    }

    
    // - MARK: WMEntry Operation
    
    static func loadEntry(entryID: String) -> WMEntry? {

        if let entryDoc: CDTDocumentRevision = try?
            WMSharedDatastore.sharedDatastore?.getDocumentWithId(entryID) {
            
            let entry = WMEntry(fromDict: entryDoc.body as! [String: Any?])
            return entry
        }
        return nil;
    }
    
    static func saveEntry(entry: WMEntry) -> String {
        return WMDao.saveEntity(entity: entry)
    }
    
    // - MARK: helpers
    
    /// load Entries, using Day specification
    static func loadEntries(forDay: String) -> [WMEntry] {
        
        var entries: [WMEntry] = []
        
        let entryDocs: [CDTDocumentRevision] = WMSharedDatastore.getEntries(forDay: forDay)
        
        for entryDoc in entryDocs   {
            let entry: WMEntry = WMEntry(fromDict: entryDoc.body as! [String: Any?])
            entries.append(entry)
        }
        return entries
    }
    /// load Entries, using Interval specification
    static func loadEntries(timeIntervalFromNow: TimeInterval) -> [WMEntry] {
        
        var entries: [WMEntry] = []
        
        let dateLimit = Date(timeIntervalSinceNow: -timeIntervalFromNow)
        let entryDocs: [CDTDocumentRevision] = WMSharedDatastore.getEntries(sinceDate: dateLimit)
        
        for entryDoc in entryDocs   {
            let entry: WMEntry = WMEntry(fromDict: entryDoc.body as! [String: Any?])
            entries.append(entry)
        }
        return entries
    }

    
    
    // - MARK: utils
    
    /// split an ID to elements (e.g. "entry\_Rob\_2021-11-09 20:30:00" => ("entry", "Rob", "2021-11-09 20:30:00")
    static func splitDate(docID: String) -> (type: String, userID: String, dateStr: String)    {
        
        let docID_Split = docID.split(separator: "_")
        
        var docID_Split_Type =     ""
        var docID_Split_UserName = ""
        var docID_Split_Date =     ""
        
        // docID should be as "entry_Rob_2021-11-09 20:30:00"
        // for legacy entries, we don't know if the dateString comes first, try all positions:
        for splitIndex in 0..<docID_Split.count {
            docID_Split_Date = String( docID_Split[splitIndex] )
            if let _ /*fullDate: Date*/ = WMDateUtils.date(fromFullDate: docID_Split_Date)  {
                if        splitIndex >= 2   {  // as expected, e.g. "entry_Rob_2021-11-09 20:30:00"
                    docID_Split_Type = String( docID_Split[0] )
                    docID_Split_UserName =  String( docID_Split[1] )
                } else if splitIndex >= 1   {  // no userID,   e.g. "entry__2021-11-09 20:30:00"
                    docID_Split_Type = String( docID_Split[0] )
                }
                return (type: docID_Split_Type, userID: docID_Split_UserName, dateStr: docID_Split_Date)
            }
        }
        return (type: docID_Split_Type, userID: docID_Split_UserName, dateStr: docID_Split_Date)
    }
    
    func getDate() -> Date?    {
        // can be as: "entry_Rob_2021-11-09 20:30:00", "entry__2021-11-09 20:30:00", "2021-11-09 20:30:00"
                
        let (type: _, userID: _, dateStr: dateString) = WMEntry.splitDate(docID: eDocID!)
        if let fullDate: Date = WMDateUtils.date(fromFullDate: dateString)  {
            return fullDate
        }
        
        return nil
    }
}
