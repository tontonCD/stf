//
//  WMUser.swift
//  STF_New
//
//  Created by Christophe Delannoy on 09/12/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

import CDTDatastore


class WMUser: WMEntity {

    static let ENTITY_TYPE = "user"
    
    static let ENTRY_Settings_Property = "settings"
    static let ENTRY_Name_Property =     "name"
    
    /// const userID for Guest User
    private static let uGuestUserID = "Guest"
    
    var uName = ""
    var uSettings: WMSettings = WMSettings()

    static var uCurrentUser: WMUser = WMUser(fromDict: [:])
    static var uCurrentUserName: String = ""
    
    /// - parameter fromDict: a Dictionnary, can be empty (never nil), in wich case this user won't be useable.
    required init(fromDict dict: [String : Any?]) {
        super.init(fromDict: dict)
        if let value: String = dict[Self.ENTRY_Name_Property] as? String    {
            uName = value
        }
    }
    
    
    
    
    /// init as default (doesn't load)
    init(userID: String)  {
        
        // docID will be as "user_Guest"

        super.init(type: Self.ENTITY_TYPE)
        uName = userID
        eDocID = "\(eDocID!)_\(userID)" // ok
    }
        
    
    static func loadUser(userName: String) -> WMUser? {

        let docID = "\(Self.ENTITY_TYPE)_\(userName)"
        if let userDoc: CDTDocumentRevision =
            try? WMSharedDatastore.sharedDatastore?.getDocumentWithId(docID) {
            
            let user = WMUser(fromDict: userDoc.body as! [String: Any?])
            return user
        }
        return nil;
    }
    
    static func saveUser(user: WMUser) -> String {
        return WMDao.saveEntity(entity: user)
    }

    override func decode(from dict:[String: Any?]) {
        super.decode(from: dict)
        
        if let value = dict[Self.ENTRY_Settings_Property] as? [String: Any?]  {
            uSettings.decode(from: value)
        }

    }
    
    
    
    
    override func encode() -> [String: Any?] {
        var dict = super.encode();
     
        dict[Self.ENTRY_Settings_Property] = uSettings.encode()
        dict[Self.ENTRY_Name_Property] =     uName

        return dict
        // po [NSJSONSerialization isValidJSONObject:dict]
        // po JSONSerialization.isValidJSONObject(dict)
    }
        
    /// loads a currentUser from UserDefaults specification, and ensure that at least a default value is set in UserDefaults
    static func currentUserFromUserDefault() -> WMUser  {
        
        let userDefaults: UserDefaults = UserDefaults.standard
        var currentUserName = userDefaults.string(forKey: "currentUser")
        
        // check UserDefaults (from system)
        if currentUserName==nil || currentUserName?.count==0    {

            currentUserName = Self.uGuestUserID // FIX: currentUserName was ""
            userDefaults.setValue(currentUserName, forKey: "currentUser")
            userDefaults.synchronize()
        }
        
        // load if necessary
        if !uCurrentUserName.elementsEqual(currentUserName!)   {

            if let test = Self.loadUser(userName: currentUserName!) {
                uCurrentUser = test
                
                // FIX:
                if test.uName.count==0  {
                    // was not saved in a previous version, set it:
                    test.uName = Self.uGuestUserID
                    _ = test.save()
                    // the fix will be only executed once
                }
                
                uCurrentUserName = test.uName
            } else {

                uCurrentUser = WMUser.init(userID: currentUserName!)
                uCurrentUserName = uCurrentUser.uName
                _ = uCurrentUser.save()
            }
            
            WMLevelUtils.settingChanged()
        }
        
        return uCurrentUser
    }
    
    
    func setDelays(optimal: Int, allowed: Int, daySartHour: String)  {
        
        uSettings.setDelays(optimal: optimal, allowed: allowed, dayStartHour: daySartHour)
        // this will update WMLevelUtils
        
        _ = self.save()
    }
    
    
    override var description: String    {
        let result =
"""
{
    \(Self.ENTRY_Name_Property): \"\(uName)\"
}
"""

        return result
    }
}
