//
//  AppDelegate.swift
//  STF_New
//
//  Created by Developer White on 05/03/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?   // FIX: For iOS 12
    
    /// copies Settings from Settings.Bundle/Root.plist to UserDefaults
    func registerDefaultsFromSettingsBundle() {
        let userDefaults: UserDefaults = UserDefaults.standard
        let fileName: String = "Root"
        
        
        let filePath: String = Bundle.main.path(forResource: fileName, ofType: "plist", inDirectory: "Settings.bundle") ?? ""
        let fileUrl: URL = URL.init(fileURLWithPath: filePath)
        
        var settingsData: Data
        do {
            try settingsData = Data.init(contentsOf: fileUrl)
        } catch
        {
            return
        }
        
        var format = PropertyListSerialization.PropertyListFormat.xml
        guard let plistDict: Dictionary<String, AnyObject> = try! PropertyListSerialization.propertyList(from: settingsData, options: .mutableContainersAndLeaves, format: &format) as? [String : AnyObject] else { return
            
        }
        
        guard let plistDictArray: Array<Dictionary<String, Any>> = plistDict["PreferenceSpecifiers"] as? Array<Dictionary<String, Any>> else    {
            return
        }

        var defaultsToRegister: Dictionary<String, Any> = [:];
        for spec: Dictionary<String, Any> in plistDictArray    {
            if let key: String = spec["Key"] as? String    {
                defaultsToRegister[key] = spec["DefaultValue"];
            }
        }
        
        userDefaults.register(defaults: defaultsToRegister)
    }
    
    /// for tests, check that Server settings are present
    func getServer() -> (String, String, String)   {
        
        let userDefaults: UserDefaults = UserDefaults.standard
        let serverAddress:  String = userDefaults.string(forKey: "sync_remote_database")    ?? ""
        let serverAccount:  String = userDefaults.string(forKey: "sync_account_identifier") ?? ""
        let serverPassword: String = userDefaults.string(forKey: "sync_account_password")   ?? ""
        return (serverAddress, serverAccount, serverPassword)
    }

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        registerDefaultsFromSettingsBundle()
        
        // test:
        // var (address, account, password) = getServer()
        // note: not called yet
                
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
//    func application(_ application: UIApplication, didChangeStatusBarOrientation oldStatusBarOrientation: UIInterfaceOrientation) {
//        // just log (frequently usefull for debug)
//        print("didChangeStatusBarOrientation")
//    }

//    func applicationDidEnterBackground(_ application: UIApplication) {
//        //
//    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        //
    }
    func applicationWillResignActive(_ application: UIApplication) {
        //
    }
    func application(_ application: UIApplication, willEncodeRestorableStateWith coder: NSCoder) {
        //
    }
}

