//
//  MainViewController.swift
//  STF_New
//
//  Created by Developer White on 05/03/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit



@available(iOS 12.0, *)
class MainViewController: UIViewController,
                          WMAddViewDelegate,
                          WMUserViewControllerDelegate,
                          WMSettingsProtocol
//                      , UITapGestureRecognizer
{

    
    @IBOutlet weak var mvGraphicView: WMGraphicView!
    @IBOutlet weak var mvScrollView:  UIScrollView!
    
    var mvTimeTextFields: [UITextField]! = []
    
    var mvCurrentEditedTextField: MWEntryTextField? = nil
    
    var digicodeAppearing: Bool = false
    
    // WMUserViewControllerDelegate
    var currentUser: WMUser 
    
    required init?(coder: NSCoder) {
        currentUser = WMUser.currentUserFromUserDefault()
        
        super.init(coder: coder)
        
        // note: the view not load yet, so mvGraphicView not initialised
        
        // !!!: reprendre tuto,
        // let nibNameSafe = self.getNibName()
    }
    
    /// checks if some ViewControllers has been displayed or not; if not does it.
    /// - NOTE: only on VC at a time, but when a VC is dismissed, we come back into this function because of viewDidAppear
    func _checkOptViewController()  { // renamed, was: _checkDisclaimer
        
        _ = WMOptionalViewController.presentIfNeverSeen(identifier: "disclaimer", fromController: self)
        // TODO: check, add other Controllers and remove this one
        
        _ = WMOptionalViewController.presentIfNeverSeen(identifier: "whatisthis", fromController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // self.accessibilityIdentifier = "MainViewController"
        
        currentUser.uSettings.sDelegate = self
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // todo: check is sameUser implemented, and true/false
        
        // TEST
        let horizontalSizeClass = self.view.traitCollection.horizontalSizeClass
        let verticalSizeClass = self.view.traitCollection.verticalSizeClass
        // -> compact or regular
        
        // the graphicView also did display
        mvGraphicView.gvNeedsZoomUpdate = true
        mvGraphicView.setNeedsDisplay()
        
        _checkOptViewController()
    }
    
    // MARK: - Navigation

    /// prepares for the Digicode View
    /// - note: replaces didTapView(_ sender:...)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        
        
        let digicodeViewController: WMDigicodeViewController = segue.destination as! WMDigicodeViewController
        digicodeViewController.dvcParentViewController = self
        
        var dateHM:String? = nil
        if let gestureRecognizer = sender as? UITapGestureRecognizer    {
            let whereInGraphView: CGPoint = gestureRecognizer.location(in: self.mvGraphicView)
            // !!!: check, possible to better use (in: self.mvScrollView) when enabled scrollView Feature,
            // currently the result is the same
            
            mvCurrentEditedTextField = mvGraphicView.findEntryTextField(where: whereInGraphView)
            if mvCurrentEditedTextField != nil  {
                
                //let distanceToCurrentDate = Date().timeIntervalSince(mvCurrentEditedTextField!.date)
                
                dateHM = mvCurrentEditedTextField!.text // or: .getHHMMString()
                digicodeViewController.dvcModifying = true
            } else                              {
                digicodeViewController.dvcModifying = false
            }
            digicodeViewController.dvcInitialTime = _prepareDigicode(dateHM)
        }

    }
    
    
    private func _prepareDigicode(_ dateHHMM: String? = nil) -> String   { 
        
        /* prepare to display Digicode */
        
        if dateHHMM == nil  {

            /* get hours/minutes */
            let now: Date = Date()
            let dateString = String( WMDateUtils.hoursString(date: now).dropLast(3) )
            
            
            return dateString
            
        } else {
            
            return dateHHMM!
        }
        
    }
    
    
    
    // MARK: WMAddViewDelegate
    
    /// called from Digicode: user wants to add an Entry
    /// - parameter dateHM: a String whose format is as "12:34"
    func didEnterCode(dateHM: String) {
        
        // convert the 4-digits Date as a Full Date

        var wholeDateString: String = WMDateUtils.fullDateToday(dateHM: dateHM)
        
        // check: add a new Entry, or edit an existing one?
        if mvCurrentEditedTextField != nil  {
            
            /* ** EDITING an existing Entry ** */
            // PB: it's also the DocID!!! => remove then add again
            // PB2: can changed the order in entriesTextFields
            
            let currentEdited_DateHM = WMDateUtils.hoursString(date: mvCurrentEditedTextField!.date).dropLast(3)
            let currentEdited_DateDay = WMDateUtils.dayDateString(date: mvCurrentEditedTextField!.date)
            
            // TIP: if the value was not changed => just force Redraw
            if dateHM.elementsEqual(currentEdited_DateHM)  {
                
                // FIX: don't ignore, don't change the value, but make mvGraphicView to redraw
                mvGraphicView.updateZoom()
                mvGraphicView.setNeedsDisplay()
                return
            }
            
            // a) remove current from Display
            let rIndex = mvGraphicView.removeFromEntriesTextFields(entry: mvCurrentEditedTextField!.etfEntry!)
            if rIndex ==  -1 {
                // failed to find the Entry
                WMUtils.printError(from: "didEnterCode() didn't find current Entry")
                
                return
             }
            
            // b) remove current from database; this can occur on a tabbed element (will update _dtLeftTabNum)
            let currentEntry = mvCurrentEditedTextField!.etfEntry
            // fix: the current may be tabbed, then untabbed in the operation, save this information
            let current_isTabbed = mvCurrentEditedTextField!.adtfTabbed
            _ = currentEntry?.remove()

            
            // c) new Entry
            wholeDateString = "\(currentEdited_DateDay) \(dateHM):00"

            
            let newEntry: WMEntry = WMEntry(userID: currentUser.uName, date: wholeDateString) // !!!: shoud be WMUser.uCurrentUser.uName
           
            if mvGraphicView.findInEntriesTextFields(entry: newEntry) == false {
             
                _ = newEntry.save()
                mvGraphicView.buildTFieldForNewEntry(entry: newEntry, tabbed: current_isTabbed)
            }
            
            // d) CHECK: compute level from minDate(rIndex, new)

        } else {
            
            /* ** NEW Entry ** */
            
            // fix, if creating a date after midnight while we are before (or the inverse) could produce a wrong Entry:
            var wholeDate : Date = WMDateUtils.date(fromFullDate: wholeDateString)!
            let diff = wholeDate.timeIntervalSinceNow
            if         diff > 18 * 3600 {
                wholeDate = Date(timeInterval: -24 * 3600, since: wholeDate)
                wholeDateString = WMDateUtils.fullDateString(date: wholeDate)
            } else if -diff > 18 * 3600 {
                wholeDate = Date(timeInterval:  24 * 3600, since: wholeDate)
                wholeDateString = WMDateUtils.fullDateString(date: wholeDate)
            }
            // TODO: when editing, did we keep the full date (with Day)?
            // NOTE: if using a Date Widget this may solve a lot of this
            
            let newEntry: WMEntry = WMEntry(userID: currentUser.uName, date: wholeDateString)
           
            // don't create if exists
            if mvGraphicView.findInEntriesTextFields(entry: newEntry) == false {
             
                _ = newEntry.save()
                mvGraphicView.buildTFieldForNewEntry(entry: newEntry)
            }
            
        }
        
    }
    
    func didCancel() {
    }// useless now, requiered for protocol // ???: check if can remove from protocol
    
    func didEG(_ code: Int)     { // easterEgg
        _ = mvGraphicView.insertDataset(numSet: code)
    }
    func didRemove()    {
        if mvCurrentEditedTextField == nil  {
            WMUtils.printError(from: "didRemove()")
            return
        }
        
        // remove from Display
        let rIndex = mvGraphicView.removeFromEntriesTextFields(entry: mvCurrentEditedTextField!.etfEntry!)
        if rIndex ==  -1 {
            // failed to find the Entry
            WMUtils.printError(from: "didRemove(), Entry not found)")
         }
        
        // remove from Database
        let currentEntry = mvCurrentEditedTextField!.etfEntry!
        _ = currentEntry.remove()
        
        return
    }
    
    // MARK: - Actions
    
    
//    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
//        print("didRotate)")
//    } // never called, perheaps in iOS prior to 9? 8? -> deprecated, use viewWillTransition
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        print("(viewWillTransitionToSize called)")

        mvGraphicView.updateZoom(newViewSize: size)
        mvGraphicView.setNeedsDisplay()
        
        // TEST, set currentOrientation (see supportedInterfaceOrientations)
        // let test: UIInterfaceOrientationMask =
    }

    // MARK: - WMUserViewControllerDelegate
    func didSetCurrentUser(user: WMUser) {
        currentUser = user
        
        _ = mvGraphicView.loadEntries(forUserID: currentUser.uName)
        // Check; updateZoom() called?
        
        self.settingChanged()
    }
    
    // never called? Yes if dismissing Disclaimer
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            return UIInterfaceOrientation.landscapeLeft
        }
    }
    
    /*
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {

            // TODO: sometimes when landscape, and Add called, the view changes to Portrait
            var result = UIInterfaceOrientationMask.init()
            // can need this?:
            // let currentFrame = self.view.frame
            // seealso viewWillTransition
            // IDEA: set possibleOrientations to current from here, set null when back
            
            if  mvGraphicView == nil || mvGraphicView.gvCanRotate_Vertical == false
            {
                result = UIInterfaceOrientationMask.landscape
            }
//            else if digicodeAppearing == true {
//                // better check the currentOrientation
//                result = UIInterfaceOrientationMask.landscape
//            // pb: when closing Digicode, changes to Portrait
//            }
            else                              {
                result = super.supportedInterfaceOrientations
            }
            
            // TEST:
            var canChange: Bool = true
            var result2: UIInterfaceOrientationMask
            if mvGraphicView == nil || mvGraphicView.gvCanRotate_Vertical
            {    canChange = false;}
            if canChange
            {    result2 = super.supportedInterfaceOrientations}
//            else
//            {    result2 = current}
            // see also canRotate
                
            
            return result
        }
        
    }
    */
    // NEW (check)
    override var shouldAutorotate: Bool {
        return (self.digicodeAppearing==false)
    }
    
    // MARK: - WMSettingsProtocol
    func settingChanged() {
        
        mvGraphicView.settingChanged()
        
        // update graph (curve):
        mvGraphicView.setNeedsDisplay()
    }

}

