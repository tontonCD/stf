//
//  WMDateUtils.swift
//  STF_New
//
//  Created by Christophe Delannoy on 18/08/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import Foundation

class WMDateUtils   {
    
    private static let _en_US_Posix_Locale: Locale = _en_US_POSIX_Locale()
    
    
    private static let _fullDateFormatter: DateFormatter = WMDateUtils._dateFormatterWith(format: "yyyy-MM-dd HH:mm:ss")

    private static let _hoursDateFormatter : DateFormatter = WMDateUtils._dateFormatterWith(format: "HH:mm:ss")
    
    private static let _dayDateFormatter : DateFormatter = WMDateUtils._dateFormatterWith(format: "yyyy-MM-dd")

    
    
    private static func _en_US_POSIX_Locale() ->  Locale                        {
        return Locale.init(identifier: "en_US_POSIX")
    }
    
    /// builds a DateFormatter, always in the "24h display" mode
    private static func _dateFormatterWith(format: String) ->  DateFormatter    {
        let dateFormatter: DateFormatter = DateFormatter()
        /* PB: if System Settings are "24h display", will use "... AM/PM"
         Locale.current:
         device     -> "fr_FR", "current"
         simulator  -> "fr_FR", "current"
         */

        dateFormatter.locale = _en_US_Posix_Locale
        dateFormatter.dateFormat = format // e.g. "yy-mm-dd HH:mm:ss"
        
        // dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        // -> no, will lost the "time zone" offset
        
        return dateFormatter
    }
    
    /// object: manage "12h display" as well as "24 display" user setting; results are reported into the functions bellow
    static func test24h()  {
        // example:
//        NSString *format = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
//        BOOL is24Hour = ([format rangeOfString:@"a"].location == NSNotFound);
        var format: String = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current) ?? ""
//        let is24HourRange: Range? =  format.range(of: "a")// == NSNotFound
        if format.contains("a") {
            format = String( format.dropLast(2) )
        }
        /*
         "h"    => "h a"
         "H"    => "HH"
         "k"    => "HH"
         "K"    => "h a" ;
         iPhone 24h->"HH \'h\'"
         iPhone 12h->"h a"
         */
        let dtf = DateFormatter.init()
        dtf.setLocalizedDateFormatFromTemplate("j")
    }
    
    // MARK: - conversion String->Date
    
    /// - returns: a Date instance
    /// - parameter string: a Full-Date String, as "yyyy-MM-dd HH:mm:ss"
    /// - note: in the case of a bad parameter (e.g. "15:67") returns nil
    static func date(fromFullDate: String) -> Date?    {
        return _fullDateFormatter.date(from: fromFullDate) // ?? Date()
    }
    
    // MARK: - conversion Date->String
    
    /// - returns: the full Date String, as "yyyy-MM-dd HH:mm:ss"
    /// - parameter date: a Date instance
    static func fullDateString(date: Date) -> String    {
        return _fullDateFormatter.string(from: date)
    }
    /// - returns: a String, as "HH:mm:ss"
    /// - parameter date: a Date instance
    static func hoursString(date: Date) -> String    {
        // let test = date.formatted(date: Date.FormatStyle.DateStyle.complete, time: Date.FormatStyle.TimeStyle.complete)
        return _hoursDateFormatter.string(from: date)
    }
    /// - returns: a String, as "yyyy-MM-dd"
    /// - parameter date: a Full-Date String
    static func dayDateString(date: Date) -> String    {
        //let dayString = _dayDateFormatter.string(from: date)
        return _dayDateFormatter.string(from: date)
    }
    
    
    // MARK: - utilities
    
    /// - returns: a Date from the parameter, setting HH:mm:ss to 00:00:00; please note of UTC effect
    /// - parameter fromDate: a Date instance
    static func dateAtMidnight(fromDate: Date) -> Date    { // "yyyy-MM-dd"
        let dayString = _dayDateFormatter.string(from: fromDate)
        let dayDate: Date = _dayDateFormatter.date(from: dayString)!
        return dayDate
    }
    /*
    static func fullHour(HH: Int) -> Date    {
        
        let hhString = WMUtils.hhmmStringFrom(HH, 00)
        return fullHour(HH: hhString)
    }
    static func fullHour(HH: String) -> Date    {
        
        let fullHourString = "\(HH)"
    }
     */

    /// - returns: the Full Date, as a String, from a partial date, i.e. adds "yyyy-MM-dd" (taken at "now") left to the "HH:mm" parameter
    /// - parameter dateHM: a String describing "HH:mm"
    /// - note: proceed with concatenation, the result may not be a valid date
    static func fullDateToday(dateHM: String) -> String    {
    
        let dateFormatter : DateFormatter = DateFormatter()
        // TODO: check if compatible with 12h-display setting -> OK
        let now: Date = Date()
        dateFormatter.dateFormat = "yyyy-MM-dd" // e.g. "yyyy-MM-dd HH:mm:ss"
        let dateYMD = dateFormatter.string(from: now)
        let wholeDateString = "\(dateYMD) \(dateHM):00"
        
        return wholeDateString // OK as 24h Display
    }

}
