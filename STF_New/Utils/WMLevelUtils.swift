//
//  WMLevelUtils.swift
//  STF_New
//
//  Created by Christophe Delannoy on 18/08/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

enum WMStfLevels: Double { // TODO: not used yet, will replace alertLevel and warningLevel
    case
        orangeLevel = 80.0, // red   if highter
        greenLevel =  10.0   // green if lower
}


class WMLevelUtils: NSObject /*, WMSettingsProtocol */   {
    
    /// settings: delay for optimal/accepted
    static var luBlueDelay =  Double( WMUser.uCurrentUser.uSettings.sDelayAllowed )   //   50 mn ; accepted delay
    static var luGreenDelay = Double( WMUser.uCurrentUser.uSettings.sDelayOptimal )   // 1h10 mn ; optimal delay
    ///note: their are static. In a future version we may choose to show more than one user at a time
    
    /// refDate for Level calculation, for use with levelForDate(date: Date)
    static var refDate: Date = Date()
    // refHour as 60*hh + mm, alternative method, for use with levelForMinutes(minutes: Int)
    static var refMinutes: Int = 0
    
    static var refLevel: Double = -1.0
    
    static var levelForAddition: Double = 40.0 // added level for each Entry
    
    /* settings
     * - say that: recommended = 70mn, accepted = 50mn
     * => Orange after 50mn, Green after 70mn; check;
     */
    /// time (mn) for which Level divided by two; recommended is 120mn (2 hours)
    static var timeForHalflife:  Double = 40.0 // in mn; 120 for 2-hours
    
    
    /// if level is lower, color is set to Green
    static var warningLevel: Double // = alertLevel - levelForAddition
        = remainingLevel(startLevel: levelForAddition, afterMn: luGreenDelay)
//        = levelForAddition // +levelForAddition // i.e. x2, because if respecting "recommended" delay, x=(x/2+L) converges to 2xL
    /// if level is higher, color is set to Red
    static var alertLevel:   Double
//        = 80.0
        = remainingLevel(startLevel: levelForAddition, afterMn: luBlueDelay)
//        = remainingLevel(startLevel: levelForAddition, afterMn: 50.0) * 2 // i.e. x2, because if respecting "accepted" delay, x=(x/2+L) converges to 2xL

    
    static var redLevelColor =     UIColor.red
    static var warningLevelColor = UIColor.orange // !!!: try blue
    static var greenLevelColor =   UIColor.green



    // computed: coef^td = 0.5 => coef = 0.5^(1/td)
    static var halfLifeCoef: Double = pow( 0.5, 1/timeForHalflife)
    
    
    // TODO: 40+1h + 40 should be green limit
    //       40+40 must be Red, because convergence of (x(n-1)/2 + V) is 2*V
    
    
    /// updates warningLevel and alertLevel
    static func _updateLevels()   {
        warningLevel = remainingLevel(startLevel: levelForAddition, afterMn: luGreenDelay)
        alertLevel = remainingLevel(startLevel: levelForAddition, afterMn: luBlueDelay)

    }
    
    
    // - MARK: WMSettingsProtocol
    // note: not really WMSettingsProtocol, it's the static version
    
    static func settingChanged()   {
        Self.luBlueDelay =  Double( WMUser.uCurrentUser.uSettings.sDelayAllowed )
        Self.luGreenDelay = Double( WMUser.uCurrentUser.uSettings.sDelayOptimal )
        
        // ???: timeForHalflife not changed so halfLifeCoef will not, set timeForHalflife?
        halfLifeCoef = pow( 0.5, 1/timeForHalflife)
        
        Self._updateLevels()
    }
    
    
    static func reductionAfter(difference: Double) -> Double {
        
        // coef^td = 0.5
        let result:Double  =  pow (halfLifeCoef, difference)
        return result
    }
    
    
    /// computes Level
    static func remainingLevel(startLevel: Double, afterMn: Double) -> Double    {

        let remainingLevel = startLevel * reductionAfter(difference: afterMn)
        return remainingLevel
    } // TODO: possible it's more efficient to not use remainingLevelForDate(date: Date) anymore; remainingLevelForHour(minutes: Int) too

    /// computes Level, using difference with refDate (whole date)
    /// - note: adding levelForAddition is no more systematic
    static func remainingLevelForDate(date: Date) -> Double    { // TODO: replace calls with remainingLevel()?
        
        let difference: Double = date.timeIntervalSince(refDate) / 60.0 // convert seconds to Mn

        let remainingLevel = refLevel * reductionAfter(difference: difference)
        return remainingLevel
    }
    
    /// returns time in Minutes, as Double (direct cast to TimeInterval)
    static func delayUntilOrange(fromLevel: Double) -> Double {

        // coef^td = 0.5 => coef = 0.5^(td)
        // => td = log(0.5)/log(coef)
        
        var resultDelay: Double = log(alertLevel/fromLevel) / log(halfLifeCoef)
        resultDelay = ceil(resultDelay)
        
        // FIX, ensure that the level after the resultDelay is conform:
        var level = remainingLevel(startLevel: fromLevel, afterMn: resultDelay)
        while !(level<alertLevel)    {
            resultDelay += 1 // or += 60?
            level = remainingLevel(startLevel: fromLevel, afterMn: resultDelay)
        }
        return resultDelay
    }
    /// returns time in Minutes, as Double (direct cast to TimeInterval)
    static func delayUntilGreen(fromLevel: Double) -> Double {

        // coef^td = 0.5 => coef = 0.5^(td)
        // => td = log(0.5)/log(coef)
        
        var resultDelay: Double = log(warningLevel/fromLevel) / log(halfLifeCoef)
        resultDelay = ceil(resultDelay)
        
        // FIX, ensure that the level after the resultDelay is conform:
        var level = remainingLevel(startLevel: fromLevel, afterMn: resultDelay)
        while !(level<warningLevel)    {
            resultDelay += 1 // or += 60?
            level = remainingLevel(startLevel: fromLevel, afterMn: resultDelay)
        }
        return resultDelay
    }

    /// computes Level, using difference with refMinutes (since "00:00:00");
    /// - note: You Must include hours in minutes (as 60*hh+mm)
    /// - note: adding levelForAddition is no more systematic
    static func remainingLevelForHour(minutes: Int) -> Double    { // TODO: replace calls with remainingLevelForDate()?
        
        let difference: Double = Double( minutes-refMinutes )

        let remainingLevel = refLevel * reductionAfter(difference: difference)
        return remainingLevel
    }
    
    /// - note: returns a type of UIColor because SetStroke() need it, convert with .cgColor elsewhere
    static func colorForLevel(level: Double) -> UIColor     {
        if        level >= WMLevelUtils.alertLevel      {
            return redLevelColor
        } else if level <= WMLevelUtils.warningLevel    {
            return greenLevelColor
        }
        else                                            {
//            // try gradient only in a smaller zone
//            if levelAfter >= WMLevelUtils.warningLevel - 5       {
//
//            }
            return warningLevelColor
        }
        
//        else if levelAfter >= WMLevelUtils.warningLevel    {
//            
//            let redComponent: CGFloat = CGFloat((level - WMLevelUtils.warningLevel) / (WMLevelUtils.alertLevel - WMLevelUtils.warningLevel))
//            let greenComponent: CGFloat = CGFloat((WMLevelUtils.alertLevel - level) / (WMLevelUtils.alertLevel - WMLevelUtils.warningLevel))
//            
//            return UIColor.init(red: CGFloat(redComponent), green: greenComponent, blue: 0.0, alpha: 1.0)
//        } 
    }

    
}
