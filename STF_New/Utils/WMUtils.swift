//
//  WMUtils.swift
//  STF_New
//
//  Created by Christophe Delannoy on 14/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class WMUtils: NSObject {
    
    /// make to debug errors with one breakpoint (here)
    static func printError(from: String)    {
        print("Error: \(from)")
    }
    static func printWarning(from: String)    {
        print("Warning: \(from)")
    }

    /// return a String, padded to 2 chars
    static func string2From(_ value: Int) -> String       {
        if value >= 10  {
            return String(value)
        } else {
            return "0\(String(value))"
        }
    }
    static func hhmmStringFrom(_ hh: Int, _ mm: Int) -> String       {
        
        return "\(string2From(hh)):\(string2From(mm))"
    }

    /// parameter String, as "HH:mm"
    static func stringToMinutes(_ stringValue: String) -> Int {
        
        var substrings: [Substring] = stringValue.split(separator: ":")
        if substrings.count==3  { // not as expected, e.g. "07:15:00"
            substrings.remove(at: 0)
        }
        let hours: Int =   Int( substrings[0] ) ?? 0
        let minutes: Int = Int( substrings[1] ) ?? 0
        
        return hours*60 + minutes
                
        // see also:  @discardableResult mutating func remove(at i: String.Index) -> Character
    }
    
     static func hhmmIntsFrom(_ stringValue: String) -> (Int, Int) {
        let substrings: [Substring] = stringValue.split(separator: ":")
        let hours: Int =   Int( substrings[0] ) ?? 0
        let minutes: Int = Int( substrings[1] ) ?? 0
        return (hours, minutes)
     }
    
    static func repair(_ hour: inout String) {
        /* FIX for: found  "010:010" */
        if hour.count>5 {
            var hh, mm:Int
            (hh, mm) = WMUtils.hhmmIntsFrom(hour)
            
            /*
             // the right fix:
            do {
                let rev = try dbDatastore!.getDocumentWithId(anID)
                
            } catch {
                
            }
             */
            hour = WMUtils.hhmmStringFrom(hh, mm)
            
        }
    }
}






