//
//  WMGraphicView.swift
//  STF_New
//
//  Created by Christophe Delannoy on 13/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit
//import STF_NewUITests
//import SnapshotHelper



// var view: UIView?


class WMGraphicView: UIView, WMSettingsProtocol {
// class WMGraphicView: UIScrollView { // <- not a bad idea

    /* params */

    /// make possible to handle rotations correctly
    var gvViewSize: CGSize
    
    var gvSwipeOffset: Int = 0
    
    /// the Hour corresponding to xGraphMin (at left)
    var gvDateMin: CGFloat = 0.0 // in minutes, from TimeInterval;
    /// the Hour corresponding to xGraphMax (at right)
    var gvDateMax: CGFloat = 1.0 // in minutes, from TimeInterval;
    // note: inited as (0, 1), so (gvDateMax-gvDateMin) is never null

    var gvRatioX2H: CGFloat = 1.0
    var gvRatioH2X: CGFloat = 1.0
    
    
    var startingDate: Date // date at "00:00:00"; replaces: dayDate
    

    /// textFields for Absisses
    var gvDisplayedDates:      WMDatesTextFields = WMDatesTextFields()
    
    /// textFields for Entries
    var gvDisplayedEntries:    WMEntriesTextFields = WMEntriesTextFields()
    
    /// textFields for CurentDate
    private var gvCurrentDateTF:        MWCurrentDateTextField = MWCurrentDateTextField(date: Date())
    private var gvCurrentDateTimer:     Timer?
    /// textFields for nextBlueDate
    private var gvNextBlueDateTF:       MWInfoTextField = MWInfoTextField(date: Date())
    /// textFields for nextGreenDate
    private var gvNextGreenDateTF:      MWInfoTextField = MWInfoTextField(date: Date())
    
    
    // parameters for the data graphics (curve)(in the graphView coordinates)
    var xGraphMin: CGFloat = 0.0
    var xGraphMax: CGFloat = 1.0 // inited to 1.0 so (xGraphMax-xGraphMin) never null
    var yGraphMin: CGFloat = 0.0
    var yGraphMax: CGFloat = 0.0


    private var gvTabZoneMarker_X:  CGFloat
    
        
    var gvDrawing: Bool = false
    
    var gvNeedsZoomUpdate: Bool = true
    var gvNeedsUpdate: Bool = true
    var gvCanRotate_Vertical: Bool = false
    
    // TEST
    var gvNeedsUpdate_currentTimeOnly = false
    
    
    
    /* never called
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        _setUp()
    }
    */
    

    required init?(coder: NSCoder) {
        
        let now: Date = Date()
        startingDate = WMDateUtils.dateAtMidnight(fromDate: now)
        
        gvDisplayedEntries.dtIdentifier = "Entry" // OBSO now (well, not really yet, not all was displaced)
        gvDisplayedDates.dtIdentifier =   "Date"
        
        
        gvTabZoneMarker_X = 0.0

        gvViewSize = CGSize.zero
        
        super.init(coder: coder)
        gvDisplayedEntries.delegate = self
        gvDisplayedDates.delegate = self
        
        
        // NOTE: see "UIInterfaceOrientation preferredInterfaceOrientationForPresentation", check if can display horizontally at start whatever the device is
                
        
        _setUp()
        
        self.accessibilityIdentifier = "GraphicView" // For UI XCTest
        
    }
    
//    // TEST (capture bounds changing)
//    override func didMoveToWindow() {
//        //
//    }
//    override func didMoveToSuperview() {
//        //
//    }
//    override func display(_ layer: CALayer) {
//        //
//        super.display(layer)
//    }
    
    
    
    // MARK: - insertions Utilities
    
    /// for tests: insert Dates and make them display (legacy)
    private func _testBuild() {
        /// min Hour to display (at left)
        let minHour: Int = 06 // was: gvMinHour
        /// max Hour to display (at right)
        let maxHour: Int = 15 // was: gvMaxHour;
        // the range may cover more than 24h, so maxHour must be highter
        
        for  hour in minHour...maxHour {

            let fullDate: Date = startingDate.addingTimeInterval( TimeInterval(hour*3600) )

            /* fix to valid values; not the same filter as in DigicodeView */
            var currentHour = hour
            if currentHour > 24 { // because of range (@see supra)
                currentHour  = currentHour - 24
            }

            _ = gvDisplayedDates.appendDate(date: fullDate)
        }
    }
    
    /// utility for insertDataset(numSet: Int) only, @see this function;
    private func _buildEntriesFieldsWith(dates_Str: [String], forDay: String) {
        for entry_dateString in dates_Str {
            
            let wholeDateString = entry_dateString.replacingOccurrences(of: "1899-12-31", with: forDay)
            
//            let newEntry: WMEntry = WMEntry(userID: "", date: wholeDateString)
            let newEntry: WMEntry = WMEntry(userID: WMUser.uCurrentUser.uName, date: wholeDateString)
            
            self.buildTFieldForNewEntry(entry: newEntry)
            
            // TEST;
            RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5)) // also try to animate the Fields displacements
        }
    }
        
        
    /// add Entries, for Test, via EasterEgg
    /// - returns true if did create some Entries
    /// - note: the first part for the code is "87:0"
    func insertDataset(numSet: Int) -> Bool {
        
        let entries_Str: [String] =
            ["1899-12-31 04:50:00", "1899-12-31 06:08:00", "1899-12-31 08:23:00", "1899-12-31 09:54:00", "1899-12-31 11:20:00", "1899-12-31 13:45:00", "1899-12-31 15:20:00", "1899-12-31 17:55:00", "1899-12-31 19:34:00", "1899-12-31 21:45:00", "1899-12-31 23:52:00"]
        
        
        
        // IMPORTANT:
        // it may fail, because of missing adding Dates TextFields at left (e.g. "04:50:00"),
        // so relaunch one time again (with doTest=false)
        // !!!: probably solved, check
        
        /* change the Day in all of the static Set values */
        var yyddmm: String
        switch numSet   {
        case 0:
            // remove all Entries:
            for displayedEntry in gvDisplayedEntries   {

                let entry: WMEntry = (displayedEntry.deDataTextField as! MWEntryTextField).etfEntry!
                _ = gvDisplayedEntries.removeEntry(entry)
            }
        case 1:
            // inserts Dates from entries_Str, at today,
            let today: Date = Date()
            yyddmm = WMDateUtils.dayDateString(date: today     )
            _buildEntriesFieldsWith(dates_Str: entries_Str, forDay: yyddmm    )
        case 2:
            // inserts Dates from entries_Str, at yesterday
            let yesterday: Date = Date.init(timeInterval: -24*3600, since: Date())
            yyddmm = WMDateUtils.dayDateString(date: yesterday )
            _buildEntriesFieldsWith(dates_Str: entries_Str, forDay: yyddmm)
        case 3:
            // inserts two Dates, for testing this:
            //      when one Entry tabbed and one Entry non-tabbed, check removing the first
            let now: Date = Date()
            let firstDate:  Date = Date.init(timeInterval: -24*3600 + 3*60, since: now)  // 24h ago plus 3mn
            let secondDate: Date = Date.init(timeInterval: -7200 ,          since: now)  //  2h ago
        
            var entryString = WMDateUtils.fullDateString(date: firstDate)
            var newEntry = WMEntry(userID: WMUser.uCurrentUser.uName, date: entryString)
            self.buildTFieldForNewEntry(entry: newEntry, tabbed: true)
            
            entryString =     WMDateUtils.fullDateString(date: secondDate )
            newEntry =     WMEntry(userID: WMUser.uCurrentUser.uName, date: entryString)
            self.buildTFieldForNewEntry(entry: newEntry)

        case 4:
            // TODO:
            // enable Synchronisation
            let flag = true
            
        default:
            return false

        }
        
        return true
    }
    
    /// only called from init()
    private func _setUp()    {
        
        // QUESTION: did viewDisplay? -> NO (it's nil)
        
        // 1) build Current TextField
        // 2) call updateZoom() (? or ensureDateRange()) then add DateTextFields around
        // notice that we can also, at launch, if not Entry for the currentDay, look in previous days. We need also the TODO supra
        
        let currentUser = WMUser.uCurrentUser

        
//        _ = ensureDateRangeForCurrentDate()
        _ = ensureDateRangeAtStart()
        // TODO: ok, but if Entries added, and zoo changes, CurrentDate TF can overleap the right screen border
        
        /* load Entries, and build Entries TextFields */
        gvNeedsZoomUpdate = loadEntries(forUserID: currentUser.uName) // do updateZoom() later
        // TODO: should displace (call from Main View)?
        
        
        yGraphMin = 10.0 + gvDisplayedEntries.frameSizeHeight + 10.0
        // TODO: yGraphMin==20 at start (ok), but should change at least adding an Entry

        
        
        /* add currentDate/blue/green TextField (they were created before) */
        self.addSubview(gvCurrentDateTF)
        self.addSubview(gvNextBlueDateTF)
        self.addSubview(gvNextGreenDateTF)
        
        // set 'hem now to clear, will be changed when adding new EntryFields
        gvNextBlueDateTF.layer.borderColor = UIColor.clear.cgColor
        gvNextGreenDateTF.layer.borderColor = UIColor.clear.cgColor
        
        // TEST (https://github.com/raywenderlich/swift-style-guide#computed-properties)
//        var tca = gvDisplayedEntries.count
//        tca += 1
//        var tcb = gvDisplayedEntries.count
        // -> OK
        
        if gvDisplayedEntries.count == 0    {
            gvNextBlueDateTF.isHidden = true
            gvNextGreenDateTF.isHidden = true
        }
        
        gvCurrentDateTimer = Timer.init(timeInterval: 1.0, target: self, selector: #selector(self.currentDateMethod), userInfo: nil, repeats: true)
       
        if gvCurrentDateTimer != nil{
            RunLoop.main.add(gvCurrentDateTimer!, forMode: RunLoop.Mode.default)
        }
        
        
        self.layer.borderColor = UIColor.black.cgColor // CGColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
    }
    
    /// method called from gvCurrentDateTimer
    @objc func currentDateMethod() {
        
        let currentDate_previous = gvCurrentDateTF.getHHMMString()
        gvCurrentDateTF.setDate(date: Date())
        let currentDate_now = gvCurrentDateTF.getHHMMString()
        
        
        if currentDate_now.elementsEqual(currentDate_previous) {
            return
        }
        // (so it's easier to debug moves)
        
        if xGraphMin > 0.0   { // skip if zoom not inited yet
            self._resetCurrentDateTextFieldXPosition()
            
            // because of the move, check if adding Dates at right is necessary
            var rangeChanged = false
            

            rangeChanged = ensureDateRangeForCurrentDate()
            
            if rangeChanged {
                updateZoom()
            }
            
            // in all cases:
            gvCurrentDateTF.cdNeedsUpdate = true
            setNeedsDisplay()
            // TODO: could override Draw() for MWCurrentDateTextField, check
//            gvNeedsUpdate_currentTimeOnly = true // needs to check rangeChanged; bad idea
            
            gvCurrentDateTF.setNeedsDisplay()
        }
    }
    
    /// Builds two Date TextFields so the Current Date is in the current Date Range;
    /// - returns true if some Date TextFields were added, so you may need to rearrange the display
    /// - note : call ony at start, as no Entry is present yet
    private func ensureDateRangeAtStart() -> Bool {
        
        // TODO: not correct at start with the TF Width
        
        
        if gvDisplayedDates.count != 0   {
            // should have been called only once, at start
            WMUtils.printWarning(from: "ensureDateRangeAtStart()")
        }

        let currentDate = gvCurrentDateTF.date

        /* no Date yet,
         *  => ADD just two, at Left and Right */
        

        let dateAtLeft_dayStr =    WMDateUtils.dayDateString(date: currentDate)
        let dateAtLeft_hoursStr =  WMDateUtils.hoursString(  date: currentDate).dropLast(6) // drop "mm:ss"
        let dateAtLeftStr = "\(dateAtLeft_dayStr) \(dateAtLeft_hoursStr):00:00"
        let dateAtLeft: Date = WMDateUtils.date(fromFullDate: dateAtLeftStr)!
            // could use? this:
//        let test = gvCurrentDateTF.getHHString()


        var newDateTextField: MWDateTextField
        newDateTextField = gvDisplayedDates.appendDate(date: dateAtLeft)
        _resetDateTextFieldXPosition(dateTextField: newDateTextField)

        let dateAtRight = Date(timeInterval: 3600, since: dateAtLeft)

        newDateTextField = gvDisplayedDates.appendDate(date: dateAtRight)
        _resetDateTextFieldXPosition(dateTextField: newDateTextField)

        
        // note: if current date has mm==00, we *should* have 3 Dates (current-1H, current, current+1h)
//        if dateAtRight.distance(to: dateAtLeft) > 3600
//        if dateAtRight.timeIntervalSince(dateAtLeft) > 3600
        // -> supposing it may be added later, just log:
        if dateAtRight.timeIntervalSince(currentDate) < 0   {
            WMUtils.printWarning(from: "ensureDateRangeAtStart()")
        }
        
        
        

        gvNeedsZoomUpdate = true


        return true
    }
        
    /// Checks if the Current Date is in the current Date Range; builds Date TextFields if necessary (so, extends the Data Range)
    /// - returns true if some Date TextFields were added, so you may need to rearrange the display
    private func ensureDateRangeForCurrentDate() -> Bool {

        var dateAddedAtRight = false
        var dateAddedAtLeft = false
        
        //if gvDisplayedEntries.count == 0   {
        // "if" commented: if no Entry while a long time, the CurrentDate can dispear at rignt

            // note: ensure it will be compatible with future Swipe function
            
  
            if gvSwipeOffset == 0   { // currently, always true;
                if gvCurrentDateTF.frame.maxX > gvViewSize.width   { // not this: frame.maxX; report?
                    let dateAtRight = Date(timeInterval: 3600,
                                           since: gvDisplayedDates.last!.deDataTextField.date)
                    _ = gvDisplayedDates.appendDate(date: dateAtRight)

                    dateAddedAtRight = true
                    gvNeedsZoomUpdate = true
//                    return true
                } // note: not "else if"
                if gvCurrentDateTF.frame.minX < 0   {
                    
//                    let dateAtLeft = Date(timeInterval: -3600, since: gvDisplayedDates.firstDate!)
//                    _ = gvDisplayedDates.insertDate(date: dateAtLeft, at: 0)
                    let newDateTextField =  gvDisplayedDates.insertNewDate(offset: -3600)
                    _resetDateTextFieldXPosition(dateTextField: newDateTextField)

                    dateAddedAtLeft = true
                    gvNeedsZoomUpdate = true
//                    return true
                }
            }// else ???
            
////            let lastDateToShow: Date = gvDisplayedDates.last!.deDataTextField.date
//            let lastDateTF = gvDisplayedDates.lastUntabbed().deDataTextField
//            while gvCurrentDateTF.frame.maxX > lastDateTF.frame.maxX    {
//                // add Dates at right
//
//
////                let newDateTimeDate = Date(timeInterval: 3600, since: gvDisplayedDates.last!.deDataTextField.date)
//                let lastDate: Date = gvDisplayedDates.lastUntabbed().deDataTextField.date
//                let newDateTimeDate = Date(timeInterval: 3600, since: lastDate)
//                let newDateTextField = gvDisplayedDates.appendDate(date: newDateTimeDate)
//
//                dateAddedAtRight = true
//            }
//
//            let firstDateTF = gvDisplayedDates.firstUntabbed().deDataTextField
//            while gvCurrentDateTF.frame.minX < firstDateTF.frame.minX    {
//                // add Dates at left
//
//                let firstDate: Date = gvDisplayedDates.firstUntabbed().deDataTextField.date
//                let newDateTimeDate = Date(timeInterval: -3600, since: firstDate)
////                let newDateTextField = gvDisplayedDates.appendDate(date: newDateTimeDate)
//                let newDateTextField = gvDisplayedDates.insertDate(date: newDateTimeDate, at: 0)
//
//                dateAddedAtLeft = true
//            }
            
            return (dateAddedAtLeft || dateAddedAtRight)
        //}
        return false
    }
    
    /// Checks if the new Entry is in the current Date range; builds Date TextFields if necessary (so, extends the Data Range)
    /// - returns true if some Date TextFields were added, so you may need to rearrange the display
    private func ensureDateRange() -> Bool {
        
        //var rangeChanged = false
        let dateAddedAtRight = false // unused?
        var dateAddedAtLeft = false
        
        // stop here if no Entry:
        if gvDisplayedEntries.count == 0   {
            return (dateAddedAtLeft || dateAddedAtRight)
        }
        
        
        // TODO: should do this at now: remove all Dates but one that come before firstEntry

        
        // add from the last Date, until currentDate and greenDate appears completelly (currentDate can be after greenDate)
        var maxX =             gvNextGreenDateTF.frame.maxX
        let currentDate_maxX = gvCurrentDateTF.frame.maxX
        if currentDate_maxX > maxX  {
            maxX = currentDate_maxX
        }
        while true {
            let lastDate_X:  CGFloat = gvDisplayedDates.last!.deDataTextField.frame.midX
            
            if lastDate_X > maxX    {
                break
            }
            
            let newDateTimeDate = Date(timeInterval: 3600, since: gvDisplayedDates.last!.deDataTextField.date)
            
            let newDateTextField = gvDisplayedDates.appendDate(date: newDateTimeDate)
            
            dateAddedAtLeft = true
           _resetDateTextFieldXPosition(dateTextField: newDateTextField) // USEFULL?
        }

        
        
        if gvDisplayedEntries.count==0    {
            // can't check Entries, and no need to check Level
            return (dateAddedAtLeft || dateAddedAtRight)
        }
        // note: as Date are only HH, they MUST be *60 before all!!!
        
        
        /* should ADD at LEFT?:
         *  check if firstEntry lower than firstDate
         */
        while true {
            
            let firstEntryDate: Date = gvDisplayedEntries.first!.deDataTextField.date
            let firstDateDate:  Date =  gvDisplayedDates.first!.deDataTextField.date
            
            if firstEntryDate>firstDateDate   {
                // OK (note: if equals, do addition)
                break
                //print("CHECK MWDateTextField.init please")
            }

            dateAddedAtLeft = true
            
            // add 1 hour (3600s) before:
//            let newDateTimeDate = Date(timeInterval: -3600, since: gvDisplayedDates.first!.deDataTextField.date)
//
//            let newDateTextField: MWDateTextField = gvDisplayedDates.insertDate(date: newDateTimeDate, at: 0)
            let newDateTextField = gvDisplayedDates.insertNewDate(offset: -3600)
            
            // !!!: usefull? if yes embed and call from insertNewDate()
            _resetDateTextFieldXPosition(dateTextField: newDateTextField)
        }
        
        return (dateAddedAtLeft || dateAddedAtRight)
    }
    
    // MARK: - display Utilities
    
    /// maps Minutes to absisses, using zoom parameters;
    /// - parameter minutes: must be as (60*h+m)
    func mapMinutesToXGraph(minutes: CGFloat) -> CGFloat {
        /* xGraphMin <-> gvDateMin
         * xGraphMax <-> gvDateMax
         */
        
        gvRatioH2X = (xGraphMax - xGraphMin)/CGFloat(gvDateMax - gvDateMin)
        let result = (CGFloat(minutes) - gvDateMin) * gvRatioH2X + xGraphMin
        
        return result
    }

    func mapXGraphToHour(xGraph: CGFloat) -> CGFloat {
        /* xGraphMin <-> gvDateMin
         * xGraphMax <-> gvDateMax
         */
        
        gvRatioX2H = CGFloat(gvDateMax - gvDateMin)/(xGraphMax - xGraphMin)
        let result =   (xGraph - xGraphMin) * gvRatioX2H  + gvDateMin
        
        return result
    }
    
    
    static var zoomCalled: Bool = false
    func updateZoom(newViewSize: CGSize = .zero)   {
        
        // for DEBUG (what called at start?)


        if Self.zoomCalled ==  false {
            print("updateZoom called")
            Self.zoomCalled = true
        }
        
        if newViewSize == .zero {
            gvViewSize = self.bounds.size
        } else {
            gvViewSize = newViewSize
        }
        // idea?: always draw horizontal at start, until all done; (then update if vertical)
        
        
        
        /* Vertical placements:
         *  ******************************  frame.top
         *                                  space = 10.0
         *      *****                       Entry.top
         *      *****                       Entry.bottom    (use a static size.height)(to compute)
         *                                  space = 10.0
         *        |
         *        |                         drawable,
         *        |
         *                                  space = 10.0
         *      *****                       Date.top
         *      *****                       Date.bottom     (use a static size.height)
         *                                  space = 10.0
         *  ******************************  frame.Bottom = self.bounds.maxY
         */
        

        yGraphMax = gvViewSize.height - 10.0 - gvDisplayedDates.frameSizeHeight - 10.0
        
        /* UPDATE for xGraphMin: if using gvTabLeft, the most TF at Left is from Entries */
        /* gvEntries_LeftTabNum     ------GView------   (replaced with gvDisplayedEntries.leftTabNum)
         *  0:                      |   1   2   3   |
         * (inactive)               |               |
         *
         *  1:                      |   1 | 2   3   |
         *  (active)                |     |         |
         *
         *  2:                      |   2 | 3   4   |
         *  (active)                |   1 |         |
         */
        // CHECK: because really, the most **displayed** TF at left is from Dates
        
        // MANDATORY:
        // TF at right is always a Date (if tabRight==0)
        // if tabLeft==0
        //  - TF at Left is a Date
        // else
        //  - TF at Left is an Entry; new: its positionned at left
        
        if gvDisplayedDates.count == 0  {
            return
        }

        var leftEntry: WMDataTextField      // not evaluated now, because possibly not yet any Entry
        
        // in both cases, leftest TextField is a Date one
        if gvDisplayedEntries.leftTabNum > 0    {
            
            /* xGraphMin/gvDateMin, tabbedLeft version
             * - TF at Left is an Entry, the last tabbed one
             * - xGraphMin begins with the next Entry
             */
            
            leftEntry = gvDisplayedEntries.firstUntabbed(-1)    // skip all tabbed, but the last (i.e. Last Tabbed)
    
            
            // CHANGED: xGraphMin concern all excepted the Tab Zone
            gvTabZoneMarker_X = 10.0 + leftEntry.deDataTextField.frame.size.width + 10.0

            
            // look for leftest TextFiels, outside TabZone
            // force a Date to be at left? try this
            let leftEntryUntabbed = gvDisplayedEntries.firstUntabbed(0)
            let (leftDateTFIndex, leftDateTF) = gvDisplayedDates.findDateTextFieldIndexAtLeft(date: leftEntryUntabbed.deDataTextField.date)
            if leftDateTF == nil {
                print("Error") // should not occur; check, build if needed
            }
            if leftDateTF!.adtfTabbed { // should not occur; however we choose that tabbing a date (can be sufficient) is always followed by tabbing again a Date or an Entry, to prevent untab effect
                
                gvDisplayedDates.doUntabLeft(atIndex: leftDateTFIndex)
            }

            if leftDateTF!.frame.minX < leftEntryUntabbed.deDataTextField.frame.minX   {
                leftDateTF!.frame.origin.x = gvTabZoneMarker_X + 10.0
                xGraphMin = leftDateTF!.frame.midX
            } else {
                // this can occur because Entry Fields are larger than Date Fields (surelly Times are equal)
                leftEntryUntabbed.deDataTextField.frame.origin.x = gvTabZoneMarker_X + 10.0
                xGraphMin = leftEntryUntabbed.deDataTextField.frame.midX
            }
            
            /* consider the first TextField to be a Date                       */
            /* also, all Date->X convertion will be computed from startingDate */
            gvDateMin = CGFloat(leftDateTF!.date.timeIntervalSince(startingDate)) / 60.0
            
        } else {
            
            /* xGraphMin/gvDateMin, not tabbedLeft version
             * - TF at Left is a Date
             * - xGraphMin begins with the first Entry (if not after the leftest Date it's an error)
             */
            
            
            
            // NOTE that no Date should be hidden (i.e. tabbed) -> check for error (now disabled)(check: should be embed)
            let leftDate = gvDisplayedDates.first!
            
            
            // fix position:
            leftDate.deDataTextField.frame.origin.x = 10.0
            // set xGraphMin/gvDateMin:
            xGraphMin = leftDate.deDataTextField.frame.midX
            gvDateMin = CGFloat( leftDate.deDataTextField.date.timeIntervalSince(startingDate) ) / 60.0
        }
        
        /* xGraphMax/gvDateMax,
         *  *not tabbedLeft version*
         * - TF at Right is a Date
         * - xGraphMax ends with the last Entry (if not before the rightest Date it's an error)
         */
        
        
        

        // dates: set field.bottom to 10.0 from view.bottom
//        gvDisplayedDates_y = gvViewSize.height - 10.0 - gvDisplayedDates.frameSizeHeight
        WMDatesTextFields.dtfDefault_y = gvViewSize.height - 10.0 - gvDisplayedDates.frameSizeHeight
        // todo: embed
        
        // CHANGED:
        if gvDisplayedEntries.rightTabNum == 0    {
            let rightDate = gvDisplayedDates.last!
            
            // fix position:
            rightDate.deDataTextField.frame.origin.x = gvViewSize.width - 10.0 - gvDisplayedDates.frameSizeWidth
            // set xGraphMin/gvDateMin:
            xGraphMax = rightDate.deDataTextField.frame.midX
            gvDateMax = CGFloat( rightDate.deDataTextField.date.timeIntervalSince(startingDate) ) / 60.0

        }
        else {
            
        }// todo (later)
        

        
        // in all cases, just because zoom parameters may have changed
        _resetTextFieldsYPositions() // TODO: animation? but we need to catch completion for needUpdate
        _resetTextFieldsXPositions()
        _resetCurrentDateTextFieldXPosition()
        _resetBlueGreenDateTextFieldXPosition()

        gvNeedsZoomUpdate = false
    }

    
    /// reset Y position; only for Dates
    private func _resetTextFieldsYPositions() {
        for dateTextField in gvDisplayedDates {
//            dateTextField.deDataTextField.frame.origin.y = gvDisplayedDates_y
            dateTextField.deDataTextField.frame.origin.y = WMDatesTextFields.dtfDefault_y
            // TODO: check and embed
        }
    }
    
    /// - param: index is for Entries with Tab usage
    private func _resetEntryTextFieldXPosition(entryTextField: MWEntryTextField, tabIndex: Int) {
        // update TextFields positions; TODO: factorize to remap(UITextField)
        
        // FIX: Int(dateTF.text!) returned 0 instead of 24
        let hourAsMn = entryTextField.date.timeIntervalSince(startingDate) / 60.0 // could Refactor!
        
        var posX = entryTextField.frame.midX // <- for Debug
        
        if entryTextField.adtfTabbed == false   {
            // normal way (not tabbed):
            posX = mapMinutesToXGraph(minutes: CGFloat(hourAsMn)) // needs (hour*60) // TODO: _hoursFromTF()
            entryTextField.frame.origin.x = posX - (entryTextField.frame.size.width / 2.0)
            // check: po dateTF.frame.midX #compare to posX
            
        } else {
            // for all tabbed:
            entryTextField.frame.origin.x = 10.0
            
            let offsetNumber = gvDisplayedEntries.leftTabNum - tabIndex - 1 // e.g. if gvEntries_LeftTabNum==1 => 0 for index==0
            entryTextField.frame.origin.y = 10.0 + CGFloat(offsetNumber) * (10.0 + entryTextField.frame.size.height) // TODO: group by 5 (from bottom)
        }
        
        // or: _resetTextFieldPosition(textField: dateTF)
    }
    private func _resetDateTextFieldXPosition(dateTextField: MWDateTextField) {
        // TODO: check, could replace with One function (entries/date)
        
        let hourAsMn = dateTextField.date.timeIntervalSince(startingDate) / 60.0
        
        var posX: CGFloat = dateTextField.frame.origin.x // <- for Debug
        posX = mapMinutesToXGraph(minutes: CGFloat(hourAsMn))
        dateTextField.frame.origin.x = posX - (dateTextField.frame.size.width / 2.0)
        
        // check: po entryTF.frame.midX #compare to posX
        
        //_resetTextFieldPosition(textField: entryTF)
    }
    
    private func _resetCurrentDateTextFieldXPosition() {
        
        // changed: distinction with current (moving) VS blue/green (not moving, and beeing hidden or not)
        
        // IMPORTANT: called from -)timer (currentDateMethod), -)_buildEntryTextField
        
        // currentDate -> X:
        let hourAsMn = gvCurrentDateTF.date.timeIntervalSince(startingDate) / 60.0
//        var posX_Test: CGFloat = gvCurrentDateTF.frame.origin.x // <- for Debug
        let posX = mapMinutesToXGraph(minutes: CGFloat(hourAsMn))
        // set frame.midX to posX (midX is read-only):
        gvCurrentDateTF.frame.origin.x = posX - (gvCurrentDateTF.frame.size.width / 2.0)
        

        // Y value: // TODO: extract (should be called rarely, at start, when adding the first Entry, when no Entry in 24h)
        if gvDisplayedEntries.count == 0    {
            // show one;
            //  set midY to view.midY
//            gvCurrentDateTF.frame.origin.y = self.frame.midY - gvCurrentDateTF.frame.size.height/2.0
            gvCurrentDateTF.frame.origin.y = gvViewSize.height/2.0 - gvCurrentDateTF.frame.size.height/2.0
        } else                              {
            // show three;
            //  set midY to 1/4, 2/4, 3/4 of the view (0.25, 0.50, 0.75), or try 0.5, 0.65, 0.8

            let minY: CGFloat = 0 + 10
            let maxY: CGFloat = gvViewSize.height - 10

            let pos1: CGFloat = 0 + (maxY-minY)*0.50   //*0.25
            let pos2: CGFloat = 0 + (maxY-minY)*0.65   //*0.50
            let pos3: CGFloat = 0 + (maxY-minY)*0.80   //*0.75
            
            gvCurrentDateTF.frame.origin.y = pos1 - gvCurrentDateTF.frame.size.height/2.0
            gvNextBlueDateTF.frame.origin.y = pos2 - gvNextBlueDateTF.frame.size.height/2.0
            gvNextGreenDateTF.frame.origin.y = pos3 - gvNextGreenDateTF.frame.size.height/2.0
        } // seeAlso _drawCurrentDateLine
            
        
        // color:
        if gvDisplayedEntries.count == 0    {
            return
        }
        let hourAsMinutes: CGFloat = mapXGraphToHour( xGraph: CGFloat(posX) )
        var level = WMLevelUtils.remainingLevelForHour(minutes: Int(hourAsMinutes))
        // update: when currentTime grows, and Dates are added, the refLevel if wrong
        let (_, refDate) = gvDisplayedEntries.findEntryTextFieldIndexAtLeft(date: gvCurrentDateTF.date)
        if refDate != nil       { // nil at launch
            WMLevelUtils.refDate = refDate!.date
            level = WMLevelUtils.remainingLevelForDate(date: gvCurrentDateTF.date)

            gvCurrentDateTF.layer.borderColor = WMLevelUtils.colorForLevel(level: level).cgColor
        } // else { gvCurrentDateTF.layer.borderColor = WMLevelUtils.greenLevelColor.cgColor }
    }
    private func _resetBlueGreenDateTextFieldXPosition() {
        
        // IMPORTANT: called from -)timer (currentDateMethod), -)_buildEntryTextField
        
        // blueDate -> X:
        var hourAsMn = gvNextBlueDateTF.date.timeIntervalSince(startingDate) / 60.0
        var posX = mapMinutesToXGraph(minutes: CGFloat(hourAsMn))
        gvNextBlueDateTF.frame.origin.x = posX - (gvNextBlueDateTF.frame.size.width / 2.0)
        
        // greenDate -> X:
        hourAsMn = gvNextGreenDateTF.date.timeIntervalSince(startingDate) / 60.0
        posX = mapMinutesToXGraph(minutes: CGFloat(hourAsMn))
        gvNextGreenDateTF.frame.origin.x = posX - (gvNextGreenDateTF.frame.size.width / 2.0)
    }
 
    
    private func _resetTextFieldsXPositions() {
        
        // update TextFields positions
        
        for dateTF in gvDisplayedDates   {
            _resetDateTextFieldXPosition(dateTextField: dateTF.deDataTextField as! MWDateTextField)
        }
        for (displayedEntryIndex, displayedEntry) in gvDisplayedEntries.enumerated()   {
            _resetEntryTextFieldXPosition(entryTextField: displayedEntry.deDataTextField as! MWEntryTextField, tabIndex: displayedEntryIndex)
        }
        
        _resetCurrentDateTextFieldXPosition()
        _resetBlueGreenDateTextFieldXPosition()
    }
    
    
    /// checks if the Double-Tap occured in a TextField; concerns only Entries
    func findEntryTextField(where point: CGPoint) -> MWEntryTextField?   {
        let result = gvDisplayedEntries.findEntryTextField(where: point)
        return result
    }
    
    
    
    /// removes Date or Entrie, at least one, and just one.
    /// - returns: true if a Date or Entry was removed. The whole display is expecting to move on Left, so you should call updateZoom()
    /// - note: currently, always return true
    private func _reArrangeView() -> Bool   {
        
        var didChangeView = false
        
        if gvDisplayedEntries.count == 0    {
            return false
        }
        
        /* get Leftest and Rightest TextFields; update: in both case consider Dates AND Entries */
        
        var firstDateUntabbed: Int = gvDisplayedDates.firstUntabbedIndex()
        var firstDateTextField =     gvDisplayedDates[entryTextField: firstDateUntabbed]
        // look for first untabbed Entry
        let firstEntryUntabbed: Int =           gvDisplayedEntries.firstUntabbedIndex()
        let firstDisplayedEntryTextField = gvDisplayedEntries[entryTextField: firstEntryUntabbed]
        


        if firstDisplayedEntryTextField == nil    {
            // occurs sometimes...
            return false
            // check: if relaunched, the display contains a valid one!
            // an error was returned, look for "error, firstUntabbed"
        }
        
        // if the first TF to remove is a Date Field, it's important to remove something else (Date or Entry), if not the Date Field will be restored in updateZoom()
        if firstDateTextField!.date < firstDisplayedEntryTextField!.date    {

            firstDateTextField!.removeFromSuperview() // or: .hidden = true // todo: refacto
            gvDisplayedDates.doTabLeft(atIndex: firstDateUntabbed)
            // note: may be re-unTabbed in updateZoom() => infinite loop => FIXED
        
            
            
            firstDateUntabbed = gvDisplayedDates.firstUntabbedIndex()
            firstDateTextField =     gvDisplayedDates[entryTextField: firstDateUntabbed]
            
            didChangeView = true // needs a new call to updateZoom()
        }
        if firstDateTextField!.date < firstDisplayedEntryTextField!.date    {
            
            firstDateTextField!.removeFromSuperview() // or: .hidden = true
            gvDisplayedDates.doTabLeft(atIndex: firstDateUntabbed)
            
            firstDateUntabbed = gvDisplayedDates.firstUntabbedIndex()
            firstDateTextField =     gvDisplayedDates[entryTextField: firstDateUntabbed]
            
        } else {
            
            let displayRange: TimeInterval = gvDisplayedEntries.last!.deDataTextField.date.timeIntervalSince(firstDisplayedEntryTextField!.date)
            // !!!: could embed
            
            
            if displayRange > (24*60*60)    { // (i.e. 86400) range > 24h
                
                // remove DisplayEntry from display (needs a new call to updateZoom()
                _ = gvDisplayedEntries.removeEntry(gvDisplayedEntries[entry: firstEntryUntabbed]!)


                
                
                didChangeView = true
                
            } else {
            
                // mark it; updateZoom() will be called;
                gvDisplayedEntries.tabAtIndex(firstEntryUntabbed) // doTabLeft(atIndex: Int)
                
                didChangeView = true
            }
        }
        
        // note: some Date Field failed to be removed (solved in _drawDateLines())
        
        return didChangeView
    }
    
    
    private func _updateView() { // UNUSED
        
        //  Entries are supposed to be sorted by Date (@see loadEntries()
        if (gvDisplayedEntries.count==0)    {
            return
        }
        
        updateZoom()
        
        while gvDisplayedEntries.checkTextFieldsRecovering() {
            
            _ = _reArrangeView()
        }
    }
    
        
    func findInEntriesTextFields(entry: WMEntry) -> Bool {
        var hourExists = false
        for displayedEntry in gvDisplayedEntries   {
            let etfText: String = displayedEntry.deDataTextField.text!
            if etfText.elementsEqual(entry.eDocID!)   {
                hourExists = true
                // TODO: should Arrayinsert instead of Array.append (later), so better keep the Index
                
                gvDisplayedEntries.currentIndex = 0
                break
            }
        }
        return hourExists
    }// TODO: refactor to WMDataTextFields?
    
    
    /// - returns index for the Entry before removing it, so (-1) if not found
    func removeFromEntriesTextFields(entry: WMEntry) -> Int {

        // remove from DisplayedEntries
        let dRemoved = gvDisplayedEntries.removeEntry(entry)
        
        // remove from Dadatabase
        _ = entry.remove()
        
        return dRemoved
    }


    // updates time for Next green/blue TextFields
    func _updateInfoNext()  {
        
        // FIX:
        if gvDisplayedEntries.count == 0    {
            return
        }
        
        let lastEntryLevel = gvDisplayedEntries.lastEntry()!.levelAfter
        
        let timeUntilGreen = WMLevelUtils.delayUntilGreen(fromLevel: lastEntryLevel)
        let timeUntilGreen_Secs: TimeInterval = ceil(timeUntilGreen * 60)
        let gDate = Date.init(timeInterval: timeUntilGreen_Secs, since: gvDisplayedEntries.last!.deDataTextField.date)
        gvNextGreenDateTF.setDate(date: gDate)
        
        let timeUntilBlue = WMLevelUtils.delayUntilOrange(fromLevel: lastEntryLevel)
        let timeUntilBlue_Secs: TimeInterval = ceil(timeUntilBlue * 60)
        let bDate = Date.init(timeInterval: timeUntilBlue_Secs, since: gvDisplayedEntries.last!.deDataTextField.date)
        gvNextBlueDateTF.setDate(date: bDate);
    }
    
    /// builds a TextField, and makes it to position in the graphView, according to its value
    func _buildEntryTextField(entry: WMEntry) -> MWEntryTextField {
        
        var newEntryTextField: MWEntryTextField
        
        newEntryTextField = MWEntryTextField.init(withEntry: entry)
        
        let newEntryIndex: Int = gvDisplayedEntries.insert(newDataTF: WMDataTextField(deRevID: entry.eDocID!, deDataTextField: newEntryTextField))
        
        if (gvDateMax - gvDateMin) > 0     {
            //_resetTextFieldPosition(textField: newEntryTextField)
            _resetEntryTextFieldXPosition(entryTextField: newEntryTextField, tabIndex: newEntryIndex)
            // _resetEntryTextFieldYPosition:
            newEntryTextField.frame.origin.y = WMEntriesTextFields.etfDefault_y
        }
        
        
        /* 2) compute Level for this Entry */
        var newEntryLevel: Double = getLazyLevelFor(newEntryIndex)
        newEntryTextField.layer.borderWidth = 1.0
        newEntryTextField.layer.borderColor = WMLevelUtils.colorForLevel(level: entry.levelBefore).cgColor
        /* if some Entry are later, compute also => compute "from" this Entry */
        var entryIndex = newEntryIndex + 1
        while entryIndex < gvDisplayedEntries.count  {
            
            newEntryLevel = getLazyLevelFor(entryIndex, forceRebuild: true)
                        
            entryIndex += 1
        }
        
        
        
        /* 3) compute time for Level green */

        // validation for updateInfoNext():
        let newEntryLevel_test = gvDisplayedEntries.lastEntry()!.levelAfter
        _updateInfoNext()
        if newEntryLevel_test != gvDisplayedEntries.lastEntry()!.levelAfter {
            WMUtils.printWarning(from: "newEntryLevel_test != gvDisplayedEntries.lastEntry()!.levelAfter")
        }
            
            
        if (gvDateMax - gvDateMin) > 0  { // NOTE: at start, they are both equal to zero (FIX? or displace)

            var (hh, mm): (Int, Int)
            
            (hh, mm) = gvNextBlueDateTF.getHHMM()
            gvNextBlueDateTF.adtfX =  mapMinutesToXGraph(minutes: CGFloat(hh*60+mm))
            // TODO: in fact, are useless...

            (hh, mm) = gvNextGreenDateTF.getHHMM()
            gvNextGreenDateTF.adtfX = mapMinutesToXGraph(minutes: CGFloat(hh*60+mm))

            // TODO: change Y pos for the three
            // check if we can take: TF.left => x => Level(x) => Y => Y + 10
            _resetCurrentDateTextFieldXPosition() // Check: sufficient? // check: usefull now? (resetx was call at each entry)
            _resetBlueGreenDateTextFieldXPosition()
        }

        
        // TEST
        newEntryTextField.tag = 24*31 + 60*23
        
        return newEntryTextField
    }
    
    func loadEntries(forUserID: String) -> Bool  {
        
        var arrayChanged: Bool = false
        
        // temp, get the current Date
        let dateFormatter : DateFormatter = DateFormatter() // WRONG?
        let now: Date = Date()
        dateFormatter.dateFormat = "yyyy-MM-dd" // e.g. "yyyy-MM-dd HH:mm:ss"
        let dateYMD = dateFormatter.string(from: now)
        let dateYMD2 = WMDateUtils.dayDateString(date: now)
        // TODO: compare; this may fail if overlapping 00h00
        if !dateYMD.elementsEqual(dateYMD2)  {
            WMUtils.printError(from: "loadEntries")
        }
        
        /* load Entries and build textFields for each */
        WMDao.userID = forUserID // e.g. "John" or "Guest" (default) // !!!: ID or Name?
        var entries: [WMEntry]
        
        if WMDao.userID.count == 0   { // <- empty string
            WMUtils.printError(from: "loadEntries()")
            entries = []
            return false
        }
        
        // load, two possibilities:
        if false {
            
            entries = WMEntry.loadEntries(forDay: dateYMD)                // for this Day
            
        } else  {
            
            entries = WMEntry.loadEntries(timeIntervalFromNow: 28*3600)
            // 24*3600 means 24h => load from 24h ago
        }
        

        
        // (???): if empty here, try to load other days; can use WMDao.loadEntries(distanceLimit: 24h)
        
        for anEntry in entries.sorted() {
            
            // FIX: check if still exists;
            let hourExists = findInEntriesTextFields(entry: anEntry)
            if hourExists==false    {
                
                _ = _buildEntryTextField(entry: anEntry)
                arrayChanged = true
            }
        }
        
        print("did load \(gvDisplayedEntries.count) entries")
        if gvDisplayedEntries.count >= 1 {

            gvCurrentDateTF.useMessage(false)
            
            _resetCurrentDateTextFieldXPosition()
        }
        
        // call updateZoom() before ensureDateRange(), because
        //  - we called _buildEntryTextField() instead of buildTFieldForNewEntry()
        //  - so, updateZoom() never called yet: ensureDateRange() requieres to fix x<->time factors
        updateZoom()
        
        let rangeChanged = self.ensureDateRange() // should test entries.count before; better call (? addEntry / buildTFieldForNewEntry) instead of _buildEntryTextField?
        // ???: should test only for this one?
        
        return arrayChanged || rangeChanged
    }
    
    
    /// builds a TextField for this Entry, and makes the View to redraw
    /// note: you should check if an identical exists, @see findInEntriesTextFields()
    func buildTFieldForNewEntry(entry: WMEntry, tabbed: Bool = false) {

        
        _ /*let newTextField*/ = _buildEntryTextField(entry: entry)
        
        // these are hidden when Entry.count==0:
        gvNextBlueDateTF.isHidden = false
        gvNextGreenDateTF.isHidden = false
        
        gvCurrentDateTF.useMessage(false)

        let rangeChanged = ensureDateRange()
        // ???: should test only for this one?
        
        if /* changedCurrentDateTFMessage || */ rangeChanged    {
            // !!!: not sure that "changedCurrentDateTFMessage" can be ignored, however the next version won't use it
            updateZoom()
        }
        setNeedsDisplay()
    }
    
    
    override func setNeedsDisplay() {
        
        // wait _setUp() to complete (requieres self.init(coder: NSCoder))
        if gvDrawing    {
            return
        }
        gvNeedsUpdate = true // todo: remove some
        super.setNeedsDisplay() // will makes draw(_ rect: CGRect) to be called, unless init() not called
    }
    
    /// - returns The Level (after); if known (i.e. cached), returns the value; otherwise computes it.
    /// - parameter index: index in the displayedEntries Array
    /// - parameter forceRebuild: computes (again) from the previous Entry, overiding the cached value.
    func getLazyLevelFor(_ index: Int, forceRebuild: Bool = false) -> Double  {
        

        let thisEntry: WMEntry = gvDisplayedEntries[entry: index]!
        var thisLevel: Double =  thisEntry.levelAfter
        let thisDate: Date =     gvDisplayedEntries[date: index]!
        if forceRebuild    {
            thisLevel = -1.0
        }
        
        if thisLevel >= 0.0 {
            // known level:
            return thisLevel
        }
        
        // build Level from previous known, if exists
        if index==0 {
            // not entry before
            thisEntry.levelBefore = 0.0
            thisEntry.levelAfter = WMLevelUtils.levelForAddition
            _ = thisEntry.save()
            print(String(format: "Built Levels for Entry: #\(index), (%.2f, %.2f)", thisEntry.levelBefore, thisEntry.levelAfter))
            
            // NOTE: not a real good idea, shoul consider the whole database, not only displayed
        }
        
        else {
            
        
        // note: if not found (e.g. if dateDiff > 8h?) => set to 0.0+40.0
        
        let prevLevel = getLazyLevelFor(index - 1)

        WMLevelUtils.refDate = gvDisplayedEntries[date: index-1]!
        WMLevelUtils.refLevel = prevLevel
        let prevLevel_Now = WMLevelUtils.remainingLevelForDate(date: thisDate)
        
        thisEntry.levelBefore = prevLevel_Now
        thisEntry.levelAfter = WMLevelUtils.levelForAddition + prevLevel_Now
        
        _ = thisEntry.save()

        print(String(format: "Built Levels for Entry: #\(index), from %.2f => (%.2f, %.2f)", prevLevel, thisEntry.levelBefore, thisEntry.levelAfter))
        }
        
        // update the border color:
        let entryTextField = gvDisplayedEntries[entryTextField: index]!
        entryTextField.layer.borderWidth = 1.0
        entryTextField.layer.borderColor = WMLevelUtils.colorForLevel(level: thisEntry.levelBefore).cgColor
        
        return thisEntry.levelAfter
    }
    // MARK: - DRAWs
    /// draw lines, direction up, fom Dates
    private func _drawDateLines()   {

        var currentX: CGFloat   // for lines
        let path: UIBezierPath = UIBezierPath()
        for (dateTextFieldIndex, dateTextField) in gvDisplayedDates.enumerated() {
            if dateTextField.deDataTextField.adtfTabbed {
                continue
            }
            // FIX (couldn't be detected from checkRecovering() // TODO: why??
            if (dateTextField.deDataTextField.frame.origin.x - 10.0) < gvTabZoneMarker_X   {

                gvDisplayedDates.doTabLeft(atIndex: dateTextFieldIndex) // TODO: missing .hidden (currently refactoring)
                continue
            }
            
            
            currentX = dateTextField.deDataTextField.frame.midX
            
            // from bottom to top:
            path.move(to:    CGPoint(x: currentX, y: yGraphMax) )
            path.addLine(to: CGPoint(x: currentX, y: yGraphMin + 2.0) )
        }
        UIColor.gray.setStroke()
        path.stroke()
    }
        
    /// draw lines for Entries
    private func _drawEntryLines()   {
        
        var currentX: CGFloat   // for lines
        let path = UIBezierPath()
        
        for (entryTextFieldIndex, entryTextField) in gvDisplayedEntries.enumerated() {
            
            // gvTabLeft active?:
            /*  0:  |   0   1   2   |       => draw for 0, 1, 2
             *      |               |
             *
             *  1:  |   0 | 1   2   |       => draw for 0, 1, 2
             *      |     |         |
             *
             *  2:  |   1 | 2   3   |       => draw for 1, 2, 3
             *      |   0 |         |
             */
            if entryTextFieldIndex >= (gvDisplayedEntries.leftTabNum - 1)
            && entryTextField.deDataTextField.adtfTabbed == false  {
            
                // FIX (couldn't be detected from checkRecovering() // TODO: why??
                if (entryTextField.deDataTextField.frame.origin.x - 10.0) < gvTabZoneMarker_X   {
 
                    gvDisplayedEntries.doTabLeft(atIndex: entryTextFieldIndex)
                    continue
                }
                
                currentX = entryTextField.deDataTextField.frame.midX
                
                path.move(to:    CGPoint(x: currentX, y: yGraphMin) )
                path.addLine(to: CGPoint(x: currentX, y: yGraphMax - 2.0) )
                
                let entry: WMEntry = (entryTextField.deDataTextField as! MWEntryTextField).etfEntry!
                WMLevelUtils.colorForLevel(level: entry.levelBefore).setStroke()
                // note: "gvDisplayedEntries.enumerated()" should return Entries

                path.stroke()
                path.removeAllPoints()
            }
            // TODO: check and apply with gvTabRight (when gvTabRight coded)
            // TODO: first identify indexMin and indexMax, it would be easier
            // TODO: TODO: and also, ensureDateRange() must be wrong by now => update!
        }

    }
    

    /// draw vertical Lines from 0 to Level, from each point in GraphView Zone
    private func _drawScores()   {
        
        let path = UIBezierPath()
        
        for entryIndex in 0..<gvDisplayedEntries.count  {
        
            /* object: Draw vertical Lines between Entries, identified by leftEntry and right Entry */
            /* update is:
             *  instead or drawing from
             *      lastDisplayedEntry  to  lastDate
             *  will display from
             *      leftEntry           to  rightEntry
             *  scanning them, and setting right to last Date at the last iteration
             */
        
        
            let leftDisplayedEntry = gvDisplayedEntries[entryIndex] 
            if leftDisplayedEntry!.deDataTextField.adtfTabbed  {
                continue
            }

            let leftEntry: WMEntry = (leftDisplayedEntry!.deDataTextField as! MWEntryTextField).etfEntry!
            let leftLevel: Double = leftEntry.levelAfter
            
            
            let leftEntry_x: Int =    Int( (leftDisplayedEntry?.deDataTextField.frame.midX)! )
            var leftEntry_date: Int
            leftEntry_date = Int(((leftDisplayedEntry?.deDataTextField.date.timeIntervalSince(startingDate))!)) / 60
            var rightEntry: WMEntry? = nil
            var rightEntry_x: Int
            var rightEntry_date: Int

            if (entryIndex+1)<gvDisplayedEntries.count  {

                let rightDisplayedEntry = gvDisplayedEntries[entryIndex+1]

                rightEntry = (rightDisplayedEntry?.deDataTextField as! MWEntryTextField).etfEntry
                
                rightEntry_x =    Int( (rightDisplayedEntry?.deDataTextField.frame.midX)! )
                rightEntry_date = Int(((rightDisplayedEntry?.deDataTextField.date.timeIntervalSince(startingDate))!)) / 60
            }
            else    {
                rightEntry_x =     Int( (gvDisplayedDates.last?.deDataTextField.frame.midX)! )
                rightEntry_date = Int((gvDisplayedDates.last?.deDataTextField.date.timeIntervalSince(startingDate))!) / 60
            }

            // FIX:
            if leftEntry_x > rightEntry_x   {
                WMUtils.printError(from: "_drawScores()") // !!!: in fact, DateArray was not extended
                continue
            }
            
            // ADDED (unsing factorisation):
            //WMLevelUtils.refDate = leftDate
            WMLevelUtils.refMinutes = leftEntry_date
            WMLevelUtils.refLevel = leftLevel
            /* skip the Tab Zone */
            var xGraph = leftEntry_x + 1
            if /* gvTabLeft > 0 && */ xGraph < Int(xGraphMin)-5  {
                xGraph = Int(xGraphMin)-5
            }
            while xGraph < rightEntry_x                 {
                
                let hourAsMinutes: CGFloat = mapXGraphToHour( xGraph: CGFloat(xGraph) )
                let level = WMLevelUtils.remainingLevelForHour(minutes: Int(hourAsMinutes))
                
                path.move(to:    CGPoint(x: CGFloat(xGraph), y: yGraphMax                  ) )
                path.addLine(to: CGPoint(x: CGFloat(xGraph), y: yGraphMax - CGFloat(level) ) )
                
                WMLevelUtils.colorForLevel(level: level).setStroke()
                path.stroke()
                path.removeAllPoints()
                
                // TODO: optimise; use gradient(?); refactor
                
                if Int(hourAsMinutes) > rightEntry_date   {
                    // TODO: needs to extend the Dates range
                    // (perheaps, checkRecovering() is sufficient)
                    
                    // TODO: instead, try:
                    // 1) ... ?
              
                }
                
                xGraph += 1
            }
            
            // set level for next
            if rightEntry != nil    {
                
                var rightLevel = rightEntry!.levelAfter
                if rightLevel <= 0.0 {
                    rightEntry!.levelAfter = WMLevelUtils.levelForAddition // TODO: check other occurences, seems that the refactoring failed
                    
                    // TODO: add remaining level from previous Entry
                    let difference = Double(rightEntry_date - leftEntry_date) / 60.0
                    let remainingLevel = leftLevel * pow ( ( 1.0/sqrt(2.0) ), difference )
                    rightEntry!.levelAfter += remainingLevel
                        
                    _ = rightEntry!.save()
                    
                    rightLevel = rightEntry!.levelAfter
                    //_buildLevelsFrom(gvDisplayedEntries.endIndex)
                }
            }
            
        } // end: for entryIndex

    }
    
    /// draw Lines up and down from the CurrentDate text field.
    private func _drawCurrentDateLine() {
        
        // TODO: this is a copy from _resetCurrentDateTextFieldXPosition()
        // - optimise
        // - in this way, will do nothing, must be called from draw()
        
        let hourAsMn = gvCurrentDateTF.date.timeIntervalSince(startingDate) / 60
        
        var posX: CGFloat = gvCurrentDateTF.frame.midX // <- for Debug
        posX = mapMinutesToXGraph(minutes: CGFloat(hourAsMn))
        // set frame.midX to posX (midX is read-only)
        gvCurrentDateTF.frame.origin.x = posX - (gvCurrentDateTF.frame.size.width / 2.0)
        
        
        // TODO? -> yes
//        if gvCurrentDateTF.frame.origin.x < 0   { // or: < gvTabZoneMarker_X
//            // add Dates at left, from firstUntabbedEntry
//            
////            let newDateTimeDate = Date(timeInterval: -3600, since: gvDisplayedDates.first!.deDataTextField.date)
////            gvDisplayedDates.insertDate(date: newDateTimeDate, at: 0)
//            _ = gvDisplayedDates.insertNewDate(offset: -3600)
//            
//        }
//        // (TODO): check and displace
//        // note: done in ensureDateRangeForCurrentDate() => better call it
        _ = ensureDateRangeForCurrentDate()
        

        // ???: can handle TextField positions changes in Draw()
        // ...this would allow smoother swipes (not implemented)
        
        // draw (from bottom):
        let path: UIBezierPath = UIBezierPath()
        
        path.move(to:    CGPoint(x: posX, y: gvCurrentDateTF.frame.midY + 70 ) )
        path.addLine(to: CGPoint(x: posX, y: gvCurrentDateTF.frame.midY + 20 ) )
        
        path.move(to:    CGPoint(x: posX, y: gvCurrentDateTF.frame.midY - 20 ) )
        path.addLine(to: CGPoint(x: posX, y: gvCurrentDateTF.frame.midY - 70 ) )
        
        // level:
        if gvDisplayedEntries.count == 0    {
            
            // can't apply Level if No Entry:
            // simply draw'em without colors
            
            WMLevelUtils.greenLevelColor.setStroke()
            path.stroke()
            
            return
        }
        else {
            
            // when currentTime grows, and Dates are added, the refLevel if wrong
            let (_, refDate) = gvDisplayedEntries.findEntryTextFieldIndexAtLeft(date: gvCurrentDateTF.date)
            if refDate != nil  {
                WMLevelUtils.refDate = refDate!.date
                // TODO: WMLevelUtils.refLevel ???
                let level = WMLevelUtils.remainingLevelForDate(date: gvCurrentDateTF.date)
                
                
                
                
                WMLevelUtils.colorForLevel(level: level).setStroke()
                path.stroke()
            } else {
//                print("error")
                WMUtils.printError(from: "_drawCurrentDateLine()")
            }
        }


        
        // blue (Down to Up):
        posX = gvNextBlueDateTF.frame.midX
        
        // tip: the graph level (I mean: level<->y) is a const!
        var level = WMLevelUtils.alertLevel
        
        path.removeAllPoints()
        path.move(to:    CGPoint(x: posX, y: yGraphMax - level - 5) )
        path.addLine(to: CGPoint(x: posX, y: yGraphMin - 2.0      ) )
        WMLevelUtils.warningLevelColor.setStroke()
        path.stroke()
        // green (Down to Up):
        posX = gvNextGreenDateTF.frame.midX
        level = WMLevelUtils.warningLevel
        path.removeAllPoints()
        path.move(to:    CGPoint(x: posX, y: yGraphMax - level - 5) )
        path.addLine(to: CGPoint(x: posX, y: yGraphMin - 2.0      ) )
        WMLevelUtils.greenLevelColor.setStroke()
        path.stroke()
        // TODO: ok, but not efficient; try to reduce Font, remove the frame, and show the TF at top?
        // try to say they are Entries?
        // ...really, gvEntryFields can embed 'hem additionally!
        // but there will be an overlapping issue...

        
    }
    
    private func _drawTabzoneMarker()   {
        let path = UIBezierPath()
        
        path.move(to:    CGPoint(x: CGFloat(gvTabZoneMarker_X), y: gvViewSize.height ) )
        path.addLine(to: CGPoint(x: CGFloat(gvTabZoneMarker_X), y: 0.0 ) )

        UIColor.black.setStroke()
        path.stroke()
    }
        
    override func draw(_ rect: CGRect) {
                
        // NOTE: frame.size was (1112.0, 754.0) at init(), and (568.0, 290.0) by now
        // NOTE: for now (why?) frame.size was (600.0, 526.0) at init(), and (568.0, 290.0) by now
        
        gvDrawing = true
        
        // TODO: compare gvViewSize and frame.size:
        // if top bar appeared since the last updateZoom(), we must call again
        // first, check what is in updateZoom() just at rotation, and if draw called at this point
        if !gvViewSize.equalTo(frame.size)   {
            gvNeedsZoomUpdate = true
        }
        
        // spray Data
        if gvNeedsZoomUpdate    {
            
            /* UI not inited yet, (could not be set before, because of a wrong frame.size)
             * or updateZoom() was called because of rotation */
        
            updateZoom(newViewSize: self.frame.size) // send parameter to force TF updates
// TODO: should update TextFields, here
//            _resetTextFieldsXPositions()
        }
        
        if gvNeedsUpdate_currentTimeOnly    {
            
            // this flag means: just draw lines for currentTime
            _drawCurrentDateLine()
            gvCurrentDateTF.cdNeedsUpdate = false // check: OBSO
            
            gvNeedsUpdate_currentTimeOnly = false
            
//            super.draw(rect)  // NO
            gvDrawing = false
            return
        }
        if self.gvNeedsUpdate == false {
            super.draw(rect)
            gvDrawing = false
            return
        }
        

        
        /* draw lines up fom Dates: */
        _drawDateLines()
        
        
        /* draw lines down fom Entries: */
        _drawEntryLines()
        
        // draw currentTime
        // -> texField drawn in _resetTextFieldsXPositions()
        _drawCurrentDateLine()

        /* display Score */
        // actually, from last Entry to last Date
        if gvDisplayedEntries.count > 0    {
//            gvDrawing = false
//            return // TODO: TODO: _reArrangeView() never called
//        }
        
            _drawScores()
        
        
            if gvDisplayedEntries.leftTabNum > 0    {
            
                _drawTabzoneMarker()
            }
        }
        
        /* All Draws Done; check the result */

        /*
         * - call _reArrangeView() in async,
         *      if not, some TF will be missing
         * - call setNeedsDisplay() in async,
         *      if not (before draw to complete), will have no effect
         */
            
            
        DispatchQueue.main.async {
                
            RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.3))
            
            //self.snapshotView(afterScreenUpdates: true)
//            Snapshot.snapshot("01LoginScreen")
            //snapshot("e")
            
            var needsUpdate = false // track if we need to call setNeedsDisplay(); this function will be called again
            var changed = false
            // check if the CurentDate come across the view right border:
            
            if needsUpdate == false     {   // do one at a time
                changed =            self.gvDisplayedEntries.removeWithDateDistance()
                changed = changed || self.gvDisplayedDates.removeWithDateDistance()
                
                needsUpdate = changed
            }
            
            if changed == false {
                let foundRecovering = self.gvDisplayedEntries.checkTextFieldsRecovering()
                if foundRecovering {
                    needsUpdate = self._reArrangeView()
                    // TODO: should only displace the firstUntabbed Entry to left, and display will tab the requiered Dates
                    // this means that the most TF at left will not be always a Date
                }
            }
            
            if needsUpdate              {
                
                    self.gvNeedsUpdate = true
                    self.updateZoom()
                
                    self.setNeedsDisplay() // immediate
            }
            else                        {
                self.gvCanRotate_Vertical = true
            }
        }

        
        self.gvNeedsUpdate = false
        super.draw(rect)
        gvDrawing = false
   }

//    override class func transition(with view: UIView, duration: TimeInterval, options: UIView.AnimationOptions = [], animations: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
//        print("transition")
//    }
    
    
    // - MARK: WMSettingsProtocol
    
        func settingChanged() {
            WMLevelUtils.settingChanged()
            _updateInfoNext()
        }
    
}
