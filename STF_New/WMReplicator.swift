//
//  WMReplicator.swift
//  STF
//
//  Created by Christophe Delannoy on 21/04/2020.
//

import UIKit

import CDTDatastore

enum ReplicatorWay: Int {
    case wSend, wGet
}


/* an error(?) appears: CredStore - performQuery - Error copying matching creds.  Error=-25300, query={...}
 * - https://forums.couchbase.com/t/credstore-error-on-ios/14576/7 says, it's because the given Credential is'nt in the keychain
 *   the error appears in https://opensource.apple.com/source/Security/Security-54/Keychain/SecBase.h as
 *      errSecItemNotFound           = -25300,
 *  - https://stackoverflow.com/questions/46099940/credstore-perform-query-error says it's because ther's no Credential with the protectionSpace
 */

class WMReplicator: NSObject , CDTNSURLSessionConfigurationDelegate, CDTReplicatorDelegate  {
    
    /// for Debug, fix static address/credential; 2 values given here.
    /// - note: comment getValuesFromPrefs(), as it overrides this values
    static let credentials: [(address: String, username: String, password: String)] = [
        /// address for test, on Server:
        ("http://127.0.0.1:5984/stf/", "Rob", "123456"),
        /// address for test, on LocaHost:
        ("http://127.0.0.1:5984/stf/", "Rob", "123456") // for remote, you should use something else than 127.0.0.1
        // can check with: $ curl -X GET http://rob:123456@127.0.0.1:5984/stf
    ]

    private var (rAddress, rUsername, rPassword): (String, String, String) = credentials[0]
    
    
    private var rReplicator: CDTReplicator? = nil
    //        NSError *error;


    func getValuesFromPrefs()   { // note: needs to have called Register at launch
        //NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        let defaults: UserDefaults = UserDefaults.standard
        
        if let defaultValue: String = defaults.string(forKey: "sync_remote_database"   )    {
            rAddress = defaultValue
        }
        if let defaultValue: String = defaults.string(forKey: "sync_account_identifier")    {
            rUsername = defaultValue
        }
        if let defaultValue: String = defaults.string(forKey: "sync_account_password"  )    {
            rPassword = defaultValue
        }
        
    }
    var address: String {
        get { return rAddress } }
    
    required override init() {// for delegate
        super.init()
        
        // trying to avoid this log from System:
        //  "CredStore - performQuery - Error copying matching creds.  Error=-25300, ..."
        // (failed)
/*
        let protectionSpace = URLProtectionSpace.init(host: "http://127.0.0.1:5984/stf/",   // ? http://127.0.0.1"
                                                      port: 5984,
                                                      protocol: NSURLProtectionSpaceHTTP,      //"http",
                                                      realm: nil,
                                                      authenticationMethod: nil)

        let userCredential = URLCredential(user: username,     // "rob"
                                           password: password, // "123456",
                                           persistence: .permanent) // ?: forSession

        URLCredentialStorage.shared.setDefaultCredential(userCredential, for: protectionSpace)
*/
        
        getValuesFromPrefs()
        
        
    }
    /// Sends data from a local database to a remote database, or from a remote database to a local one
    /// - parameter option: wSend or wGet, according to your attempts
    /// - returns: nil if success, otherwise an error
    /* class */ func getsendData(option: ReplicatorWay) -> Error? {
        
         

        
        
        // Create and start the replicator -- -start is essential!
        guard let manager: CDTDatastoreManager = WMSharedDatastore.sharedDatastoreManager else {
            return NSError.init(domain: "Replicator", code: -1, userInfo: ["Failed" : "Manager"]) // fixme: not smart
        }
        let replicatorFactory: CDTReplicatorFactory = CDTReplicatorFactory.init(datastoreManager: manager)
        
        
 
        
        
        
        // username/password can be Cloudant API keys
        /* example
         * var s: String = "https://username:password@username.cloudant.com/my_database" // (example))
         * let s = String("http://\(username):\(password)@127.0.0.1:5984/stf/") // with    name/password
         * let s2 = "http://127.0.0.1:5984/stf/"                                // without name/password
         */
        let s2 = rAddress
        // ok for: $ curl -X GET http://rob:123456@127.0.0.1:5984/stf
        
        let  remoteDatabaseURL2: URL = URL.init(string: s2)!
        
        // let datastore: CDTDatastore = manager.datastoreNamed("my_datastore")
        guard let datastore: CDTDatastore = WMSharedDatastore.sharedDatastore else {
            return NSError.init(domain: "Replicator", code: -1, userInfo: ["Failed" : "Remote URL"])
        }
        
        // Create a replicator that replicates changes from the local
        // datastore to the remote database.
        
        switch option {
        case .wSend:    // use CDTPushReplication

            let pushReplication: CDTPushReplication = CDTPushReplication.init(
                source: datastore, target: remoteDatabaseURL2, username: rUsername, password: rPassword) // username: "rob", password: "123456"
            do {
                try rReplicator = replicatorFactory.oneWay(pushReplication)
                
                rReplicator!.delegate = self
                rReplicator!.sessionConfigDelegate = self
            } catch
            {
                //return EWUtils.printError(from: "Impossible to create the Send Replicator")
                return NSError.init(domain: "Replicator", code: -1, userInfo: ["Failed" : "Send Replicator"])
            }
            
        case .wGet:     // use CDTPullReplication
            let pullReplication: CDTPullReplication = CDTPullReplication.init(source: remoteDatabaseURL2, target: datastore, username: rUsername, password: rPassword)
             
            do {
                try rReplicator = replicatorFactory.oneWay(pullReplication)
            } catch
            {
                //return EWUtils.printError(from: "Impossible to create the Get Replicator")
                return NSError.init(domain: "Replicator", code: -1, userInfo: ["Failed" : "Get Replicator"])
            }
            
        }


        
        // Start the replication
        do {
            try rReplicator!.start()
            //handle error
        } catch {
             return NSError.init(domain: "Replicator", code: -1, userInfo: ["Failed" : "replicator.start"])
        }
        
        //wait for it to complete
        while rReplicator!.isActive() {
            Thread.sleep(forTimeInterval: 1.0)
            NSLog(" -> %@", CDTReplicator.string(for: rReplicator!.state))
            // finishes with "-> CDTReplicatorStateComplete"
            
            /* (lldb) p replicator.error            (Error?) $R5 = domain: "TDHTTP" - code: 401 { _userInfo = 0x00006000033e7d20 }
             
             * (lldb) p replicator.description
             (String) $R7 = "CDTReplicator push, source: (null), target: http:/127.0.0.1:5984/stf filter name: (null), filter parameters (null), unique replication session ID: repl001"
             
             * Log:
             2020-04-21 12:47:22.481205+0200 stf[5688:1048605] CredStore - performQuery - Error copying matching creds.  Error=-25300, query={
             atyp = http;
             class = inet;
             "m_Limit" = "m_LimitAll";
             ptcl = http;
             "r_Attributes" = 1;
             sync = syna;
             }
             
             (lldb) p replicator.error?.localizedDescription
             (String?) $R2 = "401 unauthorized"
             
             */
            
            if rReplicator!.error != nil  {
                 WMUtils.printError(from: "ERROR from replicator") // throw ?
            } else {
//                print("no error for \(option)")
            }
            

        }
        
        let error = rReplicator!.error
        // rReplicator = nil
        return  error
    }
    // ???: (get:) => send a notification, for example to EWUsers?

    func doStop() {
        if rReplicator == nil   {
            return
        }
        rReplicator?.stop()
    }

    
    
    // @protocol CDTNSURLSessionConfigurationDelegate
    //- (void)customiseNSURLSessionConfiguration:(nonnull NSURLSessionConfiguration *)config;
    func customiseNSURLSessionConfiguration(_ config: URLSessionConfiguration) {

        /*
        let protectionSpace = URLProtectionSpace.init(host: "127.0.0.1:5984/stf/", // "http://127.0.0.1:5984/stf/"
                                                      port: 5984,
                                                      protocol: "http",
                                                      realm: nil,
                                                      authenticationMethod: nil)
        
        let userCredential = URLCredential(user: "rob",
                                           password: "123456",
                                           persistence: .permanent) // ?: forSession

        URLCredentialStorage.shared.setDefaultCredential(userCredential, for: protectionSpace)
         */
        
        config.urlCredentialStorage = nil
    }
    
    
    // @protocol CDTReplicatorDelegate; UNUSED (but for DEBUG)
//    - (void)replicatorDidError:(nullable CDTReplicator *)replicator info:(nonnull NSError *)info    {
//
//    }
    func replicatorDidError(_ replicator: CDTReplicator?, info: Error) {
        // NSLog("Replicator Error: %@", info.localizedDescription)
    }
    
}


