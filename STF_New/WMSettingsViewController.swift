//
//  WMSettingsViewController.swift
//  STF_New
//
//  Created by Christophe Delannoy on 08/11/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class WMSettingsViewController: UIViewController /* , UIPickerViewDataSource, UIPickerViewDelegate */ {
    

    @IBOutlet weak var delaiLabel: UILabel!
    @IBOutlet weak var delaiOptimalLabel: UILabel!
    @IBOutlet weak var delaiAllowedLabel: UILabel!
    @IBOutlet weak var delaiOptimalValue: UITextField!
    @IBOutlet weak var delaiAllowedValue: UITextField!
    @IBOutlet weak var delaiOptimalStepper: UIStepper!
    @IBOutlet weak var delaiAllowedStepper: UIStepper!
    
    var delaiOptimalStepperStartTime: CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
    var delaiAllowedStepperStartTime: CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
    
    @IBOutlet weak var dayStartLabel: UILabel!
    @IBOutlet weak var dayStartPicker: UIDatePicker!

    @IBOutlet weak var dayStartComment: UILabel!
    

    @IBOutlet weak var okButton: UIButton!
    
    
    private func _localizedString(_ key: String) -> String  {
        let result = NSLocalizedString(key, tableName: "WMSettingsViewController", bundle: Bundle.main, value: "", comment: "")
        return result
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        chooseUserTableView.isHidden = true
//        // Register the table view cell class and its reuse id
//        chooseUserTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)

        // disabled:
//        chooseUserTableView.delegate = self
//        chooseUserTableView.dataSource = self
        
        
        // get prefs from current user;
        delaiOptimalStepper.value = Double( WMUser.uCurrentUser.uSettings.sDelayOptimal )
        delaiAllowedStepper.value = Double( WMUser.uCurrentUser.uSettings.sDelayAllowed )
        
        
        delaiOptimalValue.text = String( Int(delaiOptimalStepper.value) )
        delaiAllowedValue.text = String( Int(delaiAllowedStepper.value) )
        
        // get the parameter (HH:MM)
        let dayStartValue: String = WMUser.uCurrentUser.uSettings.sDayStartHour
        // set it to a Full Date, for the Picker
        if let fullDate = WMDateUtils.date(fromFullDate:  WMDateUtils.fullDateToday(dateHM: dayStartValue)) {
            dayStartPicker.date = fullDate
        }
        _updateDayStartComment()

         
        delaiLabel.text = _localizedString("Delays")
        delaiOptimalLabel.text = _localizedString("Optimal")
        delaiAllowedLabel.text = _localizedString("Allowed")
        dayStartLabel.text = _localizedString("DayStart Hour")
        // comment1.text = "not used"
        dayStartComment.text = _localizedString("Day Session Range")
    }

    @IBAction func okAction(_ sender: UIButton) {
    

        let dayStartValue = WMDateUtils.hoursString(date: dayStartPicker.date).dropLast(3)
        
        WMUser.uCurrentUser.setDelays(
            optimal: Int( delaiOptimalStepper.value ),
            allowed: Int( delaiAllowedStepper.value ),
            daySartHour: String( dayStartValue      )
        )
        // this will update WMLevelUtils
        
        self.dismiss(animated: true) {
            //
        }
    }

    
    /// changes the increment
    private func _stepValueForTimeSince(aStartTime: CFAbsoluteTime) -> Double {
        var theStepValue: Double = 1.0;
        let theElapsedTime: CFAbsoluteTime = CFAbsoluteTimeGetCurrent() - aStartTime;
        
//        if (       theElapsedTime > 6.0) {
//            theStepValue = 10;
//        } else
//        if (theElapsedTime > 4.0) {
//            theStepValue = 5;
//        } else
        if (theElapsedTime > 2.0) {
            theStepValue = 2;
        }

        return theStepValue;
    }
    
    // target on "Value Changed"
    @IBAction func optimalStepperAction(_ sender: UIStepper) {
    
        // requirement: optimal>allowed
        if sender.value < delaiAllowedStepper.value {
            sender.value = delaiAllowedStepper.value
        }
        //let theElapsedTime: CFAbsoluteTime  = CFAbsoluteTimeGetCurrent() - delaiOptimalStepperStartTime;
    
        sender.stepValue = _stepValueForTimeSince(aStartTime: delaiOptimalStepperStartTime)
    
        delaiOptimalValue.text = String( Int(sender.value) )
    }
    @IBAction func optimalStepperTapped(_ sender: UIStepper) {
        // set the increment:
        sender.stepValue = 1
        delaiOptimalStepperStartTime = CFAbsoluteTimeGetCurrent()
    }
    
    // target on "Value Changed"
    @IBAction func allowedStepperAction(_ sender: UIStepper) {
    
        // requirement: allowed<optimal
        if sender.value > delaiOptimalStepper.value {
            sender.value = delaiOptimalStepper.value
        }
        
        sender.stepValue = _stepValueForTimeSince(aStartTime: delaiAllowedStepperStartTime)
    
        delaiAllowedValue.text = String( Int(sender.value) )
    }
    @IBAction func allowedStepperTapped(_ sender: UIStepper) {
        sender.stepValue = 1
        delaiAllowedStepperStartTime = CFAbsoluteTimeGetCurrent()
    }
    
    private func _updateDayStartComment()   {

        let dayStartValue = WMDateUtils.hoursString(date: dayStartPicker.date)
        let dayStartString = String( dayStartValue.dropLast(3) )
        let comment = "A Day session will range from \(dayStartString) one Day to \(dayStartString) the following day"
        dayStartComment.text = comment
    }
    
    // target on "Value Changed"
    @IBAction func daystartPickerAction(_ sender: UIStepper) { 
    
        self._updateDayStartComment()
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // disabled (UIPickerView for custom UIDatePicker)
//    // MARK: UIPickerViewDataSource
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 2 // "hh" / "mm"
//    }
//
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int  {
//        switch component    {
//        case 1:
//            return 60
//        default:
//            return 24
//        }
//    }
//
//    // MARK: UIPickerViewDelegate
//    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
//        return 50
//    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return WMUtils.string2From(row)
//    }
    
}

extension NSDate {

    open override func my_compactDescription() -> String! {
        return "ee"
    }
}
