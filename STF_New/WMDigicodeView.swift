//
//  WMDigicodeView.swift
//  STF_New
//
//  Created by Developer White on 05/03/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit


protocol WMAddViewDelegate {
    /// user did push "OK"
    /// - parameter lkjlkj  dateHM date as String, whose format is as "HH:mm";  the value is filtered to a valid format
    func didEnterCode(dateHM: String)
    /// user did push "Cancel"
    func didCancel()
    /// user did push "Remove"
    func didRemove()
    /// EasterEgg
    func didEG(_ code: Int)
    
    /// the view to display is load, and not appeared yet; false if the view is not load
    var digicodeAppearing: Bool { get set }
}

enum WMDigicodeActionResponse: Int {
    case rNone,
         rCancel,
         rAdd,
         rEasterEgg
}


class WMDigicodeView: UIView /*, UITextViewDelegate */ { 

    
    @IBOutlet weak var dcTextField: UITextField!
    
    var dcMainViewControler: WMAddViewDelegate? = nil
    
    var dcTextViewValue: Int = 0
    
    var avMinutes: Int = 00;
    var avHours: Int =   00;
    
    /// option, enables to restrict numbers to "HH:mm" values
    var avFilterForTime = true
    // !!!: now as an option, but as ":" is requiered in the string, it's not really
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */


    

    
    /// fills dcTextField.text with right values
    /// - return: true if th text needed to be modified
    func filterForTime() -> Bool {
        
        var changed = false
        
        if avFilterForTime  {
                        
            // FIX:

            (avHours, avMinutes) = WMUtils.hhmmIntsFrom(dcTextField.text!)
            
            if avMinutes >= 60  {
                let hours = avMinutes / 60          // e.g. 92 => 1
                avMinutes = avMinutes - (hours*60)  // => 32
                avHours += hours
                changed = true
            }
            if avHours >= 24    {
                avHours = avHours % 10  // just clean the first digit, e.g. 35=>5
                changed = true
            }
            
            if changed {
                dcTextField.text = WMUtils.hhmmStringFrom(avHours, avMinutes)
            }
        }
        return changed
    }
    func setValue(hhmmValue: String)    {
        
        if hhmmValue.count != 5 {
            return // ignore
        }

        (avHours, avMinutes) = WMUtils.hhmmIntsFrom(hhmmValue) // .hhmmIntsFrom(dcTextField.text!)
        
        dcTextField.text = WMUtils.hhmmStringFrom(avHours, avMinutes)
        
    }
    
    
    @IBAction func digitAction(sender: UIButton, event: UIEvent)    {
        
        if sender.tag == -1 {   // backKey
            
            // shift right, removing left
    
            let d3: Int = avHours % 10      // (12, 34)  => 2
            avHours /= 10                   // => (01,  34)
            avMinutes += d3*100             // => (01, 234)
            avMinutes /= 10                 // => (01,  23)
            
        } else {                // digit key
            
            // shift left, adding tag to right
            
            avMinutes = avMinutes*10 + sender.tag   // (12, 34) + 1 => (12, 341)
            let d2: Int = avMinutes / 100           // 3
            avHours = avHours*10 + d2                           // => (123, 341)
            avMinutes -= d2 * 100                   // => (123,  41)
            let d4: Int = avHours / 100             // 1
            avHours -= d4 * 100                     // => ( 23,  41)
        }
        
        dcTextField.text = WMUtils.hhmmStringFrom(avHours, avMinutes)
    }

}
