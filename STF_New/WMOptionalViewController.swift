//
//  WMOptionalViewController.swift
//  STF_New
//
//  Created by Christophe Delannoy on 13/01/2022.
//  Copyright © 2022 Wan More. All rights reserved.
//

import UIKit

// renamed, was: WMDisclaimerViewController; object: refactoring
class WMOptionalViewController: UIViewController {

    @IBOutlet weak var dTitle: UILabel!
    @IBOutlet weak var dContent: UITextView!
    
    static var dIdentifier = "" // set from caller, @see presentIfNeverSeen
    static var dFlag = ""

    private func _localizedString(_ key: String) -> String  {
        let fullKey = "\(Self.dIdentifier)_\(key)"
        let result = NSLocalizedString(fullKey, tableName: "WMDisclaimer", bundle: Bundle.main, value: "", comment: "")
        return result
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        dTitle.text = _localizedString("Title")
        dContent.text = _localizedString("Content")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func okButton(sender: UIButton)    {
        
        let userDefaults: UserDefaults = UserDefaults.standard
        var shown = userDefaults.bool(forKey: Self.dFlag)
        shown = true
        userDefaults.setValue(shown, forKey: Self.dFlag)
        userDefaults.synchronize()
        
        self.dismiss(animated: true, completion: nil)
        
        Self.dIdentifier = ""
        Self.dFlag = ""
    }
    
    /// check if a ViewController was displayed, if not does it; only one is allowed at a time!  This is internally filtered.
    /// - parameter identifier: an identifier for the content (strings) to display, load from NSLocalizedString
    /// - returns YES if a Controller is presenting
    static func presentIfNeverSeen(identifier: String, fromController: UIViewController ) -> Bool {
        
        if dFlag.count > 0    {
            // cancel, because another UIViewController is in use;
            // * when this one is dismissed, the caller will fall in viewDidAppear (if UIModalPresentationStyle.fullScreen) and will call us again
            return false
        }
        dIdentifier = identifier
        dFlag = "\(dIdentifier)Shown" // e.g. "disclaimerShown"
        let userDefaults: UserDefaults = UserDefaults.standard
        let shown = userDefaults.bool(forKey: dFlag)

        if shown == false   {
            var storyBoardName = "Main"
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                storyBoardName = "Main_iPad"
            }
            let storyBoard = UIStoryboard.init(name: storyBoardName, bundle: Bundle.main)

            if let disclaimer: WMOptionalViewController = storyBoard.instantiateViewController(withIdentifier: "disclaimer") as? WMOptionalViewController
            {
                disclaimer.modalPresentationStyle = UIModalPresentationStyle.formSheet
                // fullScreen, overFullScreen
                // default: pageSheet
                
                fromController.present(disclaimer, animated: true)
                return true
            }
            // note: will update userDefaults>xxShown when in disclaimer->okButton()
        }

        dIdentifier = ""
        dFlag = ""
        return false
    }
    // ???: should be in: extension UIViewController... not sure because of flags
}
