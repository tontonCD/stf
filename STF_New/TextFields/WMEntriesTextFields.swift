//
//  WMEntriesTextFields.swift
//  STF_New
//
//  Created by Christophe Delannoy on 09/11/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class WMEntriesTextFields: WMDataTextFields {

    /// offset from top or bottom (common to all fields)
    static var etfDefault_y : CGFloat = 10.0
    
    /// enables: displayedEntries[entry : i]
    subscript(entry index: Int) -> WMEntry? {
        get {
            if self._dtDataTextFields.indices.contains(index)   {
                let result: WMEntry = (self._dtDataTextFields[index].deDataTextField as! MWEntryTextField).etfEntry!
                return result
            }
            return nil
        }
    }

    func lastEntry( )-> WMEntry? {
        let result: WMEntry = (self._dtDataTextFields.last?.deDataTextField as! MWEntryTextField).etfEntry!
        return result
    }
    
    /// giving an iOS Date, returns the index for the nearest EntryTextField at Left; possibly with identical Date
    func findEntryTextFieldIndexAtLeft(date: Date) -> (Int, MWEntryTextField?)  {
        let (entryTextFieldIndex, entryTextField) = findTextFieldIndexAtLeft(date: date)
        if let etfTest = entryTextField as? MWEntryTextField    {
            return (entryTextFieldIndex, etfTest)
        }
        return (-1, nil)
    }
    
    /// the search is based on the eDocID
    func findIndexForEntry(_ entry: WMEntry) -> Int {
        var resultIndex = -1
        for (entryIndex, displayedEntry) in _dtDataTextFields.enumerated()   {
            
            // only for EntryTextFields: check type for deDataTextField
            guard let entryTextField: MWEntryTextField =
                    displayedEntry.deDataTextField as? MWEntryTextField else   {
                // useless to check others
                return resultIndex // check: can return entryIndex?
            }
            
            let etfText: String = (entryTextField.etfEntry?.eDocID)!
            if etfText.elementsEqual(entry.eDocID!)   {
                resultIndex = entryIndex
                return resultIndex
            }
        }
        return -1
    }

    /// - returns the first field containing the point (parameter)
    func findEntryTextField(where point: CGPoint) -> MWEntryTextField?   {
        
        var result: MWEntryTextField? = nil
        
        for dataTextField in _dtDataTextFields   {
            if dataTextField.deDataTextField.frame.contains(point)    {
                
                result = dataTextField.deDataTextField as? MWEntryTextField
            }
        }
        return result
    }
    
    /// removes an Entry, for Display only (database untouched!)
    /// - returns index for the Entry before removing it, so (-1) if not found
    func removeEntry(_ entry: WMEntry) -> Int { 

        let removeIndex = self.findIndexForEntry(entry)
        if removeIndex >= 0 {
            self._removeData(removeIndex)
        }
        return removeIndex
    }
    
}
