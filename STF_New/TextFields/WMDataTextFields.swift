//
//  WMDataTextFields.swift
//  STF_New
//
//  Created by Christophe Delannoy on 04/09/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

// replaces: gvDisplayedEntries: [DisplayedEntry], in graphicView

/* ABSRACT:
 * Data stand for both Entry and Date
 * the difference is that Entris are persistent (via database), Dates are not.
 */

/// to be used as an Array; for both Entry and Date;
struct WMDataTextField {
    var deRevID: String    
    var deDataTextField: MWAbstractDataTextField
}


//protocol WMDataTextFieldsDelegate   {
//    /// allows addtoSuperview
//    func superView() -> UIView
//}

/// manages an Array<WMDataTextField>; for both Entry and Date;
class WMDataTextFields: NSObject, Sequence, IteratorProtocol {
    
    var delegate: UIView?
    
    /// identifier (for debug)
    var dtIdentifier: String = "(?)"
    
    /// the data
    internal var _dtDataTextFields: [WMDataTextField] = [] 
    /// used from darw(); private, use: var leftTabNum (get only)
    private var _dtLeftTabNum: Int = 0 // replaces: gvEntries_LeftTabNum
    /// used from darw(); private, use: var rightTabNum (get only)
    private var _dtRightTabNum: Int = 0 // unused yet
    
    ///
    private var _dtFieldsWidth: CGFloat = 10.0 // 47.0 for TF displaying "hh:mm"
    
    @inlinable var count: Int  { // TODO: try to omit "get"; https://github.com/raywenderlich/swift-style-guide#computed-properties
        /*get { */return _dtDataTextFields.count /*}*/
    }
    @inlinable var first: WMDataTextField?  {
        get { return _dtDataTextFields.first  }
    }
    @inlinable var firstDate: Date?  {
        get { return _dtDataTextFields.first?.deDataTextField.date  }
    }
    @inlinable var last: WMDataTextField?  {
        get { return _dtDataTextFields.last  }
    }
    @inlinable var fieldsWidth: CGFloat  { // todo: unused; replace frameSizeWidth? gvDisplayedDates.first!.deDataTextField.frame.size.width
        get { return _dtFieldsWidth  }
    }
    @inlinable var frameSizeHeight: CGFloat  {
        get { return _dtDataTextFields.first?.deDataTextField.frame.size.height ?? 0.0  }
    }
    @inlinable var frameSizeWidth: CGFloat  {
        get { return _dtDataTextFields.first?.deDataTextField.frame.size.width ?? 0.0  }
    }
    @inlinable var leftTabNum: Int          {
        get { return _dtLeftTabNum      }
    }
    @inlinable var rightTabNum: Int         {
        get { return _dtRightTabNum     }
    }


    /// insert in the way as the result is sorted by Date
    /// - returns: index for the parameter where inserted (so, index from which Levels need to be recomputed)
    func insert(newDataTF: WMDataTextField) -> Int   {
        
        /// index to increment until we find a Date highter then the parameter Date
        var prevDateIndex: Int = 0
        
        if _dtDataTextFields.count == 0                                                    {
            _dtDataTextFields.append( newDataTF)
            prevDateIndex = 0
        } else if  newDataTF.deDataTextField.date > (_dtDataTextFields.last?.deDataTextField.date)!    {

            // the parameter Date is highter than all of the Array
            // (this test could be avoid, but we can know if inserted VS append)
            
            _dtDataTextFields.append( newDataTF)
            prevDateIndex = _dtDataTextFields.count - 1
        } else                                                                              {

            // increment index:
            for displayedEntry in _dtDataTextFields   {
                
                if newDataTF.deDataTextField.date < displayedEntry.deDataTextField.date    {

                    break
                }

                prevDateIndex += 1
                
            }
            _dtDataTextFields.insert(newDataTF, at: prevDateIndex)
        }
        
        delegate?.addSubview(newDataTF.deDataTextField)
        
        return prevDateIndex
    }
    
    /// inserts
    // - returns: index for the parameter where inserted (so, index from which Levels need to be recomputed)
    func insert(_ newEntry: WMDataTextField, at atIndex: Int) /* -> Int */  {
        _dtDataTextFields.insert(newEntry, at: atIndex)
    }

    
    /* MWDateTextField extentions */
    
    
    /// giving an iOS Date, returns the index for the nearest MWAbstractDataTextField at Left; possibly with identical Date
    func findTextFieldIndexAtLeft(date: Date) -> (Int, MWAbstractDataTextField?) {
        
        var resultTextField: MWAbstractDataTextField? = nil
        var resultIndex: Int = -1
        
        if _dtDataTextFields.count == 0   {
            return (-1, nil)
        }
        

        for entryIndex in 0..<_dtDataTextFields.count   {
            
            let textField: MWAbstractDataTextField = _dtDataTextFields[entryIndex].deDataTextField
            let entryDate: Date = textField.date
            
            if entryDate > date {
                
                //return resultTextField
                return (resultIndex, resultTextField)
            }
            
            resultTextField = textField // will be the previous compared to the next iteration
            resultIndex = entryIndex
        }
        
        return (resultIndex, resultTextField)
    }
    
    
    /// - note: don't use directly but from subclasses
    /// - since 0.42 woks both with Entries and Dates
    internal func  _removeData(_ dataIndex: Int) /* -> Int */ { // TODO: displace
        // var removedIndex = -1
        
        // remove from Array
        let displayedEntry = _dtDataTextFields[dataIndex]
        _dtDataTextFields.remove(at: dataIndex)
        displayedEntry.deDataTextField.removeFromSuperview()
        
        print("did remove old \(dtIdentifier) Field from Screen: \(displayedEntry.deDataTextField.getHHMMString())")

        if displayedEntry.deDataTextField.adtfTabbed   {
            // this Entry was tabbed, update the count:
            _dtLeftTabNum -= 1
            // TODO: should check for tabbedLeft/tabRight, for now we never tabRight
            print(" leftTabNum is now: \(_dtLeftTabNum)")
            // debug
            if  _dtLeftTabNum < 0   {
                print("Error: leftTabNum")
            }
        }
    }
    
    /// checks distance (time) to the Current Date. May remove some TextFields
    /// - note: default distance is 24h
    /// - note: can remove tabbed item as well as un-tabbed ones
    /// - since 0.43 used for both Dates and Entries
    func removeWithDateDistance() -> Bool  { 
        
        var didChange = false
        
        if count < 1    {
            return false
        }

        // check for removals; // note: always remove at index zero, even if repeated.
        while _dtDataTextFields.count > 0 {
            
            let displayedEntry = _dtDataTextFields[0]
            
            let distanceToCurrentDate = Date().timeIntervalSince(displayedEntry.deDataTextField.date)
            if distanceToCurrentDate < 24*3600                      {
                // no more removing:
                break;
            }
            
            self._removeData(0)
           
            didChange = true
        }
        
        return didChange
    }



    /// enables: displayedEntries[i]
    subscript(index: Int) -> WMDataTextField? {
        get { return self._dtDataTextFields.indices.contains(index) ? self._dtDataTextFields[index] : nil }
    }
    
    
    /// enables: displayedEntries[entryTextField: i]
    subscript(entryTextField index: Int) -> MWAbstractDataTextField? {
        get { return self._dtDataTextFields.indices.contains(index) ? self._dtDataTextFields[index].deDataTextField : nil }
    }
    /// enables: displayedEntries[date: i]
    subscript(date index: Int) -> Date? {
        get { return self._dtDataTextFields.indices.contains(index) ? self._dtDataTextFields[index].deDataTextField.date : nil }
    }

    
    /// enables: "for x in WMDisplayedEntries"
    var currentIndex = 0
    /// - note: if using break or return, in a for.in loop, you must explicitely reset currentIndex to 0
    func next() -> WMDataTextField? {
        if currentIndex >= _dtDataTextFields.count {
            currentIndex = 0 // for the next time!
            return nil
        } else {
            
            defer {
                currentIndex += 1
            }
            
            return _dtDataTextFields[currentIndex]
        }
    }
    
    
    
    /*
     *
     *
     */
    
    /// called when moving a TextField Entry in to the TabZone;
    /// - note: if a Date, it will be removed from the main view // todo: embed this
    func doTabLeft(atIndex: Int)    { // check: index should always be the firt not-tabbed, so _dtLeftTabNum, simplify

        let dataTextField =     self[entryTextField: atIndex]
        if dataTextField!.adtfTabbed == true {
            return // error from caller
        }
        if _dtLeftTabNum + 1 == _dtDataTextFields.count {
            return // error from caller; in fact means that all Entries will be tabbed...
        }

        dataTextField!.doTab()
        _dtLeftTabNum += 1
        print("did tab \(dtIdentifier):  \(dataTextField!.getHHMMString())")
    }
    /// called when moving a TextField Entry out from the TabZone;
    /// /// - note: if a Date, it will be added to the main view  // todo: embed this
    func doUntabLeft(atIndex: Int)    {
        // check: index should always be the firt not-tabbed, so _dtLeftTabNum, simplify
        
        let dataTextField =     self[entryTextField: atIndex]
        if dataTextField!.adtfTabbed == false    {
            print("error doUntabLeft")
            return
        }

        dataTextField!.doUntab()
        _dtLeftTabNum -= 1
        
        delegate?.addSubview(dataTextField!)
        // ???: can use .hidden false/true instead
        
        print("did untab \(dtIdentifier):  \(dataTextField!.getHHMMString())")
        
    }

    
    func firstUntabbedIndex() -> Int {
        var firstUntabbedIndex: Int = 0
        while firstUntabbedIndex < _dtDataTextFields.count  {
            if _dtDataTextFields[firstUntabbedIndex].deDataTextField.adtfTabbed == false {
                break;
            }
            firstUntabbedIndex += 1
        }
        // TEST
        if firstUntabbedIndex != _dtLeftTabNum  {
            print("error, firstUntabbedIndex()")
        }
        // TODO: when OK (check), try to deal only with _dtLeftTabNum (remove .adtfTabbed)
        // check also firstUntabbed()
        
        if firstUntabbedIndex >= _dtDataTextFields.count  {
            print("error, firstUntabbedIndex()")
            return -1
        }
        
        return firstUntabbedIndex
    }
    /// - parameter offset Optional, makes possible to offset the found index, e.g. if (-1), you get the last tabbed element (from Left)
    func firstUntabbed(_ offset: Int = 0) -> WMDataTextField {
        var firstUntabbedIndex: Int = 0
        while firstUntabbedIndex < _dtDataTextFields.count  {
            if _dtDataTextFields[firstUntabbedIndex].deDataTextField.adtfTabbed == false {
                break;
            }
            firstUntabbedIndex += 1
        }
        // TEST
        if firstUntabbedIndex != _dtLeftTabNum  {
            print("error, firstUntabbed()")
        }
        // TODO: simplify: use firstUntabbedIndex()
        // check this:
        firstUntabbedIndex = self.firstUntabbedIndex()
        if firstUntabbedIndex != _dtLeftTabNum  {
            print("error, firstUntabbedIndex() from firstUntabbed()")
        }
        
        // BUG to fix, check:
        var result = firstUntabbedIndex + offset

        if result < 0   {
            result = 0 // bug (?)
        }
        if result >= _dtDataTextFields.count {
            result = _dtDataTextFields.count - 1 // bug?
            // -> no here with the result, because of tests;
            // this means that all Entries are tabbed, updateZoom() should have been called before removing the last
            print("Error, all Entries are tabbed")
            // correction is in doTabLeft()
        }
        return _dtDataTextFields[result]
    }
    
    /// - parameter offset Optional, makes possible to offset the found index, e.g. if (+1), you get the last tabbed element from Right
    func lastUntabbed(_ offset: Int = 0) -> WMDataTextField {
        var lastUntabbedIndex: Int = _dtDataTextFields.count - 1
        while lastUntabbedIndex > 0  {
            if _dtDataTextFields[lastUntabbedIndex].deDataTextField.adtfTabbed == false {
                break;
            }
            lastUntabbedIndex -= 1
        }
        // TEST
        if lastUntabbedIndex != _dtDataTextFields.count - 1 - _dtRightTabNum {
            print("error, lastUntabbed()")
        }
        // TODO: ditto (see supra)
        
        
        if lastUntabbedIndex + offset < _dtDataTextFields.count   {
            return _dtDataTextFields[lastUntabbedIndex + offset]
        }
        return _dtDataTextFields[count-1]
    }

    
    /// checks recovering. Updated: distance (x-position)  must be at least 2.
    /// - important Must be called on an instance covering Entries
    func checkTextFieldsRecovering() -> Bool  {
        // TODO: subclass (or handle both arrays in one instance?)
        // NOTE: "Must be called on an instance covering Entries" not sure...
        
        if count < 1    {
            return false
        }
        
        var hasRecovering: Bool = false
        var prevDisplayedEntry = _dtDataTextFields.first!
        
        // look for the first untabbed
        let firstUntabbed: Int = firstUntabbedIndex()
        if firstUntabbed < 0    {
//            return false
            // !!!: this mean that no Entry appear, so, no Entry for a long time: check for hiding green/orange times and remove Dates
            
            // TODO: returning from here implies that no control applied to Dates
            // try this:
            return true
            // !!!: should separate checking for Date Removals
        }
        prevDisplayedEntry = _dtDataTextFields[firstUntabbed]
            

        var displayedEntryIndex: Int = firstUntabbed + 1
        while displayedEntryIndex < _dtDataTextFields.count {
            
            let displayedEntry = _dtDataTextFields[displayedEntryIndex]
            
            if displayedEntry.deDataTextField.adtfTabbed { // should not occur
                // ignore
                displayedEntryIndex += 1
                continue
            }
            
            // Note: if the tabb Zone active, the distance between the first untabbed element and the tabMark is set to 10 (@see update Draw(), update updateZoom())
            
            if displayedEntry.deDataTextField.frame.minX < prevDisplayedEntry.deDataTextField.frame.maxX + 2  {
                
                print("found recovering between: \(prevDisplayedEntry.deDataTextField.getHHMMString()) and \(displayedEntry.deDataTextField.getHHMMString())")
                
                hasRecovering = true
                
                return hasRecovering
                
            }
            
            prevDisplayedEntry = displayedEntry
            displayedEntryIndex += 1
        }
        
        return hasRecovering
    } // end: checkTextFieldsRecovering()
    
    
    func tabAtIndex(_ index: Int)   { // !!!: compare to: doTabLeft(atIndex: Int)

        _dtDataTextFields[index].deDataTextField.doTab()
        print("did tab Entry: \(_dtDataTextFields[index].deDataTextField.getHHMMString())")
       _dtLeftTabNum += 1
    }
}
