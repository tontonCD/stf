//
//  MWInfoTextField.swift
//  STF_New
//
//  Created by Christophe Delannoy on 23/10/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class MWInfoTextField: MWEntryTextField   {
    
    
    override func setDate(date: Date)    {
        
        super.setDate(date: date)
       
        let textFieldText = self.getHHMMString()
        self.text = textFieldText
        self.sizeToFit() // (trace) => 59, 34
        
        self.sizeThatFits(CGSize.zero) // trace -> no change
    }

    
    convenience init(date: Date)  {
        
        self.init(frame: Self.baseFrame)
        
        self.borderStyle = .roundedRect // default: .line
        self.layer.cornerRadius = 3.0
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0

        self.font = UIFont.italicSystemFont(ofSize: 12.0)
       
        self.setDate(date: date) // will call sizeToFit() => (59, 34)
        
        self.frame.size = CGSize(width: 60, height: 20)
    }
} // MWCurrentDateTextField
