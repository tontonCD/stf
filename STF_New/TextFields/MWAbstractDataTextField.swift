//
//  MWAbstractDataTextField
//  STF_New
//
//  Created by Christophe Delannoy on 18/07/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//


/* object: usage for both Entries and Date TextFields */
import UIKit
import CDTDatastore

/// abstract class for TextFields; changes will be on Vertical position, the way the Date is displayed, if tracked on Database...
class MWAbstractDataTextField: UITextField {

    static let baseFrame: CGRect = CGRect(x: 10.0, y: 10.0, width: 10.0, height: 10.0)
    static var startingDate: Date = WMDateUtils.dateAtMidnight(fromDate: Date())
    
    var adtfX: CGFloat = 0.0 // currently, only used from MWInfoTextField
    
    private var _adtfTabbed: Bool = false
    
    /// needs a particular attention (visibility, etc...; @see subclasses)
    var adtfTabbed: Bool {
        get {
            return _adtfTabbed
        }
    }
    func doTab()    {
        _adtfTabbed = true
    }
    func doUntab()    {
        _adtfTabbed = false
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    private var _date: Date?
    var date: Date {
        get         {
            return self._date ?? Date()
        }
        set(inDate) {
            self._date = inDate
            self._date_Hours = WMDateUtils.hoursString(date: inDate)
        }
    }
    private var _date_Hours: String = ""
    
    func setDate(date: Date)    {
        _date = date
        _date_Hours = WMDateUtils.hoursString(date: date)
    }

    override init(frame: CGRect) {
        

        super.init(frame: frame)
        
        _setUp()
    }
    

    convenience init()  {

        /* newTextField */

        self.init(frame: Self.baseFrame)
        
        _setUp()
    }
    
    required init?(coder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        
        super.init(coder: coder)
    }
    
    
    private func _setUp()   {
        
        self.textAlignment = NSTextAlignment.center
        self.borderStyle = UITextField.BorderStyle.line
        self.isUserInteractionEnabled = false

        
        /* change the Font Size */
        var newFont: UIFont?
        if self.font!.fontName.elementsEqual(".SFUI-Regular")   {
            // avoid execution warning
            newFont = UIFont.systemFont(ofSize: 14.0)
        } else {
            newFont = UIFont(name: self.font!.fontName, size: 14)
        }
        if (newFont != nil) {
            self.font = newFont
        }
    }
    
    
    func getHHMMString()  -> String   { // TODO: will replace stringToMinutes()

        let hhmmss: String = String( self._date_Hours.split(separator: " ").last ?? "00:00:00" ) // default using hh:mm:ss because of dropLast(3)
        let hhmm: String = String( hhmmss.dropLast(3) )
        return hhmm
    }
    func getHHString()  -> String   {
        
        let hhmmss: String = String( self._date_Hours.split(separator: " ").last ?? "00:00:00" ) // default using hh:mm:ss because of dropLast(6)
        //var substrings: [Substring] = stringValue.split(separator: ":")
        let hh: String = String( hhmmss.dropLast(6) )
        return hh
    }
    func getHHMM() -> (Int, Int)    {
        let hhmmss = self.getHHMMString()
        let substrings: [Substring] = hhmmss.split(separator: ":")
        return (Int( substrings[0] ) ?? 0, Int( substrings[1] ) ?? 0)
    }
    func getHHMMInt()  -> Int   { // TODO: remove, or replace with xx.date.timeIntervalSince(startingDate)
        
        let result = Int(self.date.timeIntervalSince(Self.startingDate)) / 60
        
        // NOTE: be aware that Self.startingDate can change, so,
        // - ensure that all comparison based on this call must be in the same block
        
        return result
    }
        
} // MWAbstractDateTextField










