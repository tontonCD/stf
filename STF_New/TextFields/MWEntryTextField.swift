//
//  MWEntryTextField.swift
//  STF_New
//
//  Created by Christophe Delannoy on 23/10/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit


class MWEntryTextField: MWAbstractDataTextField {
    
    /// ref for a database Entry; makes possible to Edit or Remove
    var etfEntry: WMEntry?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    override func setDate(date: Date)    {
        super.setDate(date: date)
       
        let textFieldText = self.getHHMMString()
        self.text = textFieldText
        self.sizeToFit() // (trace) => 47.0, 26.5
    }

    convenience init(withEntry: WMEntry)  {
        
        let fullDate: Date = withEntry.getDate()!
        
        self.init(date: fullDate)
        etfEntry = withEntry
        
    }
    
    
    convenience init(date: Date)  {
        
        self.init(frame: Self.baseFrame)
        
        setDate(date: date)
        
        // it would be nice to have the same width, for zomm calculations (avoiding overleaps)
        if self.bounds.size.width > 48.0    {
            print("debug") // found: 48.0 (on iPhone 6s)
        }
    }

    required init?(coder: NSCoder) { // called when using storyboard
       // fatalError("init(coder:) has not been implemented")
        
        super .init(coder: coder)
        
    }
} // MWEntryTextField
