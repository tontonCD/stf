//
//  MWDateTextField.swift
//  STF_New
//
//  Created by Christophe Delannoy on 23/10/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class MWDateTextField: MWAbstractDataTextField {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func doTab()    {
        super.doTab()
        self.isHidden = true
    }
    override func doUntab()    {
        super.doUntab()
        self.isHidden = false
    }

    
    override func setDate(date: Date)    {
        super.setDate(date: date)
       
        let textFieldText = self.getHHString()
        self.text = textFieldText
        self.sizeToFit() // (trace) => 26.0, 26.5
    }

    convenience init(date: Date)  {
        
        self.init(frame: Self.baseFrame)
        
        setDate(date: date)
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
