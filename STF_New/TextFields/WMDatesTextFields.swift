//
//  WMDatesTextFields.swift
//  STF_New
//
//  Created by Christophe Delannoy on 09/11/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class WMDatesTextFields: WMDataTextFields {
    
    /// offset from top or bottom (depending on subclasses)
    static var dtfDefault_y : CGFloat = 10.0
    // note: replaces gvDisplayedDates_y
    
    /// giving an iOS Date, returns the index for the nearest DateTextField at Left; possibly with identical Date
    func findDateTextFieldIndexAtLeft(date: Date) -> (Int, MWDateTextField?)    {
        let (dateTextFieldIndex, dateTextField) = findTextFieldIndexAtLeft(date: date)
        if let dtfTest = dateTextField as? MWDateTextField      {
            return (dateTextFieldIndex, dtfTest)
        }
        return (-1, nil)
    }

    
    /// insert Dates TF
    private func insert(_ dateTF: MWDateTextField, at atIndex: Int)    {
        
        let newDataTF = WMDataTextField(deRevID: "", deDataTextField: dateTF)
        
        _dtDataTextFields.insert(newDataTF, at: atIndex)
        
        delegate?.addSubview(dateTF)
        
        print("Inserted Date: \(dateTF.getHHString())h")
    }
    /// append
    private func append(_ dateTF: MWDateTextField)                     {
        
        let newDataTF = WMDataTextField(deRevID: "", deDataTextField: dateTF)
        
        _dtDataTextFields.append(newDataTF)
        
        delegate?.addSubview(dateTF)
        
        print("Added Date:    \(dateTF.getHHString())h")
    }
    /// shortcut
    func appendDate(date inDate: Date) -> MWDateTextField   {
        let newDateTextField: MWDateTextField = MWDateTextField.init(date: inDate)
        
        // security:
        newDateTextField.frame.origin.y = WMDatesTextFields.dtfDefault_y
        
        self.append(newDateTextField)
        delegate?.addSubview(newDateTextField)
        return newDateTextField
    }
    
    /// shortcut
    func insertDate(date inDate: Date, at: Int) -> MWDateTextField   {
        let newDateTextField: MWDateTextField = MWDateTextField.init(date: inDate)
        self.insert(newDateTextField, at: at)

        return newDateTextField
    }
    
    /// shortcut; inserts at left
    /// - parameter offset: should be negative (usually -3600); to add at right better use append()
    func insertNewDate(offset: TimeInterval /* = -3600 */) -> MWDateTextField   {
        let newDateTimeDate = Date(timeInterval: offset, since: _dtDataTextFields.first!.deDataTextField.date)
        return self.insertDate(date: newDateTimeDate, at: 0)
    }
    
    /// removes items where date is to old
    /// - note: can remove tabbed item as well as un-tabbed ones
    func removeUntilDate(date inDate: Date) -> Bool  {
        // ???: never called, @see WMDataTextFields->removeWithDateDistance
        
        var didChange = false
        
        if count < 1    {
            return false
        }

        // check for removals; // note: always remove at index zero, even if repeated.
        while _dtDataTextFields.count > 0 {
            
            let displayedEntry = _dtDataTextFields[0]

            if displayedEntry.deDataTextField.date > inDate    {
                // no more removing:
                break;
            }
            
            self._removeData(0)
           
            didChange = true
        }
        
        return didChange
    }
}
