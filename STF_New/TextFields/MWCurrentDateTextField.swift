//
//  MWCurrentDateTextField.swift
//  STF_New
//
//  Created by Christophe Delannoy on 23/10/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

class MWCurrentDateTextField: MWEntryTextField   {
    
    /// flag that says that the draw() function needs to draw the MWCurrentDateTextField instance only
    var cdNeedsUpdate: Bool = true // usefull? (perheaps OBSO)

    /// display message at launch, until an Entry is built
    private var _cdDisplayMessage = true
    /// - parameter :  if false, the TextField will display instructions, if true, will display the current Date ; the Date is set to the current One, and the display updated
    /// - note It is important to know tha the TextField width will be upated
    func useMessage(_ use: Bool)    {
        _cdDisplayMessage  = use
        self.setDate(date: Date())
    }
    
    
    private func _localizedString(_ key: String) -> String  {
        let result = NSLocalizedString(key, tableName: "MWCurrentDateTextField", bundle: Bundle.main, value: "", comment: "")
        return result
    }

    override func draw(_ rect: CGRect) {
       
        // TEST: displace some draw() to the relative textField (-> fails)
        
        let path: UIBezierPath = UIBezierPath()
        
        let currentX = rect.midX
        let currentY = rect.midY
        
        path.move(to:    CGPoint(x: currentX-20.0, y: currentY-20.0) )
        
        path.addLine(to: CGPoint(x: currentX-20.0, y: currentY+20.0) )
        path.addLine(to: CGPoint(x: currentX+20.0, y: currentY-20.0) )
        path.addLine(to: CGPoint(x: currentX+20.0, y: currentY+20.0) )
        
        path.addLine(to: CGPoint(x: currentX-20.0, y: currentY-20.0) )

        UIColor.black.setStroke()
        path.stroke()
        
       // super.draw(rect)
    }
    override func setDate(date: Date)    {
        
        super.setDate(date: date)
       
        let textFieldText = self.getHHMMString()
        self.text = textFieldText
//        self.sizeToFit() // (trace) => 42.0
        
        if _cdDisplayMessage {
            self.text = _localizedString("Double-Tap")
        } else {
                
            let attString = NSMutableAttributedString.init(string: self.text!)
            let newDate_secs: Int = Int(WMDateUtils.hoursString(date: date).dropFirst(6))!
            if newDate_secs % 2 == 0    {
                
                attString.addAttributes([ NSAttributedString.Key.foregroundColor: UIColor.white ], range: NSRange(location: 2, length: 1)) // for ":" solely
                
            }
            self.attributedText = attString
  
        }
        
        self.sizeToFit()
        
    }

    
    convenience init(date: Date)  {
        
        self.init(frame: Self.baseFrame)
        
        self.borderStyle = .roundedRect // default: .line
        self.layer.cornerRadius = 5.0
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
        self.minimumFontSize = 16 // default -> font-family: ".SFUI-Regular"; font-weight: normal; font-style: normal; font-size: 14.00pt
        self.setDate(date: date) // will call sizeToFit() => (64, 34)
    }
} // MWCurrentDateTextField
