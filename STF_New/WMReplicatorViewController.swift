//
//  WMReplicatorViewController.swift
//  STF_New
//
//  Created by Christophe Delannoy on 13/01/2022.
//  Copyright © 2022 Wan More. All rights reserved.
//

import UIKit

class WMReplicatorViewController: UIViewController {

    @IBOutlet weak var rActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var rTitle:  UILabel!
    @IBOutlet weak var rTitle2: UILabel!
    @IBOutlet weak var rURL: UILabel!
    // TODO:
    @IBOutlet weak var rImageUpView: UIImageView!
    @IBOutlet weak var rImageDownView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        _doReplication() 
    }
    private func _doReplication()   {

        let replicator: WMReplicator  = WMReplicator()
        
        rURL.text = replicator.address
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        
        rActivityIndicator.startAnimating()
        rImageDownView.alpha = 1.0
        rImageUpView.alpha =   0.25
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        /*let send*/_ = replicator.getsendData(option: ReplicatorWay.wSend)
        
        rImageDownView.alpha = 0.25
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        rImageUpView.alpha =   1.0
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        /*let get*/_ = replicator.getsendData(option: ReplicatorWay.wGet)
        
        rImageUpView.alpha =   0.25
        RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.5))
        rActivityIndicator.stopAnimating()
        
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
