//
//  WMUserViewController.swift
//  STF_New
//
//  Created by Christophe Delannoy on 09/11/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import UIKit

protocol WMUserViewControllerDelegate: NSObject   {
    func didSetCurrentUser(user: WMUser)
}

class WMUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var chooseUserTableView: UITableView!
    var sUsers: [String] = [] // ["Add User"]
    var sCurrentUser: Int = -1
    let cellReuseIdentifier = "userCell"
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var okButton:         UIButton!
    @IBOutlet weak var cancelButton:     UIButton!
    @IBOutlet weak var createUserButton: UIButton!
    
    weak var delegate: WMUserViewControllerDelegate? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        chooseUserTableView.isHidden = true
        // Register the table view cell class and its reuse id
        chooseUserTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        chooseUserTableView.delegate = self
        chooseUserTableView.dataSource = self

        userNameTextField.delegate = self
        
        
        createUserButton.isHidden = true
        if sUsers.count == 0    {
            createUserButton.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let userDefaults: UserDefaults = UserDefaults.standard
        let currentUserName = userDefaults.string(forKey: "currentUser")

        userNameTextField.text = currentUserName
            
        super.viewDidAppear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func okAction(sender: UIButton)   {
        
        if userNameTextField.text != nil    {
            
            let userDefaults: UserDefaults = UserDefaults.standard

            userDefaults.setValue(userNameTextField.text, forKey: "currentUser")
            userDefaults.synchronize()
            
            _ = WMUser.loadUser(userName: userNameTextField.text!)
        }
        // todo: should have disable OK if userNameTextField nil
        // todo: should have disable cancel, if userNameTextField AND no current User
        // todo: should enable cancel if called after Start (from Settings), i.e. if no current User
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelAction(sender: UIButton)   {

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func createUserAction(sender: UIButton)   {
        
        if userNameTextField.text != nil    {
            
            sUsers.insert(userNameTextField.text!, at: 0)
            sCurrentUser = 0
            
//            chooseUserTableView.setNeedsDisplay()
            chooseUserTableView.reloadData()
            
            chooseUserTableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: UITableView.ScrollPosition.top)
            createUserButton.isHidden = true
            
            userNameTextField.endEditing(true)
        }
        
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sUsers.count //+ 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: cellReuseIdentifier)
        }
        //cell.textLabel = sUsers[row]
        
//        if row < sUsers.count   {
            cell!.textLabel?.text = self.sUsers[indexPath.row]
            
//        } else
//        {
//            // let conf: UIListContentConfiguration = UIListContentConfiguration(row)
//            //cell.textLabel?.text = AttributedString("Add User", attributes: AttributeContainer([NSAttributedString.Key.obliqueness : 1.5]))
//            //cell.font?.fontDescriptor = UIFontDescriptor.init(fontAttributes: [UIFontDescriptor.AttributeName : Any])
//            //           self.font = UIFont.italicSystemFont(ofSize: 12.0)
//
//            // iOS 14.0
//            //            cell.contentConfiguration
//
//            cell!.textLabel?.text = "ooooo"
//            cell!.textLabel?.font = UIFont.italicSystemFont(ofSize: 12.0)
//
//            let test = cell?.subviews
//            // TODO: try to embed an editable textview
//
//            let textField: UITextField = UITextField(frame: cell!.frame)
//            textField.text = "Add User"
//            textField.font = UIFont.italicSystemFont(ofSize: 12.0)
//            textField.delegate = self
//
//            cell?.addSubview(textField)
//
//
//        }
        
        return cell!
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row < sUsers.count   {
            
        sCurrentUser = indexPath.row
        
        userNameTextField.text = sUsers[sCurrentUser]
        
////            tableView.isHidden = true
//        } else
//        {
//            // TODO: new user
//
//            // check: could be set from System Prefs?
//        }
    }
    
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField)  {
        // TODO:
//        if textField.text != nil    {
//
//            sUsers.insert(textField.text!, at: 0)
//            sCurrentUser = 0
//
////            chooseUserTableView.setNeedsDisplay()
//            chooseUserTableView.reloadData()
//
//            chooseUserTableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: UITableView.ScrollPosition.top)
//            createUserButton.isHidden = true
//        }
    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        // TODO:
//        
//        createUserButton.isHidden = false
//    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text != nil && !sUsers.contains(textField.text!)  {
            createUserButton.isHidden = false
        } else {
            createUserButton.isHidden = true
        }
    }
}

