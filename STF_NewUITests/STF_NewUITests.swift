//
//  STF_NewUITests.swift
//  STF_NewUITests
//
//  Created by Developer White on 05/03/2021.
//  Copyright © 2021 Wan More. All rights reserved.
//

import XCTest
@testable import STF_New


class STF_NewUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
//        let element1 = app.descendants(matching: .any)["someID"]
//        let element2 = app.descendants(matching: .window)
//        let count = element2.count
        
                        let element = app.descendants(matching: .other)
        let temp = element["GraphicView"]
        //exemple: po temp.doubleTap()
        let graphicView: WMGraphicView? = temp as? WMGraphicView // TODO: CRASH; should use a scrollView
        let temp2 = app.scrollViews
                //exemple: po temp2.element.doubleTap()
        
        var doInsert = false // only for DEBUGGER
        let entries_Str: [String] = [
            "1899-12-31 04:50:00", "1899-12-31 06:08:00", "1899-12-31 08:23:00", "1899-12-31 09:54:00",
            "1899-12-31 11:20:00", "1899-12-31 13:45:00", "1899-12-31 15:20:00", "1899-12-31 17:55:00",
            "1899-12-31 19:34:00", "1899-12-31 21:45:00", "1899-12-31 23:52:00" ]
        //        var entries_Date: [Date] = [ ]
        if doInsert {
            for es in entries_Str   {
                // entries_Date.append( WMDateUtils.date(string: es) )
                let wholeDateString = es
                
                let newEntry: WMEntry = WMEntry(userID: WMUser.uCurrentUser.uName, date: wholeDateString)
                if graphicView != nil  {
                    graphicView!.buildTFieldForNewEntry(entry: newEntry)
                }
            }
        }

        /// Undefined symbol: type metadata accessor for STF_New.WMUtils
        
    }

//    func testLaunchPerformance() {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}
