# STF - Lizez-Moi (Fr)

## Résumé

STF est une application dont le but est de vous aider dans la réduction ou l'arrêt du tabac.
Lien pour télécharger (Store Français) : https://apps.apple.com/fr/app/s-t-f/id1250088340

## Compatibilité

Tout appareil (iPad/iPhone) à partir d'iOS 9.0. 

## Principe

En tant que fumeur j'ai pu constater que vouloir se passer de cigarette est plus facile à certain moments.

Certaines sources prétendent que s'en passer d'une, provoque un état de stress qui vous incitera à plus fumer par la suite.

En réalité, cet état de stress apparaît parfois, et des fois pas... comment identifier ces moments ?

Une première version[^1] se basait sur les "Chaînes de Markov", c'est à dire que chaque évaluation d'un temps d'attente tenait compte des deux événements précédents (todo: détailler). La faire évoluer en efficacité impliquait de considérer plus que deux événements, et l'aurait fatalement rendue très délicate à configurer.

La deuxième version utilise un loi[^2] établie par des tabacologues, qui dit que la concentration du sang en nicotine, qui augmente à chaque consommation, est divisée par deux toutes les 2 heures, et qu'elle peut être considérée comme nulle au bout de 8 heures.



## Présentation

(screenshots)


## Installation

1) Ce projet utilise CocoaPods, l'installer ; Fastlane a été rajouté mais n'est pas nécessaire pour la compilation.

2)Depuis le Terminal, exécuter :
`sh pod update`

3) Ouvrir le .xcworkspace, compiler.

### Note
Si vous voyez ce message : `The linked framework 'Pods_STF_New.framework' is missing one or more architectures required by this target` losque vous changez la cible de Simulator à Device (ou l'inverse), il suffit d'appeler "Clean Build Folder" depuis le menu Product d'Xcode.


## Utilisation (Comment-Faire)

- chaque fois que vous avez "décidé" de fumer, entrez cette information, en double-tappant n'importe où sur l'écran; l'heure qui lui est associée est par défaut celle à laquelle vous avez procédé, mais vous pouvez la modifier ; 
- un événement que nous nommerons "Entrée" ("Entry" en Anglais) sera créé en haut de l'écran, d'autre événements appelés "Date" apparaissent au bas de l'écran, en tant qu'indicateur de Temps; deux autres indicateur selont également créés la première fois, sinon displacés,
- chaque fois que vous voulez fumer, vous devez vous assurer que le Temps actuel se trouve dans la "zone verte", ou du moins dans la  "zone orange". Ce sont les deux indicateurs qui viennent d'être mentionnés, et leur position est calculée selon vos propres réglages.
- double-tapper sur une Entry existante permet d'en modifier l'heure comme lors de la création (ce qui aura pour effet de la déplacer en conséquence sur l'écran) ou de la supprimer ; chaque Entry qui lui succèderait (comprendre : correspondant à une Heure ultérieure), sera recalculée, de même que les zones vertes et orange; soyez prudent.

- note (iOS > 13) : si vous avez ajouté une langue à votre appareil dans ```Réglages > Général > Langues et région > Autres langues...```, il est alors possible de la modifier dans ```Réglages > STF > Langue```

-----


[^1]: non publiée. Apple est très pointilleux sur sa charte. En réalité j'aurais sans doute pû reformuler certains des messages que l'application affiche à l'utilisateur

[^2]: Je ne la confirme pas. En réalité on dit la même chose sur l'alcool, et on sait que nous ne sommes pas tous égaux sur le sujet. Intuitivement la diminution devrait être linéaire, mais l'organisme mobilise plus de moyens pour éliminer des produits indésirables, s'ils sont en plus grande quantité. L'application a également vocation de recherche, et des amélioration seront proposées.
