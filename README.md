# STF - Read ME (En)

## Abstract

STF is an application, which intent is to help you to slow or stop smoking.
Link for the app (English Store): https://apps.apple.com/en/app/s-t-f/id1250088340

## Compatibility

Any iOS Device (iPad/iPhone) from 9.0.

## Principle

As a smoker, I did notice that trying to skip a cigarette is easier if the moment is convenient.

Some informative sources say that momently differ smoking would produce a stress context, whith invloves you in more smoking in the future.

Reality, this stress context appears, somtimes, sometimes not... how can we identify this moments?

A first version[^1] used a "Markov chain" principle, to say that each delay evaluation computes from the twice previous time events (todo: détailler). Making this feature to upgrade would imply to consider more than two events, this would be an headake to configure it (settings).
evolution in this impliquait qu'il fallait considérer plus que deux événements, l'aurait rendue délicate à configurer.

The second version used a "law"[^2] edicted from tabacologs, saying that a nicotine concentration in blood (higthering at each took), is divided by two every 2 hours. It goes to zero after 8 hours.

L'idée est donc qu'à partir de paramètres choisis (délais d'attente), le système calcule l'évolution du taux de nicotine dans le sang, pour déterminer en fonction d'un seuil de stress , un délai d'attente acceptable.



## Presentation

(todo: screenshots)


## Installation

1) This project uses CocoaPods, install it; Fastlane is available but not requiered for building the app.

2) From the Terminal, call this :
`sh pod update`

3) Open the .xcworkspace, build.

### Note
if you see this message : `The linked framework 'Pods_STF_New.framework' is missing one or more architectures required by this target` when changing from Simulator to Device (or from Device to Simulator), just call "Clean Build Folder" from the Product menu in Xcode.


## Usage (how-to)

- every time you intend to smoke, enter the Time, double-tapping anywhere on the screen; a Time will be assigned to it (by default the Current Time is puposed), you can change it;
- this creates an event, on the top of the screen, that we call "Entry"; some events called "Date" are placed on the bottom, as Time indicators; two indicators are created if it is the first time, or displaced afterwards,
- everytime you intend to smoke, you should verify that the current Time corresponds to a "green zone", or at least a "orange zone". These are the two Indicators we just mentionned, they are computed from your own settings.
- when creating a new Entry you can specify a different Time (by default the current Time); double-tapping in an existing Entry makes possible to edit the Time as for the creation (it will displace it accordingly over the screen) or remove it; each Entry that can follow it (meaning: occured later), will be re-computed, as are the green and orange zones; be carrefull.

- note (iOS > 13): if you did add a language to your Device via ```Settings > General > Language & Region > Add Language...```, it will be possible to choose is in Réglages > STF > Langue

-----

[^1]: not published. Apple consideres that some advices don't match with their policy. Really, it's possible that rewiting some messages could be the solution.

[^2]: I am not sure that it's completely true. Really, we heard the same about alcool, and we know that we behave differently. Je pencherais plutôt pour une diminution linéaire, associée à une division par un facteur différent. From my intuition, the decrease is basically linear, while the body uses more resources because of for more toxins. The application is consided as a research tool, et some improvement will be suggested thereafter.
